#! /usr/bin/env python
# encoding: utf-8

import os

def configure(conf):
    def get_param(varname,default):
        return getattr(Options.options,varname,'')or default

    if not conf.options.gmp_incdir:
        for d in ['GMP_INCLUDE','GMP_INCLUDE_DIR','GMP_INC_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.gmp_incdir=env_dir
                
    if not conf.options.gmp_libdir:
        for d in ['GMP_LIB','GMP_LIB_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.gmp_incdir=env_dir

    if not conf.options.gmp_dir:
        env_dir=os.getenv('GMP_DIR')
        if env_dir:
            conf.options.gmp_incdir=env_dir
                
    # Find GMP
    if conf.options.gmp_dir:
        if not conf.options.gmp_incdir:
            conf.options.gmp_incdir=conf.options.gmp_dir + "/include"
        if not conf.options.gmp_libdir:
            conf.options.gmp_libdir=conf.options.gmp_dir + "/lib"

    if conf.options.gmp_incdir:
        gmp_incdir=conf.options.gmp_incdir.split()
    else:
        gmp_incdir=[]
    if conf.options.gmp_libdir:
        gmp_libdir=conf.options.gmp_libdir.split()
    else:
        gmp_libdir=[]

    if conf.options.gmp_libs:
        gmp_libs=conf.options.gmp_libs.split()
    else:
        gmp_libs=['gmp']

    conf.check_cxx(msg="Checking for GMP",
                   header_name='gmp.h',
                   includes=gmp_incdir,
                   uselib_store='gmp',
                   libpath=gmp_libdir,
                   rpath=gmp_libdir,
                   lib=gmp_libs)

def options(opt):
    gmp=opt.add_option_group('GMP Options')
    gmp.add_option('--gmp-dir',
                   help='Base directory where gmp is installed')
    gmp.add_option('--gmp-incdir',
                   help='Directory where gmp include files are installed')
    gmp.add_option('--gmp-libdir',
                   help='Directory where gmp library files are installed')
    gmp.add_option('--gmp-libs',
                   help='Names of the gmp libraries without prefix or suffix\n'
                   '(e.g. "gmp")')
