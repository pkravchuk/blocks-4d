#! /usr/bin/env python
# encoding: utf-8

import os

def configure(conf):
    def get_param(varname,default):
        return getattr(Options.options,varname,'')or default

    if not conf.options.eigen_incdir:
        for d in ['EIGEN_INCLUDE','EIGEN_INCLUDE_DIR','EIGEN_INC_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.eigen_incdir=env_dir

    if not conf.options.eigen_dir:
        env_dir=os.getenv('EIGEN_DIR')
        if env_dir:
            conf.options.eigen_incdir=env_dir

    # Find EIGEN
    if conf.options.eigen_dir:
        if not conf.options.eigen_incdir:
            conf.options.eigen_incdir=conf.options.eigen_dir + "/include"

    if conf.options.eigen_incdir:
        eigen_incdir=conf.options.eigen_incdir.split()
    else:
        eigen_incdir=['/usr/include/eigen3']

    conf.check_cxx(msg="Checking for Eigen",
                   header_name=['Eigen/LU'],
                   includes=eigen_incdir,
                   uselib_store='eigen',
                   use=['cxx14'])

def options(opt):
    eigen=opt.add_option_group('EIGEN Options')
    eigen.add_option('--eigen-dir',
                     help='Base directory where eigen is installed')
    eigen.add_option('--eigen-incdir',
                     help='Directory where eigen include files are installed')
