#include "table_io.hxx"
#include <sstream>
#include <regex>
#include <iostream>

boost::filesystem::path
table_io::find_scalar_block(const boost::filesystem::path &dir,
                            int64_t keptPoleOrder, int64_t order, int64_t L,
                            const std::string &Delta12,
                            const std::string &Delta34,
                            int64_t requested_lambda)
{
  boost::filesystem::directory_iterator file(dir), end;
  std::stringstream regex_stream;
  regex_stream << "abDerivTable-d4-delta12-" << Delta12 << "-delta34-"
               << Delta34 << "-L" << L << "-nmax([0-9]+)"
               << "-keptPoleOrder" << keptPoleOrder << "-order" << order
               << ".m";

  std::regex regex(regex_stream.str());
  std::smatch match_result;
  bool found(false);
  boost::filesystem::path found_path;

  for(; file != end; ++file)
    {
      std::string filename = file->path().filename().string();
      if(std::regex_match(filename, match_result, regex))
        {
          auto submatch = match_result.begin();
          ++submatch;
          int64_t nmax = std::stoi(submatch->str());
          if(2 * nmax - 1 >= requested_lambda)
            {
              found_path = file->path();
              found = true;
            }
        }
    }
  if(found)
    return found_path;
  else
    throw std::runtime_error(
      "Couldn't find scalar block file matching \"" + regex_stream.str()
      + "\" with sufficiently large nmax in " + dir.string());
}

boost::filesystem::path
table_io::find_seed_block(const boost::filesystem::path &dir,
                          int64_t keptPoleOrder, int64_t order, int64_t L,
                          const SeedBlockType &seed, int64_t requested_lambda)
{
  if(seed.p == 0)
    {
      return table_io::find_scalar_block(dir, keptPoleOrder, order, L,
                                         seed.Delta_12, seed.Delta_34,
                                         requested_lambda);
    }

  boost::filesystem::directory_iterator file(dir), end;
  std::stringstream regex_stream;

  std::string type_str;
  if(seed.type == 0)
    type_str = "primal";
  else
    type_str = "dual";

  regex_stream << "seedBlockTable4D-type-" << type_str << "-perm-" << seed.perm
               << "-P" << seed.p << "-L" << L << "-delta12-" << seed.Delta_12
               << "-delta34-" << seed.Delta_34 << "-lambda-([0-9]+)"
               << "-keptPoleOrder-" << keptPoleOrder << "-order-" << order
               << ".m";

  std::regex regex(regex_stream.str());
  std::smatch match_result;
  bool found(false);
  boost::filesystem::path found_path;

  for(; file != end; ++file)
    {
      std::string filename = file->path().filename().string();
      if(std::regex_match(filename, match_result, regex))
        {
          auto submatch = match_result.begin();
          ++submatch;
          int64_t lambda = std::stoi(submatch->str());
          if(lambda >= requested_lambda)
            {
              found_path = file->path();
              found = true;
            }
        }
    }
  if(found)
    return found_path;
  else
    throw std::runtime_error(
      "Couldn't find seed block file matching \"" + regex_stream.str()
      + "\" with sufficiently large lambda in " + dir.string());
}

std::string table_io::form_float_string(const Bigfloat &num, int64_t len)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(len) << num;
  return ss.str();
}

int64_t table_io::get_precision(const std::string &s)
{
  size_t i = 0;
  for(; i < s.length() && s.at(i) != '.'; i++) {};
  return static_cast<int64_t>(s.length()) - i - 1;
}