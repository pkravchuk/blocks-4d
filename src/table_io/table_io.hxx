#pragma once

#include <boost/filesystem.hpp>
#include "../Bigfloat.hxx"
#include "../BlockDescription/BlockDescription.hxx"

namespace table_io
{
  boost::filesystem::path
  find_scalar_block(const boost::filesystem::path &dir, int64_t keptPoleOrder,
                    int64_t order, int64_t L, const std::string &Delta12,
                    const std::string &Delta34, int64_t requested_lambda);

  boost::filesystem::path
  find_seed_block(const boost::filesystem::path &dir, int64_t keptPoleOrder,
                  int64_t order, int64_t L, const SeedBlockType &seed,
                  int64_t requested_lambda);

  std::string form_float_string(const Bigfloat &num, int64_t len);

  int64_t get_precision(const std::string &s);
}
