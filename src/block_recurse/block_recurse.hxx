#pragma once

#include <string>
#include <map>
#include <boost/filesystem.hpp>

#include "../Bigfloat.hxx"
#include "../DerivativeTable/BlockTable.hxx"
#include "../DerivativeTable/SeedTable.hxx"
#include "../BlockDescription/BlockDescription.hxx"

BlockTable
block_recurse(const SeedTable &seed_table, BlockDescription &recursion_rule,
              int64_t lambda, int64_t p, int64_t L, const Bigfloat &Delta12,
              const Bigfloat &Delta34, const std::string &prefix = "");

void write_blocks(const std::string &input_dir_format,
                  const std::string &outfile_format,
                  const boost::filesystem::path &description_path,
                  const std::vector<int64_t> &spins,
                  const std::map<std::string, Bigfloat> &lookup,
                  const int64_t lambda, const int64_t keptPoleOrder,
                  const int64_t keptPoleOrder_final, const int64_t order,
                  const int64_t format, const size_t num_threads);

// void write_seeds(const boost::filesystem::path &input_dir,
//                  const boost::filesystem::path &output_dir, int64_t
//                  block_type, int64_t perm, int64_t p, const
//                  std::vector<int64_t> &spins, const std::string
//                  &Delta_12_string, const std::string &Delta_34_string,
//                  int64_t lambda, int64_t keptPoleOrder, int64_t order,
//                  size_t num_threads);