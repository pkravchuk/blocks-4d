#include <map>
#include <list>
#include <thread>
#include <sstream>
#include <set>

#include <fmt/core.h>

#include "block_recurse.hxx"
#include "../Polynomial/Polynomial.hxx"
#include "../table_io/table_io.hxx"
#include "../parsers/parsers.hxx"
#include "../BlockDescription/BlockDescription.hxx"
#include "../Timers.hxx"
#include "../pole_shifting/pole_shifting.hxx"

bool operator<(const SeedBlockType &a, const SeedBlockType &b)
{
  return (a.Delta_12 < b.Delta_12)
         || (a.Delta_12 == b.Delta_12 && a.Delta_34 < b.Delta_34)
         || (a.Delta_12 == b.Delta_12 && a.Delta_34 == b.Delta_34
             && a.perm < b.perm)
         || (a.Delta_12 == b.Delta_12 && a.Delta_34 == b.Delta_34
             && a.perm == b.perm && a.p < b.p)
         || (a.Delta_12 == b.Delta_12 && a.Delta_34 == b.Delta_34
             && a.perm == b.perm && a.p == b.p && a.type < b.type);
};

std::vector<std::vector<Bigfloat>>
zzb_derivatives(const Bigfloat &power, int64_t lambda)
{
  std::vector<std::vector<Bigfloat>> result;
  result.resize(lambda + 1);
  for(int64_t m = 0; m <= lambda; ++m)
    {
      result.at(m).resize((lambda - m) / 2 + 1);
    }
  for(int64_t m = 0; m <= lambda; ++m)
    for(int64_t n = 0; 2 * n + m <= lambda; ++n)
      {
        result.at(m).at(n) = pow(Bigfloat(2), -2 * power + 2 * n + m)
                             * (1 - 2 * (n % 2))
                             * Pochhammer(1 + 2 * power - 2 * n - m, m)
                             * Pochhammer(1 + power - n, n);
      }

  return result;
};

void add_derivative(int64_t m, int64_t n, Polynomial<Bigfloat> &result,
                    const SeedTable &seed, int64_t e,
                    const DerivativeWithFastPrefactor &derivative)
{
  for(size_t k_m = 0;
      k_m <= static_cast<size_t>(m) && k_m <= derivative.m_prefactor_max;
      ++k_m)
    for(size_t k_n = 0;
        k_n <= static_cast<size_t>(n) && k_n <= derivative.n_prefactor_max;
        ++k_n)
      {
        const auto &prefactor_der
          = derivative.prefactor_derivatives.at(k_m).at(k_n);

        if(prefactor_der.size() == 0)
          continue;
        result += prefactor_der
                  * seed.derivative(e, m - k_m + derivative.m,
                                    n - k_n + derivative.n)
                  * binomial_coefficient(m, k_m)  // Can optimize away these
                  * binomial_coefficient(n, k_n); // binomials, but unnecessary
      }
}

BlockTable
block_recurse(const std::map<SeedBlockType, SeedTable> &seed,
              const BlockDescription &block_rule, int64_t lambda,
              const int64_t L, const std::map<std::string, Bigfloat> &lookup,
              const int64_t format, const std::string &prefix = "")
{
  std::string timer_prefix = prefix + ".block_recurse";
  Timer &total = global_timers->add_and_start(timer_prefix);

  BlockTable result;

  std::map<std::string, Bigfloat> lookup_internal(lookup);
  lookup_internal[block_rule.spinParameter] = L;

  result.p = block_rule.exchangeP;
  result.exchangeType = block_rule.exchangeType;
  result.L = L;

  result.delta_minus_x = 2 + L + Bigfloat(result.p) / 2;
  result.description.CopyFrom(block_rule.description,
                              result.description.GetAllocator());

  result.lambda = lambda;
  result.lookup = lookup;

  bool set_poles = false;
  for(const auto &rule : block_rule.blockRules)
    {
      auto block_timer_prefix = timer_prefix + ".block=\"" + rule.first + "\"";
      Timer &block_timer = global_timers->add_and_start(block_timer_prefix);

      Timer &specialization_timer = global_timers->add_and_start(
        block_timer_prefix + ".specializing_rule");
      PrefactorDerivativeTable specialized_rule
        = block_rule.make_prefactor_derivative_table(
          rule.first, lookup_internal, format, false, block_rule.exchangeP);
      specialization_timer.stop();

      auto &result_for_block = result.derivatives[rule.first];

      result_for_block.resize(lambda + 1);
      for(int64_t m = 0; m <= lambda; ++m)
        {
          result_for_block.at(m).resize((lambda - m) / 2 + 1);
        }

      for(int64_t m = 0; m <= lambda; ++m)
        for(int64_t n = 0; 2 * n + m <= lambda; ++n)
          {
            auto &derivative_to_fill = result_for_block.at(m).at(n);
            derivative_to_fill = Polynomial<Bigfloat>(0);
            for(const auto &block_contribution : specialized_rule.rule)
              {
                int64_t block_e = block_contribution.first.e;
                const SeedTable &current_seed
                  = seed.at(SeedBlockType(block_contribution.first));
                if(block_e < 0 || block_e > block_rule.exchangeP)
                  continue;
                for(const auto &block_derivative : block_contribution.second)
                  {
                    add_derivative(m, n, derivative_to_fill, current_seed,
                                   block_e, block_derivative);
                  }
              }
            for(const auto &pole : specialized_rule.spurious_poles)
              {
                Polynomial<Bigfloat> pole_poly({-pole, 1});
                derivative_to_fill = boost::math::tools::quotient_remainder(
                                       derivative_to_fill, pole_poly)
                                       .first;
              }
          }

      if(!specialized_rule.zPower.is_zero())
        {
          Timer &zTimer
            = global_timers->add_and_start(block_timer_prefix + ".zPower");

          std::vector<std::vector<Bigfloat>> zzb_ders(
            zzb_derivatives(specialized_rule.zPower, lambda));

          std::vector<std::vector<Polynomial<Bigfloat>>> new_table;
          new_table.resize(lambda + 1);
          for(int64_t m = 0; m <= lambda; ++m)
            new_table.at(m).resize((lambda - m) / 2 + 1);

          for(int64_t m = 0; m <= lambda; ++m)
            for(int64_t n = 0; 2 * n + m <= lambda; ++n)
              {
                auto &new_der = new_table.at(m).at(n);
                for(int64_t m_old = 0; m_old <= m; ++m_old)
                  for(int64_t n_old = 0; n_old <= n; ++n_old)
                    {
                      new_der += result_for_block.at(m_old).at(n_old)
                                 * zzb_ders.at(m - m_old).at(n - n_old)
                                 * binomial_coefficient(m, m_old)
                                 * binomial_coefficient(n, n_old);
                    }
              }
          result_for_block = std::move(new_table);
          zTimer.stop();
        }

      if(!specialized_rule.one_minus_zPower.is_zero())
        {
          Timer &zTimer = global_timers->add_and_start(block_timer_prefix
                                                       + ".one_minus_zPower");
          std::vector<std::vector<Bigfloat>> zzb_ders(
            zzb_derivatives(specialized_rule.one_minus_zPower, lambda));

          std::vector<std::vector<Polynomial<Bigfloat>>> new_table;
          new_table.resize(lambda + 1);
          for(int64_t m = 0; m <= lambda; ++m)
            new_table.at(m).resize((lambda - m) / 2 + 1);

          for(int64_t m = 0; m <= lambda; ++m)
            for(int64_t n = 0; 2 * n + m <= lambda; ++n)
              {
                auto &new_der = new_table.at(m).at(n);
                for(int64_t m_old = 0; m_old <= m; ++m_old)
                  for(int64_t n_old = 0; n_old <= n; ++n_old)
                    {
                      new_der += result_for_block.at(m_old).at(n_old)
                                 * zzb_ders.at(m - m_old).at(n - n_old)
                                 * binomial_coefficient(m, m_old)
                                 * binomial_coefficient(n, n_old)
                                 * (1 - 2 * ((m - m_old) % 2));
                    }
              }
          result_for_block = std::move(new_table);
          zTimer.stop();
        }

      if(!set_poles)
        {
          result.poles = specialized_rule.poles;
          set_poles = true;
        }

      block_timer.stop();
    }

  total.stop();
  return result;
}

void truncate_polynomials(BlockTable &bt, const BlockDescription &description)
{
  for(const auto &a : description.h_infinity_order)
    {
      auto &derivs = bt.derivatives[a.first];
      int64_t base_degree = a.second + bt.poles.size();
      //    std::vector<std::vector<Polynomial<Bigfloat>>> new_derivs;
      //    new_derivs.resize(old_derivs.size());
      for(size_t m(0); m < derivs.size(); ++m)
        {
          //      new_derivs.at(m).resize(old_derivs.at(m).size());
          for(size_t n(0); n < derivs.at(m).size(); ++n)
            {
              int64_t degree = base_degree + m + n;
              if(derivs.at(m).at(n).size() > (size_t)degree + 1)
                {
                  derivs.at(m).at(n).data().resize(degree + 1);
                }
            }
        }
    }
}

SeedBlock form_seed_block(const SeedBlockDerivative &der,
                          const std::map<std::string, Bigfloat> &lookup,
                          int64_t float_format);

std::set<SeedBlockType>
generate_seed_list(const BlockDescription &description,
                   const std::map<std::string, Bigfloat> &lookup,
                   const int64_t format)
{
  std::set<SeedBlockType> result;

  for(const auto &block : description.blockRules)
    {
      for(const auto &entry : block.second)
        {
          result.emplace(form_seed_block(entry.second, lookup, format));
        }
    }

  return result;
}

boost::filesystem::path
format_input_dir(const std::string &input_dir_format, int64_t L)
{
  return boost::filesystem::path(fmt::format(input_dir_format, L));
}

boost::filesystem::path
format_output_file(const std::string &outfile_format, int64_t L)
{
  return boost::filesystem::path(fmt::format(outfile_format, L));
}

std::vector<Bigfloat>
seed_pole_list_single(int64_t L, int64_t p, int64_t type,const Bigfloat &shift)
{
  if(type == 0 && L > 0)
    {
      std::vector<Bigfloat> result(2);
      result[0] = (Bigfloat(2) + p) / 2 - shift;
      result[1] = (Bigfloat(4) + 2 * L + p) / 2 - shift;
      return result;
    }
  if(type == 0 && L == 0)
    {
      std::vector<Bigfloat> result(1);
      result[0] = (Bigfloat(2) + p) / 2 - shift;
      return result;
    }
  if(type == 1 && L > 0)
    {
      std::vector<Bigfloat> result(1);
      result[0] = (Bigfloat(4) - p) / 2 - shift;
      return result;
    }
  if(type == 1 && L == 0)
    {
      return std::vector<Bigfloat>(0);
    }
  // Should use more appropriate types and not deal with this...
  throw std::runtime_error("Impossible type/spin combination");
}

void add_seed_poles(int64_t pole_order, int64_t L, int64_t p, int64_t type,
                    std::vector<Bigfloat> &poles)
{
  std::list<Bigfloat> new_poles;
  int64_t sign = 1 - 2 * type;
  Bigfloat shift = Bigfloat(p) / 2 * sign;

  // Scalar poles
  for(int64_t k = 1; k <= pole_order; ++k)
    {
      new_poles.push_back(1 - L - k - shift);
      if(k <= (pole_order - 2 * L - 2) / 2)
        {
          new_poles.push_back(1 - L - k - shift);
        }
    }
  for(int64_t k = 1; k <= pole_order && k <= L; ++k)
    {
      new_poles.push_back(3 + L - k - shift);
    }
  for(int64_t k = L + 2; k <= pole_order && k <= 2 * L + 2; ++k)
    {
      new_poles.push_back(3 + L - k - shift);
    }

  // Seed poles
  for(int64_t n = 1; n <= p; ++n)
    {
      std::vector<Bigfloat> single = seed_pole_list_single(
        L, p - n + 1, type, sign * (Bigfloat(n) - 1) / 2);
      for(auto &pole : single)
        {
          new_poles.push_back(pole);
        }
    }

  // Append
  poles.reserve(poles.size() + new_poles.size());
  for(auto &pole : new_poles)
    poles.push_back(pole);
}

void write_blocks_thread(const std::string &input_dir_format,
                         const std::string &outfile_format,
                         const BlockDescription &description,
                         const std::vector<int64_t> &spins,
                         const std::map<std::string, Bigfloat> &lookup,
                         const int64_t lambda, const int64_t keptPoleOrder,
                         const int64_t keptPoleOrder_final,
                         const int64_t order, const int64_t format,
                         const size_t thread_rank, const size_t num_threads)
{
  std::string thread_prefix
    = "write_blocks.thread" + std::to_string(thread_rank);
  Timer &total_thread = global_timers->add_and_start(thread_prefix + ".Total");

  for(size_t spin_pos = thread_rank; spin_pos < spins.size();
      spin_pos += num_threads)
    {
      size_t L = spins.at(spin_pos);

      std::string thread_spin_prefix
        = thread_prefix + ".L" + std::to_string(L);

      Timer &spin_reading = global_timers->add_and_start(
        thread_spin_prefix + ".reading_seed_blocks");

      std::set<SeedBlockType> seed_types(
        generate_seed_list(description, lookup, format));

      std::map<SeedBlockType, SeedTable> seeds;
      for(auto &seed_type : seed_types)
        {
          boost::filesystem::path seed_file = table_io::find_seed_block(
            format_input_dir(input_dir_format, L), keptPoleOrder, order, L,
            seed_type, lambda + description.lambdaShift);
          seeds.emplace(std::make_pair(
            seed_type, parsers::parse_seed_table_file(seed_file.string())));
          if(description.exchangeP == 0)
            seeds.at(seed_type).normalize(L);
          else
            seeds.at(seed_type).normalize_seeds(seed_type.type, seed_type.p,
                                                L);
        }

      spin_reading.stop();

      Timer &total_recursion
        = global_timers->add_and_start(thread_spin_prefix + ".seed_recurse");

      BlockTable block(block_recurse(seeds, description, lambda, L, lookup,
                                     format,
                                     thread_spin_prefix + ".seed_recurse"));

      block.order = order;
      block.pole_order = keptPoleOrder;
      block.final_pole_order = keptPoleOrder;
      std::vector<Bigfloat> new_poles(block.poles);
      add_seed_poles(keptPoleOrder, L, description.exchangeP,
                     description.seedType, block.poles);

      if(keptPoleOrder_final != keptPoleOrder)
        {
          Timer &pole_shifting = global_timers->add_and_start(
            thread_spin_prefix + ".shifting_poles");

          add_seed_poles(keptPoleOrder_final, L, description.exchangeP,
                         description.seedType, new_poles);

          block = shift_poles(block, new_poles);
          block.final_pole_order = keptPoleOrder_final;

          pole_shifting.stop();
        }

      std::sort(block.poles.begin(), block.poles.end());

      Timer &truncation_timer = global_timers->add_and_start(
        thread_spin_prefix + ".truncating_polynomials");
      truncate_polynomials(block, description);
      truncation_timer.stop();

      total_recursion.stop();

      Timer &total_writing
        = global_timers->add_and_start(thread_spin_prefix + ".writing_file");

      // std::stringstream ss;
      // seedBlockTable4D-type-dual-perm-1234-P1-L0-a--0.43955000-b-0.61955000-lambda-9-keptPoleOrder-25-order-90
      // ss << "block-L" << L << ".m";

      boost::filesystem::path filename = format_output_file(outfile_format, L);

      if(global_timers->debug)
        std::cout << "Writing file " << filename.string() << std::endl;

      create_directories(filename.parent_path());
      boost::filesystem::ofstream outfile(filename);
      if(!outfile)
        throw std::runtime_error("Cannot open file " + filename.string()
                                 + " for writing.");

      outfile << std::fixed;
      outfile.precision(boost::multiprecision::mpf_float::default_precision());

      outfile << block << std::endl;

      outfile.close();

      total_writing.stop();
    }
  total_thread.stop();
}

void write_blocks(const std::string &input_dir_format,
                  const std::string &output_format,
                  const boost::filesystem::path &description_path,
                  const std::vector<int64_t> &spins,
                  const std::map<std::string, Bigfloat> &lookup,
                  const int64_t lambda, const int64_t keptPoleOrder,
                  const int64_t keptPoleOrder_final, const int64_t order,
                  const int64_t format, const size_t num_threads)
{
  Timer &description_timer
    = global_timers->add_and_start("write_blocks.load_description");
  BlockDescription description(
    parsers::parse_block_description_file(description_path.string()));
  description.validate();
  description_timer.stop();

  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      threads.emplace_back(write_blocks_thread, std::cref(input_dir_format),
                           std::cref(output_format), std::cref(description),
                           std::cref(spins), std::cref(lookup), lambda,
                           keptPoleOrder, keptPoleOrder_final, order, format,
                           thread_rank, num_threads);
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
}
