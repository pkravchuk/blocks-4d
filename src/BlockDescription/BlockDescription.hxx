#pragma once

#include <string>
#include <vector>
#include <ostream>
#include "../Polynomial/Multivariate.hxx"
#include "../Polynomial/RationalFunction.hxx"
#include "../Bigfloat.hxx"

#include "../rapidjson/document.h"

struct SeedBlockDerivative
{
  int64_t perm, m, n;
  int64_t type, p;
  RationalFunction a, b, e;
};

struct SeedBlock
{
  int64_t perm, e, p, type;
  Bigfloat a, b;
  std::string Delta_12_string, Delta_34_string;
};

struct SeedBlockType
{
  int64_t perm, p, type;
  std::string Delta_12, Delta_34;
  SeedBlockType(const SeedBlock &block)
      : perm(block.perm), p(block.p), type(block.type),
        Delta_12(block.Delta_12_string), Delta_34(block.Delta_34_string){};
};

struct DerivativeWithFastPrefactor
{
  int64_t m, n;
  size_t m_prefactor_max, n_prefactor_max;
  std::vector<std::vector<Polynomial<Bigfloat>>> prefactor_derivatives;
};

struct PrefactorDerivativeTable
{
  std::vector<Bigfloat> spurious_poles;
  std::vector<Bigfloat> poles;
  Bigfloat zPower, one_minus_zPower;
  std::vector<std::pair<SeedBlock, std::vector<DerivativeWithFastPrefactor>>>
    rule;
};

using BlockRule = std::vector<std::pair<RationalFunction, SeedBlockDerivative>>;

struct BlockDescription
{
  std::string dimensionParameter = "d";
  std::string spinParameter = "l";
  std::string xParameter = "x";
  std::string tParameter = "t";
  std::vector<RationalFunction> spuriousPoles;
  std::map<int64_t, std::vector<RationalFunction>> spuriousPolesL;
  std::vector<RationalFunction> poles;
  std::map<int64_t, std::vector<RationalFunction>> polesL;
  std::map<std::string, RationalFunction> zPower;
  std::map<std::string, RationalFunction> one_minus_zPower;
  std::map<std::string, int64_t> h_infinity_order;
  int64_t exchangeType = -1; // primal = 0, dual = 1
  int64_t seedType = -1; // primal = 0, dual = 1
  int64_t exchangeP = -1;
  int64_t lambdaShift = -1;
  std::map<std::string, BlockRule> blockRules;

  rapidjson::Document description;

  void validate();

  PrefactorDerivativeTable make_prefactor_derivative_table(
    const std::string &, const std::map<std::string, Bigfloat> &lookup,
    int64_t float_format, bool override_p = false, int64_t p_new = 0) const;
};

std::ostream &operator<<(std::ostream &, const BlockDescription &);
std::ostream &operator<<(std::ostream &, const BlockRule &);
std::ostream &operator<<(std::ostream &, const SeedBlockDerivative &);

std::ostream &operator<<(std::ostream &, const PrefactorDerivativeTable &);