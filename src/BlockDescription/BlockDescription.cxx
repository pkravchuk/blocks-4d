#include <sstream>

#include "../table_io/table_io.hxx"
#include "BlockDescription.hxx"

SeedBlock form_seed_block(const SeedBlockDerivative &der,
                          const std::map<std::string, Bigfloat> &lookup,
                          int64_t float_format)
{
  SeedBlock result;

  Multivariate a = der.a.specialize_to_multivariate(lookup);
  if(a.terms.size() != 1)
    throw std::runtime_error(
      "\"a\" parameter of a seed block does not specialize to a number");
  result.a = a.terms.begin()->coefficient;

  Multivariate b = der.b.specialize_to_multivariate(lookup);
  if(b.terms.size() != 1)
    throw std::runtime_error(
      "\"b\" parameter of a seed block does not specialize to a number");
  result.b = b.terms.begin()->coefficient;

  Multivariate e = der.e.specialize_to_multivariate(lookup);
  if(e.terms.size() != 1)
    throw std::runtime_error(
      "\"e\" parameter of a seed block does not specialize to a number");

  Bigfloat ef = e.terms.begin()->coefficient;

  int64_t ei = round_bigfloat(ef);
  if(abs(ei - ef) > 0.01)
    throw std::runtime_error(
      "Evaluated value of e + does not look like an integer.");
  result.e = ei;

  result.perm = der.perm;
  result.p = der.p;
  result.type = der.type;

  Bigfloat Delta12 = -2 * result.a;
  Bigfloat Delta34 = 2 * result.b;
  result.Delta_12_string = table_io::form_float_string(Delta12, float_format);
  result.Delta_34_string = table_io::form_float_string(Delta34, float_format);

  return result;
};

PrefactorDerivativeTable BlockDescription::make_prefactor_derivative_table(
  const std::string &block_name, const std::map<std::string, Bigfloat> &lookup,
  int64_t float_format, bool override_p, int64_t p_new) const
{
  PrefactorDerivativeTable result;

  // For compatibility with seed blocks it is important that exchangeP is only
  // used here. This is because seed blocks may use negatvie exchangeP to
  // optimize polynomial shifting.

  Bigfloat unitarity_bound;
  if(!override_p)
    unitarity_bound = 2 + lookup.at(spinParameter) + Bigfloat(exchangeP) / 2;
  else
    unitarity_bound = 2 + lookup.at(spinParameter) + Bigfloat(p_new) / 2;
  const auto &block_rule = blockRules.at(block_name);

  int64_t spin = round_bigfloat(lookup.at(spinParameter));
  auto poles_it = spuriousPolesL.find(spin);
  if(poles_it == spuriousPolesL.end())
    {
      result.spurious_poles.reserve(spuriousPoles.size());
      for(const auto &pole : spuriousPoles)
        {
          Multivariate p = pole.specialize_to_multivariate(lookup);
          if(p.terms.size() != 1)
            throw std::runtime_error(
              "A spurious pole does not specialize to a number");
          result.spurious_poles.push_back(p.terms.begin()->coefficient
                                          - unitarity_bound);
        }
    }
  else
    {
      result.spurious_poles.reserve(poles_it->second.size());
      for(const auto &pole : poles_it->second)
        {
          Multivariate p = pole.specialize_to_multivariate(lookup);
          if(p.terms.size() != 1)
            throw std::runtime_error(
              "A spurious pole does not specialize to a number");
          result.spurious_poles.push_back(p.terms.begin()->coefficient
                                          - unitarity_bound);
        }
    }

  poles_it = polesL.find(spin);
  if(poles_it == polesL.end())
    {
      result.poles.reserve(poles.size());
      for(const auto &pole : poles)
        {
          Multivariate p = pole.specialize_to_multivariate(lookup);
          if(p.terms.size() != 1)
            throw std::runtime_error("A pole does not specialize to a number");
          result.poles.push_back(p.terms.begin()->coefficient);
        }
    }
  else
    {
      result.poles.reserve(poles_it->second.size());
      for(const auto &pole : poles_it->second)
        {
          Multivariate p = pole.specialize_to_multivariate(lookup);
          if(p.terms.size() != 1)
            throw std::runtime_error("A pole does not specialize to a number");
          result.poles.push_back(p.terms.begin()->coefficient);
        }
    }

  Multivariate exponent
    = zPower.at(block_name).specialize_to_multivariate(lookup);
  if(exponent.terms.size() != 1)
    throw std::runtime_error("zPower does not specialize to a number");
  result.zPower = exponent.terms.begin()->coefficient;

  exponent
    = one_minus_zPower.at(block_name).specialize_to_multivariate(lookup);
  if(exponent.terms.size() != 1)
    throw std::runtime_error(
      "one_minus_zPower does not specialize to a number");
  result.one_minus_zPower = exponent.terms.begin()->coefficient;

  auto find_in_result = [&](const SeedBlock &seed) {
    for(auto it = result.rule.begin(); it != result.rule.end(); ++it)
      {
        auto &term = *it;
        if(term.first.perm == seed.perm && term.first.e == seed.e
           && term.first.Delta_12_string == seed.Delta_12_string
           && term.first.Delta_34_string == seed.Delta_34_string)
          {
            return it;
          }
      }
    return result.rule.end();
  };

  for(const auto &term : block_rule)
    {
      SeedBlock block = form_seed_block(term.second, lookup, float_format);

      auto block_entry = find_in_result(block);
      if(block_entry == result.rule.end())
        {
          result.rule.emplace_back();
          block_entry = result.rule.end() - 1;
          block_entry->first = block;
        }
      block_entry->second.emplace_back();
      auto &der_with_pref = block_entry->second.back();

      der_with_pref.m = term.second.m;
      der_with_pref.n = term.second.n;

      Multivariate prefactor
        = term.first.specialize_to_multivariate(lookup).shift_variable(
          dimensionParameter, unitarity_bound);

      der_with_pref.m_prefactor_max = prefactor.degree(xParameter);
      der_with_pref.n_prefactor_max = prefactor.degree(tParameter);

      der_with_pref.prefactor_derivatives.resize(der_with_pref.m_prefactor_max
                                                 + 1);
      for(auto &der_list : der_with_pref.prefactor_derivatives)
        {
          der_list.resize(der_with_pref.n_prefactor_max + 1,
                          Polynomial<Bigfloat>(0));
        }

      for(auto &prefactor_term : prefactor.terms)
        {
          int64_t m = prefactor_term.exponent(xParameter),
                  n = prefactor_term.exponent(tParameter);

          auto &entry = der_with_pref.prefactor_derivatives.at(m).at(n);

          Multivariate::Term t(prefactor_term);

          t.exponents.erase(xParameter);
          t.exponents.erase(tParameter);
          if(t.exponent(dimensionParameter) > 0)
            t.exponents["x"] = t.exponent(dimensionParameter);
          t.exponents.erase(dimensionParameter);
          t.coefficient = t.coefficient * factorial(m) * factorial(n);

          entry = entry + Multivariate(t).to_poly();
        }
    }
  return result;
}

void BlockDescription::validate()
{
  if(exchangeType == -1)
    throw std::runtime_error("Error parsing block description: type parameter "
                             "of block description must be specified.");
  if(exchangeP == -1)
    throw std::runtime_error("Error parsing block description: p parameter of "
                             "block description must be specified.");
  if(lambdaShift == -1)
    throw std::runtime_error(
      "Error parsing block description: lambda_shift parameter of "
      "block description must be specified.");

  if(blockRules.empty())
    throw std::runtime_error(
      "Error parsing block description: at least one block must be exported.");
}

std::ostream &operator<<(std::ostream &out, const BlockDescription &desc)
{
  out << "dimensionParameter = " << desc.dimensionParameter << ";"
      << std::endl;
  out << "spinParameter = " << desc.spinParameter << ";" << std::endl;
  out << "xParameter = " << desc.xParameter << ";" << std::endl;
  out << "tParameter = " << desc.tParameter << ";" << std::endl;
  out << "p =" << desc.exchangeP << ";" << std::endl;
  out << "type =" << desc.exchangeType << ";" << std::endl;
  out << "lambdaShift =" << desc.lambdaShift << ";" << std::endl;
  out << "spuriousPoles = {";
  for(auto &pole : desc.spuriousPoles)
    {
      out << pole << ", ";
    }
  out << "};" << std::endl;
  out << "poles = {";
  for(auto &pole : desc.poles)
    {
      out << pole << ", ";
    }
  out << "};" << std::endl;
  for(auto &special_poles : desc.spuriousPolesL)
    {
      out << "spuriousPoles[" << special_poles.first << "] = {";
      for(auto &pole : special_poles.second)
        {
          out << pole << ", ";
        }
      out << "};" << std::endl;
    }
  for(auto &special_poles : desc.polesL)
    {
      out << "poles[" << special_poles.first << "] = {";
      for(auto &pole : special_poles.second)
        {
          out << pole << ", ";
        }
      out << "};" << std::endl;
    }
  out << "exchangeType = " << desc.exchangeType << ";" << std::endl;
  out << "exchangeP = " << desc.exchangeP << ";" << std::endl;
  out << "lambdaShift = " << desc.lambdaShift << ";" << std::endl;

  for(auto &rule : desc.blockRules)
    {
      out << "zPower = " << desc.zPower.at(rule.first) << std::endl;
      out << "zbPower = " << desc.one_minus_zPower.at(rule.first) << std::endl;
      out << rule.first << " -> " << rule.second << ";" << std::endl;
    }

  return out;
}

std::ostream &operator<<(std::ostream &out, const BlockRule &rule)
{
  out << "<";
  for(auto &term : rule)
    {
      out << "(" << term.first << ") * " << term.second << ",";
    }
  out << ">";
  return out;
}

std::ostream &operator<<(std::ostream &out, const SeedBlockDerivative &block)
{
  out << "seedBlock[" << block.perm << "," << block.type << ", " << block.a
      << ", " << block.b << ", " << block.p << ", " << block.e << "]["
      << block.m << ", " << block.n << "]";
  return out;
}
std::ostream &
operator<<(std::ostream &out, const PrefactorDerivativeTable &table)
{
  out << "Spurious poles: {";
  for(const auto &pole : table.spurious_poles)
    {
      out << pole << ", ";
    }
  out << "}" << std::endl;

  for(const auto &block : table.rule)
    {
      out << "Using seed e=" << block.first.e << " perm" << block.first.perm
          << " Delta12" << block.first.Delta_12_string << " Delta34"
          << block.first.Delta_34_string << std::endl;
      for(const auto &term : block.second)
        {
          out << "Using xtDerivative[" << term.m << ", " << term.n << "]"
              << std::endl;
          out << "{ ";
          for(size_t m = 0; m <= term.m_prefactor_max; ++m)
            {
              out << "{ ";
              for(size_t n = 0; n <= term.n_prefactor_max; ++n)
                {
                  out << term.prefactor_derivatives.at(m).at(n) << ", ";
                }
              out << " }, " << std::endl;
            }
          out << " }" << std::endl;
        }
    }
  return out;
}