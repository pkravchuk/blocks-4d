#include <iostream>
#include <fstream>
#include <vector>
// #include "parsers/parsers.hxx"
#include "BlockDescription/BlockDescription.hxx"
#include "Timers.hxx"
#include "pole_shifting/pole_shifting.hxx"
// #include "table_io/table_io.hxx"
// #include "block_recurse/block_recurse.hxx"

// #include "parsers/shunting_yard/read_token.hxx"
// #include "parsers/shunting_yard/shunting_yard.hxx"

// #include <boost/property_tree/ptree.hpp>
// #include <boost/property_tree/json_parser.hpp>

// #include "rapidjson/document.h"
// #include "rapidjson/istreamwrapper.h"
// #include "rapidjson/ostreamwrapper.h"
// #include "rapidjson/prettywriter.h"

// namespace pt = boost::property_tree;

Timers *global_timers;

// std::ostream &operator<<(std::ostream &out, const pt::ptree &tree)
// {
//   out << "[\n  data : \"" << tree.data() << "\"\n"
//       << "{\n";
//   for(const auto &child : tree)
//     {
//       out << "name : \"" << child.first << "\"\n";
//       out << child.second;
//       out << "\n";
//     }
//   out << "}";
//   return out;
// }
template <typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &vec);

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::pair<T1, T2> &p)
{
  out << "{" << p.first << ", " << p.second << "}";
  return out;
}

template <typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &vec)
{
  out << "{";
  for(int64_t i = 0; i < static_cast<int64_t>(vec.size()); ++i)
    {
      out << vec.at(i);
      if(i + 1 < static_cast<int64_t>(vec.size()))
        out << ", ";
    }
  out << "}";
  return out;
}

BlockDescription fast_parse_block_description(std::istream &input);

int main()
{
  global_timers = new Timers(true);

  boost::multiprecision::mpf_float::default_precision(200);
  std::cout << std::fixed;

  Polynomial<Bigfloat> poly({1, 2, 3, 4, 5, 6, 7});
  std::vector<Bigfloat> poles = {-1, -2, -2, -2, -3, -3};

  std::cout << poly << std::endl;

  PoleCache cache(poles);

  std::cout << cache.denom_derivs << std::endl;

  MeromorphicFunction m(to_meromorphic_fn(poly, cache));

  std::cout << m.poles << std::endl;
  std::cout << m.polynomial << std::endl;

  PolynomialCache pcache(cache.unique_poles);

  std::cout << "pcache.poly_part" << pcache.poly_part << std::endl;
  std::cout << "pcache.denom_parts" << pcache.denom_parts << std::endl;

  Polynomial<Bigfloat> new_poly(to_polynomial(m, pcache));

  std::cout << new_poly << std::endl;

  //  std::vector<std::pair<Bigfloat, int64_t>> new_poles
  //    = {std::make_pair(-2.5, 1), std::make_pair(-5, 2), std::make_pair(-6,
  //    5)};
  //
  //  MeromorphicFunction m_new(shift_poles_m(m, new_poles, -1));
  //
  //  std::cout << m_new.poles << std::endl;
  //  std::cout << m_new.polynomial << std::endl;

  delete global_timers;

  return 0;
}
