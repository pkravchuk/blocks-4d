#pragma once

#include <utility>
#include <map>
#include <string>
#include <ostream>
#pragma once

#include "../Bigfloat.hxx"
#include "Multivariate.hxx"

struct RationalFunction
{
  RationalFunction();
  RationalFunction(const Multivariate &);
  RationalFunction(const Multivariate &, const Multivariate &);

  RationalFunction operator+(const RationalFunction &) const;
  RationalFunction operator-() const;
  RationalFunction operator-(const RationalFunction &) const;
  RationalFunction operator*(const RationalFunction &)const;
  RationalFunction operator/(const RationalFunction &) const;
  RationalFunction operator^(int64_t) const;

  RationalFunction specialize(const std::map<std::string, Bigfloat> &) const;
  Multivariate
  specialize_to_multivariate(const std::map<std::string, Bigfloat> &) const;

  std::pair<Multivariate, Multivariate> value;
};

std::ostream &operator<<(std::ostream &, const RationalFunction &);
