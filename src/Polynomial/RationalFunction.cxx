#include "RationalFunction.hxx"

RationalFunction::RationalFunction()
{
  value.first = Multivariate(int64_t(0));
  value.second = Multivariate(int64_t(1));
}

RationalFunction::RationalFunction(const Multivariate &poly)
{
  value.first = poly;
  value.second = Multivariate(int64_t(1));
}

RationalFunction::RationalFunction(const Multivariate &num,
                                   const Multivariate &denom)
{
  value.first = num;
  value.second = denom;
}

RationalFunction RationalFunction::
operator+(const RationalFunction &right) const
{
  return RationalFunction(value.first * right.value.second
                            + value.second * right.value.first,
                          value.second * right.value.second);
}

RationalFunction RationalFunction::
operator-(const RationalFunction &right) const
{
  return RationalFunction(value.first * right.value.second
                            - value.second * right.value.first,
                          value.second * right.value.second);
}

RationalFunction RationalFunction::operator-() const
{
  return RationalFunction(-value.first, value.second);
}

RationalFunction RationalFunction::
operator*(const RationalFunction &right) const
{
  return RationalFunction(value.first * right.value.first,
                          value.second * right.value.second);
}

RationalFunction RationalFunction::
operator/(const RationalFunction &right) const
{
  return RationalFunction(value.first * right.value.second,
                          value.second * right.value.first);
}

RationalFunction RationalFunction::operator^(int64_t pow) const
{
  if(pow >= 0)
    return RationalFunction(value.first ^ pow, value.second ^ pow);
  else
    return RationalFunction(value.second ^ (-pow), value.first ^ (-pow));
}

RationalFunction RationalFunction::specialize(
  const std::map<std::string, Bigfloat> &lookup) const
{
  return RationalFunction(value.first.specialize(lookup),
                          value.second.specialize(lookup));
}

Multivariate RationalFunction::specialize_to_multivariate(
  const std::map<std::string, Bigfloat> &lookup) const
{
  RationalFunction tmp(specialize(lookup));
  if(tmp.value.second.terms.size() != 1
     || tmp.value.second.terms.begin()->exponents.size() != 0
     || tmp.value.second.terms.begin()->coefficient.is_zero())
  {
    std::ostringstream s;
    s << tmp.value.second;
    throw std::runtime_error(
      std::string("When specializing rational function to a polynomial, could not "
      "evaluate denominator to a non-zero number. The denominator is "+s.str()));
  }
  return Multivariate(1 / tmp.value.second.terms.begin()->coefficient)
         * tmp.value.first;
}

std::ostream &operator<<(std::ostream &out, const RationalFunction &fun)
{
  out << "(" << fun.value.first << ")/(" << fun.value.second << ")";
  return out;
}