#include <vector>
#include "Multivariate.hxx"

Multivariate::Term::Term(const Bigfloat &val) { coefficient = val; }

Multivariate::Term Multivariate::Term::
operator*(const Multivariate::Term &b) const
{
  Multivariate::Term result(coefficient * b.coefficient);
  result.exponents = exponents;
  for(auto &factor : b.exponents)
    {
      auto factor_in_result = result.exponents.find(factor.first);
      if(factor_in_result != result.exponents.end())
        {
          factor_in_result->second += factor.second;
        }
      else
        {
          result.exponents[factor.first] = factor.second;
        }
    }
  return result;
}

bool Multivariate::Term::operator==(const Multivariate::Term &b) const
{
  return exponents == b.exponents;
}

bool Multivariate::Term::operator!=(const Multivariate::Term &b) const
{
  return exponents != b.exponents;
}

bool Multivariate::Term::operator<(const Multivariate::Term &b) const
{
  return exponents < b.exponents;
}

bool Multivariate::Term::operator>(const Multivariate::Term &b) const
{
  return exponents > b.exponents;
}

bool Multivariate::Term::operator<=(const Multivariate::Term &b) const
{
  return exponents <= b.exponents;
}

bool Multivariate::Term::operator>=(const Multivariate::Term &b) const
{
  return exponents >= b.exponents;
}

int64_t Multivariate::Term::exponent(const std::string &var) const
{
  std::map<std::string, int64_t>::const_iterator it = exponents.find(var);
  if(it == exponents.end())
    return 0;
  return it->second;
}

Multivariate::Multivariate(const std::string &s)
{
  terms.push_back(Term(Bigfloat(1)));
  terms.back().exponents[s] = 1;
}

Multivariate::Multivariate(const char *s) : Multivariate(std::string(s)){};

Multivariate::Multivariate(const Bigfloat &val) { terms.emplace_back(val); };

Multivariate::Multivariate(int64_t val) : Multivariate(Bigfloat(val)){};
Multivariate::Multivariate(double val) : Multivariate(Bigfloat(val)){};

Multivariate::Multivariate(const Multivariate::Term &term)
{
  terms.push_back(term);
}

Multivariate Multivariate::operator+(const Multivariate &b) const
{
  Multivariate result;
  std::list<Term>::const_iterator it_a = terms.begin(), it_b = b.terms.begin();
  while(it_a != terms.end() && it_b != b.terms.end())
    {
      if(*it_a < *it_b)
        {
          result.terms.push_back(*it_a);
          ++it_a;
          continue;
        }
      if(*it_b < *it_a)
        {
          result.terms.push_back(*it_b);
          ++it_b;
          continue;
        }
      if(*it_a == *it_b)
        {
          result.terms.push_back(*it_a);
          result.terms.back().coefficient += it_b->coefficient;
          ++it_a;
          ++it_b;
          continue;
        }
    }
  while(it_a != terms.end())
    {
      result.terms.push_back(*it_a);
      ++it_a;
    }
  while(it_b != b.terms.end())
    {
      result.terms.push_back(*it_b);
      ++it_b;
    }
  result.clean();
  return result;
}

Multivariate Multivariate::operator-() const
{
  Multivariate result(*this);
  for(auto &term : result.terms)
    {
      term.coefficient = -term.coefficient;
    }
  return result;
}

Multivariate Multivariate::operator-(const Multivariate &b) const
{
  Multivariate result;
  std::list<Term>::const_iterator it_a = terms.begin(), it_b = b.terms.begin();
  while(it_a != terms.end() && it_b != b.terms.end())
    {
      if(*it_a < *it_b)
        {
          result.terms.push_back(*it_a);
          ++it_a;
          continue;
        }
      if(*it_b < *it_a)
        {
          result.terms.push_back(*it_b);
          result.terms.back().coefficient = -result.terms.back().coefficient;
          ++it_b;
          continue;
        }
      if(*it_a == *it_b)
        {
          result.terms.push_back(*it_a);
          result.terms.back().coefficient -= it_b->coefficient;
          ++it_a;
          ++it_b;
          continue;
        }
    }
  while(it_a != terms.end())
    {
      result.terms.push_back(*it_a);
      ++it_a;
    }
  while(it_b != b.terms.end())
    {
      result.terms.push_back(*it_b);
      result.terms.back().coefficient = -result.terms.back().coefficient;
      ++it_b;
    }
  result.clean();
  return result;
}

Multivariate Multivariate::operator*(const Multivariate &b) const
{
  std::vector<Term> tmp;
  tmp.reserve(terms.size() * b.terms.size());

  for(auto &term_a : terms)
    for(auto &term_b : b.terms)
      {
        tmp.push_back(term_a * term_b);
      }

  std::sort(tmp.begin(), tmp.end());

  Multivariate result;
  std::vector<Term>::const_iterator i = tmp.begin();
  result.terms.push_back(*i);
  ++i;
  while(i != tmp.end())
    {
      if(*i == result.terms.back())
        {
          result.terms.back().coefficient += i->coefficient;
        }
      else
        {
          result.terms.push_back(*i);
        }
      ++i;
    }
  result.clean();
  return result;
}

void Multivariate::clean()
{
  std::list<Multivariate::Term>::iterator i = terms.begin();
  while(i != terms.end())
    {
      if(i->coefficient.is_zero())
        {
          terms.erase(i++);
        }
      else
        {
          ++i;
        }
    }
  if(terms.empty())
    {
      terms.push_back(Multivariate::Term(0));
    }
}

Multivariate Multivariate::operator^(uint64_t pow) const
{
  Multivariate result(int64_t(1));
  Multivariate power(*this);
  while(pow != 0)
    {
      if(pow % 2 == 1)
        {
          result = result * power;
        }
      pow = pow / 2;
      if(pow != 0)
        power = power * power;
    }
  return result;
}

bool Multivariate::operator==(const Multivariate &rhs) const
{
  if(terms.size() != rhs.terms.size())
    return false;
  std::list<Term>::const_iterator left(terms.begin()),
    right(rhs.terms.begin());
  while(left != terms.end())
    {
      if(*left != *right || left->coefficient != right->coefficient)
        return false;
    }
  return true;
}

Multivariate
Multivariate::specialize(const std::map<std::string, Bigfloat> &values) const
{
  std::vector<Multivariate::Term> specialized_terms;
  specialized_terms.reserve(terms.size());

  for(std::list<Term>::const_iterator it = terms.begin(); it != terms.end();
      it++)
    {
      specialized_terms.emplace_back(it->coefficient);
      for(auto &factor : it->exponents)
        {
          std::map<std::string, Bigfloat>::const_iterator value
            = values.find(factor.first);
          if(value != values.end())
            {
              specialized_terms.back().coefficient
                *= pow(value->second, factor.second);
            }
          else
            {
              specialized_terms.back().exponents[factor.first] = factor.second;
            }
        }
    }

  std::sort(specialized_terms.begin(), specialized_terms.end());

  Multivariate result;
  result.terms.push_back(specialized_terms.at(0));
  std::vector<Term>::const_iterator term = specialized_terms.begin();
  term++;
  while(term != specialized_terms.end())
    {
      if(*term == result.terms.back())
        {
          result.terms.back().coefficient += term->coefficient;
        }
      else
        {
          result.terms.push_back(*term);
        }
      term++;
    }
  return result;
}

Multivariate
Multivariate::shift_variable(const std::string &var, Bigfloat sh) const
{
  Multivariate result(int64_t(0));
  Multivariate to_add(int64_t(1));
  for(const auto &term : terms)
    {
      int64_t exp = term.exponent(var);
      if(exp > 0)
        {
          for(int64_t k = 0; k <= exp; ++k)
            {
              to_add.terms.begin()->exponents = term.exponents;
              to_add.terms.begin()->exponents[var] = k;
              to_add.terms.begin()->coefficient
                = term.coefficient * binomial_coefficient(exp, k)
                  * pow(sh, exp - k);
              result = result + to_add;
            };
        }
      else
        {
          *to_add.terms.begin() = term;
          result = result + to_add;
        }
    }
  return result;
}

int64_t Multivariate::degree(const std::string &var) const
{
  int64_t result = 0;
  for(const auto &term : terms)
    {
      int64_t exp = term.exponent(var);
      if(exp > result)
        result = exp;
    }
  return result;
}

Polynomial<Bigfloat> Multivariate::to_poly() const
{
  Polynomial<Bigfloat> result;
  std::string varname = "";
  bool varname_set(false);
  int64_t degree = 0;
  for(const auto &term : terms)
    {
      if(term.exponents.size() > 1)
        throw std::runtime_error("Trying to cast Multivatiate with more than "
                                 "one variable to Polynomial.");
      if(term.exponents.size() == 1)
        {
          if(!varname_set)
            {
              varname = term.exponents.begin()->first;
              varname_set = true;
              if(degree < term.exponents.begin()->second)
                degree = term.exponents.begin()->second;
            }
          else
            {
              if(varname != term.exponents.begin()->first)
                throw std::runtime_error(
                  "Trying to cast Multivatiate with more than "
                  "one variable to Polynomial.");
              if(degree < term.exponents.begin()->second)
                degree = term.exponents.begin()->second;
            }
        }
    }

  result.data().resize(degree + 1);
  for(const auto &term : terms)
    {
      result[term.exponent(varname)] = term.coefficient;
    }

  return result;
}

std::ostream &operator<<(std::ostream &out, const Multivariate::Term &term)
{
  if(term.coefficient < 0)
    {
      out << "- " << -term.coefficient;
    }
  else
    {
      out << term.coefficient;
    }

  for(const auto &exponent : term.exponents)
    {
      out << "*" << exponent.first;
      if(exponent.second > 1)
        {
          out << "^" << exponent.second;
        }
    }

  return out;
}

std::ostream &operator<<(std::ostream &out, const Multivariate &poly)
{
  std::list<Multivariate::Term>::const_iterator term = poly.terms.begin();
  while(term != poly.terms.end())
    {
      if(term == poly.terms.begin())
        {
          out << *term;
        }
      else
        {
          if(term->coefficient >= 0)
            out << " + ";
          else
            out << " ";
          out << *term;
        }
      ++term;
    }
  return out;
}

Multivariate operator+(int val, const Multivariate &poly)
{
  return Multivariate(int64_t(val)) + poly;
}

Multivariate operator+(double val, const Multivariate &poly)
{
  return Multivariate((int64_t)val) + poly;
}

Multivariate operator-(int val, const Multivariate &poly)
{
  return Multivariate((int64_t)val) - poly;
}

Multivariate operator-(double val, const Multivariate &poly)
{
  return Multivariate(val) - poly;
}

Multivariate operator*(int val, const Multivariate &poly)
{
  return Multivariate((int64_t)val) * poly;
}

Multivariate operator*(double val, const Multivariate &poly)
{
  return Multivariate(val) * poly;
}
