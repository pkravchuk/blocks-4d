#pragma once

#include <boost/math/tools/polynomial.hpp>

template <typename T> using Polynomial = boost::math::tools::polynomial<T>;

template <typename T>
Polynomial<T> shift_polynomial(const Polynomial<T>& poly, const T& shift)
{
  Polynomial<T> result(0);
  Polynomial<T> offset_atom({shift, T(1)});
  Polynomial<T> offset_power({T(1)});
  for(size_t power(0); power < poly.size(); ++power)
    {
      result += poly[power] * offset_power;
      offset_power *= offset_atom;
    }
  return result;
}