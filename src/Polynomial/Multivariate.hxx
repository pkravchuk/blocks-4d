#pragma once

#include <map>
#include <string>
#include <list>
#include <ostream>
#include "../Bigfloat.hxx"
#include "Polynomial.hxx"

struct Multivariate
{
  struct Term
  {
    Term() {};
    Term(const Bigfloat &);
    Term operator*(const Term &)const;
    bool operator<(const Term &) const;
    bool operator>(const Term &) const;
    bool operator<=(const Term &) const;
    bool operator>=(const Term &) const;
    bool operator==(const Term &) const;
    bool operator!=(const Term &) const;

    int64_t exponent(const std::string &) const;

    std::map<std::string, int64_t> exponents; // Should not have exponents = 0
    Bigfloat coefficient;
  };

  Multivariate() {};
  Multivariate(const Term &);
  Multivariate(const Bigfloat &);
  Multivariate(const std::string &var);
  Multivariate(const char *var);
  Multivariate(int64_t var);
  Multivariate(double var);

  Multivariate operator+(const Multivariate &) const;
  Multivariate operator-() const;
  Multivariate operator-(const Multivariate &) const;
  Multivariate operator*(const Multivariate &)const;
  Multivariate operator^(uint64_t) const;

  bool operator==(const Multivariate &) const;

  Multivariate specialize(const std::map<std::string, Bigfloat> &) const;
  Multivariate shift_variable(const std::string&, Bigfloat) const;
  Polynomial<Bigfloat> to_poly() const;

  int64_t degree(const std::string &) const;

  std::list<Term> terms; // Must always be sorted

  void clean();
};

std::ostream &operator<<(std::ostream &, const Multivariate &);
std::ostream &operator<<(std::ostream &, const Multivariate::Term &);

Multivariate operator+(int, const Multivariate &);
Multivariate operator+(double, const Multivariate &);
Multivariate operator-(int, const Multivariate &);
Multivariate operator-(double, const Multivariate &);
Multivariate operator*(int, const Multivariate &);
Multivariate operator*(double, const Multivariate &);
