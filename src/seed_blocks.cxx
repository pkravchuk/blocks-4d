#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "parsers/parsers.hxx"
#include "table_io/table_io.hxx"
#include "DerivativeTable/SeedTable.hxx"
#include "seed_recurse/seed_block_descriptions.hxx"
#include "seed_recurse/seed_recurse.hxx"
#include "Timers.hxx"

Timers *global_timers;

namespace po = boost::program_options;

std::vector<int64_t>
parse_spin_ranges(const std::string &spin_ranges_string);

int main(int argc, char *argv[])
{
  try
    {
      boost::filesystem::path input_dir, output_dir;
      int64_t perm, p, num_threads, precision_binary, order, kept_pole_order,
        lambda;
      std::string block_type_string, Delta_12_string, Delta_34_string,
        spins_string;
      bool debug(false);

      po::options_description desc("Allowed options");
      desc.add_options()("help", "Show this message.")(
        "perm", po::value<int64_t>(&perm)->default_value(1234),
        "The permutation type of the seed (1234 or 1243).")(
        "type", po::value<std::string>(&block_type_string)->required(),
        "Seed type (primal or dual).")("p", po::value<int64_t>(&p)->required(),
                                       "p")(
        "delta-12", po::value<std::string>(&Delta_12_string)->required(),
        "-2*a_p")("delta-34",
                  po::value<std::string>(&Delta_34_string)->required(),
                  "2*b_p")
        ("threads", po::value<int64_t>(&num_threads)->default_value(1),
         "Number of threads to use.")(
          "precision", po::value<int64_t>(&precision_binary)->required(),
          "The precision, in the number of bits, to use in the computation.")(
          "order", po::value<int64_t>(&order)->required(),
          "The recursion order of the scalar blocks to use.")(
          "poles", po::value<int64_t>(&kept_pole_order)->required(),
          "The order of kept poles of the scalar blocks ot use (aka "
          "keptPoleOrder).")(
          "spin-ranges", po::value<std::string>(&spins_string)->required(),
          "Comma-separated list of ranges of spins to compute.")(
          "lambda", po::value<int64_t>(&lambda)->required(),
          "Derivative order to compute. "
          "The scalar blocks used are those which match other parameters and "
          "have "
          "lambda=2*n_max-1 at least lambda+4*p.")(
          ",i", po::value<boost::filesystem::path>(&input_dir)->required(),
          "Input directory that contains scalar block files.")(
          ",o", po::value<boost::filesystem::path>(&output_dir)->required(),
          "Output directory.")(
          "debug", po::bool_switch(&debug), "Enable some debug output");

      po::variables_map variables_map;

      po::store(po::parse_command_line(argc, argv, desc), variables_map);
      if(variables_map.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }
      po::notify(variables_map);

      const size_t precision_base_10(
        boost::multiprecision::detail::digits2_2_10(precision_binary));

      boost::multiprecision::mpf_float::default_precision(precision_base_10);

      global_timers = new Timers(debug);


      std::vector<int64_t> spins(parse_spin_ranges(spins_string));

      int64_t block_type;
      if(block_type_string == "primal")
        block_type = 0;
      else if(block_type_string == "dual")
        block_type = 1;
      else
        throw std::runtime_error("unrecognized seed type");

      Timer& total = global_timers->add_and_start("Total");
      write_seeds(input_dir, output_dir, block_type, perm, p, spins,
                  Delta_12_string, Delta_34_string, lambda, kept_pole_order,
                  order, num_threads);
      total.stop();

      if(debug)
        global_timers->write_profile(output_dir / "profile.m");

      delete global_timers;
    }
  catch(std::exception &e)
    {
      std::cout << "Error: " << e.what() << std::endl;
      exit(1);
    }
  catch(...)
    {
      std::cout << "Unknown error." << std::endl;
      exit(1);
    }

  return 0;
}
