#include <boost/algorithm/string.hpp>

#include <set>
#include <vector>
#include <string>

std::vector<int64_t> parse_spin_ranges(const std::string &spin_ranges_string)
{
  std::set<int64_t> result_set;
  std::vector<std::string> spin_range_elements;
  boost::split(spin_range_elements, spin_ranges_string, boost::is_any_of(","));
  for(auto &spin_range : spin_range_elements)
    {
      std::vector<std::string> spin_numbers;
      boost::split(spin_numbers, spin_range, boost::is_any_of("-"));
      if(spin_numbers.empty())
        {
          throw std::runtime_error("Invalid spin-range.  It is empty or "
                                   "composed entirely of dashes: '"
                                   + spin_range + "'");
        }
      if(spin_numbers.size() == 1)
        {
          result_set.insert(std::stoi(spin_numbers.front()));
        }
      else if(spin_numbers.size() == 2)
        {
          int64_t current(std::stoi(spin_numbers.front())),
            finish(std::stoi(spin_numbers.back()));
          if(finish < current)
            {
              throw std::runtime_error(
                "Invalid range.  The number after the dash must be larger "
                "than the number before the dash: '"
                + spin_range + "'");
            }
          for(; current <= finish; ++current)
            {
              result_set.insert(current);
            }
        }
      else
        {
          throw std::runtime_error("Invalid spin-range.  It is empty or "
                                   "composed entirely of dashes: '"
                                   + spin_range + "'");
        }
    }
  if(result_set.empty())
    {
      throw std::runtime_error("Invalid spin-range.  It is empty: '"
                               + spin_ranges_string + "'");
    }

  std::vector<int64_t> result(result_set.size());
  
  std::set<int64_t>::const_iterator i = result_set.begin();
  std::vector<int64_t>::iterator j = result.begin();

  while(i != result_set.end())
  {
    *j = *i;
    ++i;
    ++j;
  }

  return result;
}
