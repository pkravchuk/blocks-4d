#include <istream>
#include <sstream>

#include "read_token.hxx"

inline bool isoperator(char symbol)
{
  return (symbol == '*' || symbol == '/' || symbol == '^' || symbol == '+'
          || symbol == '-' || symbol == '(' || symbol == ')');
}

std::pair<std::string, TokenType> read_token(std::istream &stream)
{
  char symbol = ' ';
  std::stringstream result;

  while(std::isspace(symbol) && !stream.eof())
    stream.get(symbol);

  if(stream.eof())
    return std::make_pair("", Eof);

  result << symbol;

  if(isoperator(symbol))
    {
      return std::make_pair(result.str(), Operator);
    }

  // If we reach here, we must be parsing either a symbol name (anything
  // starting with alpha until whitespace or an operator) or a number

  if(std::isalpha(symbol))
    {
      while(true)
        {
          stream.get(symbol);
          if(stream.eof())
            break;
          if(std::isspace(symbol) || isoperator(symbol))
            {
              stream.unget();
              break;
            }
          result << symbol;
        }
      return std::make_pair(result.str(), Identifier);
    }

  if(std::isdigit(symbol))
    {
      uint64_t dot_count = 0;
      while(true)
        {
          stream.get(symbol);
          if(stream.eof())
            break;
          if(!(std::isdigit(symbol) || symbol == '.'))
            {
              stream.unget();
              break;
            }
          result << symbol;
          if(symbol == '.')
            {
              ++dot_count;
              if(dot_count > 1)
                throw std::runtime_error(
                  std::string("Unexpected '.' in a number: ") + result.str());
            }
        }
      return std::make_pair(result.str(), Number);
    }

  throw std::runtime_error(std::string("Unexpected symbol when parsing: ")
                           + symbol);
}