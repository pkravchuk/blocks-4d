#include <stack>
#include <queue>

#include "read_token.hxx"
#include "shunting_yard.hxx"

uint64_t precedence(std::string op_string)
{
  char op = op_string.at(0);
  switch(op)
    {
    case '+':
    case '-': return 0;
    case '*':
    case '/': return 1;
    case '^': return 2;
    case '_': // This is unary minus
      return 3;
    default: throw std::runtime_error("Unknown operator");
    }
}

bool update_unary_minus_context(std::pair<std::string, TokenType> token)
{
  if(token.second == Number || token.second == Identifier)
    return false;
  // Assuming not EOF token
  if(token.first.at(0) == ')')
    return false;
  return true;
}

// void parse_rational_function(std::istream &input)
// {
//   std::pair<std::string, TokenType> token;
//   std::queue<std::string> result;
//   std::stack<std::string> operator_stack;

//   bool unary_minus_context = true;

//   while(true)
//     {
//       token = read_token(input);
//       if(token.second == Number | token.second == Identifier)
//         {
//           result.push(token.first);
//         }
//       if(token.second == Operator)
//         {
//           if((token.first.at(0) == '-') && unary_minus_context)
//             token.first.at(0) = '_'; // Internal only notation for unary minus

//           if(token.first.at(0) != '(' && token.first.at(0) != ')')
//             {
//               while(
//                 operator_stack.size() > 0 && operator_stack.top().at(0) != '('
//                 && precedence(operator_stack.top()) >= precedence(token.first))
//                 {
//                   result.push(operator_stack.top());
//                   operator_stack.pop();
//                 }
//               operator_stack.push(token.first);
//             }
//           if(token.first.at(0) == '(')
//             {
//               operator_stack.push(token.first);
//             }
//           if(token.first.at(0) == ')')
//             {
//               while(operator_stack.size() > 0
//                     && operator_stack.top().at(0) != '(')
//                 {
//                   result.push(operator_stack.top());
//                   operator_stack.pop();
//                 }
//               if(operator_stack.size() == 0)
//                 throw std::runtime_error("Unmatched parenthesis");

//               // We can only end up here if '(' is a the top
//               operator_stack.pop();
//             }
//         }
//       if(token.second == Eof)
//         {
//           while(operator_stack.size() > 0)
//             {
//               result.push(operator_stack.top());
//               operator_stack.pop();
//             }
//           break;
//         }
//       unary_minus_context = update_unary_minus_context(token);
//     }
//   while(result.size() > 0)
//     {
//       std::cout << result.front() << " ";
//       result.pop();
//     }
// }

void apply_operator(std::stack<RationalFunction> &stack, char op)
{
  if(op == '_')
    {
      if(stack.empty())
        throw std::runtime_error("Parsing error: no operand for unary -.");

      RationalFunction operand(std::move(stack.top()));
      stack.pop();
      stack.push(-operand);
      return;
    }

  if(stack.size() < 2)
    throw std::runtime_error("Parsing error: not enough operands");

  RationalFunction operand2(std::move(stack.top()));
  stack.pop();
  RationalFunction operand1(std::move(stack.top()));
  stack.pop();

  switch(op)
    {
    case '+': stack.push(operand1 + operand2); return;
    case '-': stack.push(operand1 - operand2); return;
    case '*': stack.push(operand1 * operand2); return;
    case '/': stack.push(operand1 / operand2); return;
    case '^':
      int64_t power = round_bigfloat(
        operand2.value.first.terms.begin()
          ->coefficient); // Going to get very unexpected results if not using
                          // powers that are explicitly integers.
      stack.push(operand1 ^ power);
      return;
    }
}

RationalFunction parse_rational_function(std::istream &input)
{
  std::pair<std::string, TokenType> token;
  std::stack<RationalFunction> result;
  std::stack<std::string> operator_stack;

  bool unary_minus_context = true;

  while(true)
    {
      token = read_token(input);
      if(token.second == Number)
        {
          result.push(Multivariate(Bigfloat(token.first)));
        }
      if(token.second == Identifier)
        {
          result.push(Multivariate(token.first));
        }
      if(token.second == Operator)
        {
          if((token.first.at(0) == '-') && unary_minus_context)
            token.first.at(0) = '_'; // Internal only notation for unary minus

          if(token.first.at(0) != '(' && token.first.at(0) != ')')
            {
              while(
                operator_stack.size() > 0 && operator_stack.top().at(0) != '('
                && precedence(operator_stack.top()) >= precedence(token.first))
                {
                  apply_operator(result, operator_stack.top().at(0));
                  operator_stack.pop();
                }
              operator_stack.push(token.first);
            }
          if(token.first.at(0) == '(')
            {
              operator_stack.push(token.first);
            }
          if(token.first.at(0) == ')')
            {
              while(operator_stack.size() > 0
                    && operator_stack.top().at(0) != '(')
                {
                  apply_operator(result, operator_stack.top().at(0));
                  operator_stack.pop();
                }
              if(operator_stack.size() == 0)
                throw std::runtime_error("Unmatched parenthesis");

              // We can only end up here if '(' is a the top
              operator_stack.pop();
            }
        }
      if(token.second == Eof)
        {
          while(operator_stack.size() > 0)
            {
              apply_operator(result, operator_stack.top().at(0));
              operator_stack.pop();
            }
          break;
        }
      unary_minus_context = update_unary_minus_context(token);
    }

  if(result.size() != 1)
    throw std::runtime_error("Couldn't parse the complete expression");

  return result.top();
}