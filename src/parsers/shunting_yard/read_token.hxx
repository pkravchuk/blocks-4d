#include <istream>

enum TokenType
{
  Eof,
  Operator,
  Identifier,
  Number
};

std::pair<std::string, TokenType> read_token(std::istream &stream);
