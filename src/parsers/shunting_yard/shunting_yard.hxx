#pragma once

#include <istream>

#include "../../Polynomial/RationalFunction.hxx"

//void parse_rational_function(std::istream &);
RationalFunction parse_rational_function(std::istream &input);
