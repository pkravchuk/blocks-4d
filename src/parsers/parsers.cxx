#include <fstream>
#include <sstream>

#include "parsers.hxx"

#include "parsers.hxx"
//#include "polynomial.hxx"
//#include "derivative_table.hxx"
//#include "block_description.hxx"

std::string parsers::get_file_contents(const std::string &filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if(in)
    {
      std::string contents;
      in.seekg(0, std::ios::end);
      contents.resize(in.tellg());
      in.seekg(0, std::ios::beg);
      in.read(&contents[0], contents.size());
      in.close();
      return (contents);
    }
  throw std::runtime_error("Couldn't open file " + filename);
}

// Multivariate parsers::parse_polynomial(const std::string &str)
// {
//   Multivariate result;
//   bool succ
//     = x3::phrase_parse(str.begin(), str.end(), multivariate_parser::polynomial,
//                        x3::ascii::space, result);
//   if(succ)
//     return result;
//   throw std::runtime_error("Couldn't parse polynomial " + str);
// }

// DerivativeTable parsers::parse_derivative_table(const std::string &str)
// {
//   DerivativeTable result;
//   bool succ = x3::phrase_parse(str.begin(), str.end(),
//                                derivative_table_parser::der_table,
//                                parser_util::skipper, result);
//   if(succ)
//     return result;
//   throw std::runtime_error("Couldn't parse derivative table");
// }

// DerivativeTable
// parsers::parse_derivative_table_file(const std::string &filename)
// {
//   std::string str(get_file_contents(filename));
//   try
//     {
//       return parse_derivative_table(str);
//     }
//   catch(std::exception e)
//     {
//       throw std::runtime_error("Couldn't parse derivative table from "
//                                + filename
//                                + "\n Original exception: " + e.what());
//     }
// }

BlockDescription fast_parse_block_description(std::istream &input);

BlockDescription parsers::parse_block_description(const std::string &str)
{
  std::istringstream ss(str);
  return fast_parse_block_description(ss);
}

BlockDescription
parsers::parse_block_description_file(const std::string &filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if(in)
    {
      BlockDescription result;
      result=fast_parse_block_description(in);
      in.close();
      return result;
    }
  throw std::runtime_error("Couldn't open file " + filename);
}
