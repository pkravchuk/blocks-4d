#pragma once

#include <string>
#include <map>

#include "../Polynomial/Multivariate.hxx"
#include "../DerivativeTable/DerivativeTable.hxx"
#include "../BlockDescription/BlockDescription.hxx"
#include "../DerivativeTable/SeedTable.hxx"

namespace parsers
{
  std::string get_file_contents(const std::string &filename);
  Multivariate parse_polynomial(const std::string &);
  BlockDescription parse_block_description_file(const std::string &filename);
  BlockDescription parse_block_description(const std::string &description);
  SeedTable parse_seed_table(std::string &file_contents);
  SeedTable parse_seed_table_file(const std::string &filename);
  std::map<std::string, Bigfloat> parse_lookup(const std::string &json);
  std::map<std::string, Bigfloat> parse_lookup_file(const std::string &filename);
};
