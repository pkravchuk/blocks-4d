#include <list>
#include <istream>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "../Bigfloat.hxx"
#include "../Polynomial/Polynomial.hxx"
#include "../DerivativeTable/SeedTable.hxx"
#include "../Timers.hxx"
#include "parsers.hxx"

Polynomial<Bigfloat>
parse_polynomial(std::string &data, size_t begin, size_t end);

SeedTable parsers::parse_seed_table_file(const std::string &filename)
{
  std::string str(get_file_contents(filename));
  try
    {
      return parse_seed_table(str);
    }
  catch(std::exception e)
    {
      throw std::runtime_error("Couldn't parse derivative table from "
                               + filename
                               + "\n Original exception: " + e.what());
    }
}

// messes up file_contents
SeedTable parsers::parse_seed_table(std::string &file_contents)
{
  // Timer &total = global_timers->add_and_start("parse_seed_table");

  SeedTable result;

  std::list<std::pair<size_t, size_t>> entries;
  std::map<
    std::string,
    std::list<std::pair<std::pair<int64_t, int64_t>, Polynomial<Bigfloat>>>>
    data;

  // boost::split(entries, file_contents, boost::is_any_of("{,}"));

  size_t token_start(0), token_end(0);
  int64_t square_braket_level(0);
  for(size_t i = 0; i < file_contents.length(); ++i)
    {
      if(file_contents.at(i) == '[')
        ++square_braket_level;
      if(file_contents.at(i) == ']')
        --square_braket_level;
      if(file_contents.at(i) == '{')
        token_start = i + 1;

      if(file_contents.at(i) == '}'
         || (file_contents.at(i) == ',' && square_braket_level == 0))
        {
          token_end = i - 1;
          entries.emplace_back(
            // file_contents.substr(token_start, token_end - token_start + 1)
            std::make_pair(token_start, token_end));
          token_start = i + 1;
          token_end = token_start;
        }
    }

  for(const auto &entry : entries)
    {
      size_t name_start(entry.first), name_end(0);
      while(file_contents.at(name_start) == ' '
            || file_contents.at(name_start) == '\n')
        ++name_start;
      name_end = name_start;
      while(file_contents.at(name_end) != '[')
        ++name_end;

      std::string name
        = file_contents.substr(name_start, name_end - name_start);

      size_t int_begin = name_end;
      while(!std::isdigit(file_contents.at(int_begin)))
        ++int_begin;
      size_t int_end = int_begin;
      while(std::isdigit(file_contents.at(int_end)))
        ++int_end;

      int64_t m = boost::lexical_cast<int64_t>(
        file_contents.c_str() + int_begin, int_end - int_begin);

      int_begin = int_end + 1;
      while(!std::isdigit(file_contents.at(int_begin)))
        ++int_begin;
      int_end = int_begin;
      while(std::isdigit(file_contents.at(int_end)))
        ++int_end;

      int64_t n = boost::lexical_cast<int64_t>(
        file_contents.c_str() + int_begin, int_end - int_begin);

      // std::cout << name << " " << m << " " << n << std::endl;

      size_t poly_begin = int_end;
      while(file_contents.at(poly_begin) != '>')
        ++poly_begin;
      // std::string poly = file_contents.substr(
      //   poly_begin + 1, entry.second - poly_begin - 1 + 1);

      // std::cout << name << " " << m << " " << n << std::endl;
      // std::cout << "Poly:" << std::endl;
      // std::cout << parse_polynomial(file_contents, poly_begin + 1,
      // entry.second) << std::endl;
      data[name].emplace_back(
        std::make_pair(m, n),
        ::parse_polynomial(file_contents, poly_begin + 1, entry.second));
    }

  for(auto &block : data)
    {
      size_t lambda = 0;
      for(auto &derivative : block.second)
        {
          size_t curr_lambda
            = derivative.first.first + 2 * derivative.first.second;
          if(curr_lambda > lambda)
            lambda = curr_lambda;
        }
      auto &dertable = result.derivatives[block.first];
      dertable.resize(lambda + 1);
      for(size_t m = 0; m <= lambda; m++)
        {
          dertable.at(m).resize((lambda - m) / 2 + 1);
        }
      if(block.first != "abDeriv")
        {
          for(auto &derivative : block.second)
            {
              dertable.at(derivative.first.first).at(derivative.first.second)
                = std::move(derivative.second);
            }
        }
      else
        {
          for(auto &derivative : block.second)
            {
              dertable.at(derivative.first.first).at(derivative.first.second)
                = derivative.second
                  * pow(Bigfloat(2),
                        derivative.first.first + 2 * derivative.first.second);
            }
        }
    }

  size_t p = result.derivatives.size();
  if(p == 0)
    throw std::runtime_error("No blocks parsed");
  --p;

  result.lookup.resize(p + 1);
  result.p = p;

  if(p > 0)
    {
      for(size_t e = 0; e <= p; ++e)
        {
          std::stringstream ss;
          ss << "seedBlockE" << e;
          result.lookup[e] = &result.derivatives[ss.str()];
        }
    }
  else
    {
      result.lookup[0] = &(result.derivatives.begin()->second);
    }

  // total.stop();

  // std::cout << result << std::endl;

  return result;
}

// return start and end of first and last non-space chars of the first token at
// or after pos
std::pair<size_t, size_t>
find_next_token(const std::string &data, size_t pos, size_t end)
{
  size_t first;
  while(pos < end + 1 && std::isspace(data.at(pos)))
    ++pos;
  if(pos >= end + 1)
    return std::make_pair(end + 1, end + 1);

  first = pos;
  char c = data.at(pos);

  if(std::isdigit(c) || c == '.')
    {
      // current token is a number
      while(pos < end + 1 && (std::isdigit(c) || c == '.'))
        {
          ++pos;
          c = data.at(pos);
        }
      return std::make_pair(first, pos - 1);
    }
  if(c == 'x' || c == '+' || c == '-' || c == '^' || c == '*')
    {
      return std::make_pair(first, first);
    }

  throw std::runtime_error(
    "Unrecognzied token in polynomial starting at "
    + data.substr(pos, std::min(static_cast<size_t>(10), end - pos + 1)));
}

Polynomial<Bigfloat>
parse_polynomial(std::string &data, size_t begin, size_t end)
{
  std::list<Bigfloat> coefficients;
  std::pair<size_t, size_t> tok;
  tok = find_next_token(data, begin, end);
  bool minus(false), power(false);
  Bigfloat minus_one("-1");
  while(tok.first <= end)
    {
      char start = data.at(tok.first);
      if(start == '-')
        {
          minus = true;
          tok = find_next_token(data, tok.second + 1, end);
          continue;
        }
      if(start == '^')
        {
          power = true;
          tok = find_next_token(data, tok.second + 1, end);
          continue;
        }
      if(start == 'x' || start == '*' || start == '+')
        {
          tok = find_next_token(data, tok.second + 1, end);
          continue;
        }
      if(std::isdigit(start) || start == '.')
        {
          if(power)
            {
              power = false;
              tok = find_next_token(data, tok.second + 1, end);
              continue;
            }
          char c;
          c = data.at(tok.second + 1);
          data.at(tok.second + 1) = 0;
          coefficients.emplace_back(data.c_str() + tok.first);
          data.at(tok.second + 1) = c;
          if(minus)
            {
              coefficients.back() *= minus_one;
              minus = false;
            }
          tok = find_next_token(data, tok.second + 1, end);
          continue;
        }
      throw std::runtime_error(
        "Unrecognzied token in polynomial starting at "
        + data.substr(tok.first, tok.second - tok.first + 1));
    }

  Polynomial<Bigfloat> result;
  result.data().reserve(coefficients.size());
  for(auto &coeff : coefficients)
    {
      result.data().emplace_back(std::move(coeff));
    }
  return result;
}