#include <sstream>
#include <istream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "parsers.hxx"

namespace pt = boost::property_tree;

std::map<std::string, Bigfloat> parse_lookup_stream(std::istream &json)
{
  std::map<std::string, Bigfloat> result;
  pt::ptree tree;

  pt::read_json(json, tree);

  for(const auto &child : tree)
    {
      result[child.first] = Bigfloat(child.second.data());
    }

  return result;
}

std::map<std::string, Bigfloat> parsers::parse_lookup(const std::string &json)
{
  std::stringstream ss(json);

  return parse_lookup_stream(ss);
}

std::map<std::string, Bigfloat>
parsers::parse_lookup_file(const std::string &filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);

  if(in)
    {
      return parse_lookup_stream(in);
    }

  throw std::runtime_error("Couldn't open file " + filename);
}
