#include <istream>
#include <sstream>

#include "../BlockDescription/BlockDescription.hxx"

#include "../rapidjson/document.h"
#include "../rapidjson/document.h"
#include "../rapidjson/istreamwrapper.h"

#include "shunting_yard/shunting_yard.hxx"

void read_string(rapidjson::Value &obj, const char *name, std::string &target,
                 bool optional = true)
{
  if(!obj.HasMember(name))
    {
      if(optional)
        return;
      else
        throw std::runtime_error(
          std::string("Error parsing block description: ") + name
          + " must be defined.");
    }

  if(!obj[name].IsString())
    throw std::runtime_error(std::string("Error parsing block description: ")
                             + name + " must be a string.");
  target = obj[name].GetString();
}

bool read_int(rapidjson::Value &obj, const char *name, int64_t &target,
              bool optional = true)
{
  if(!obj.HasMember(name))
    {
      if(optional)
        return false;
      else
        throw std::runtime_error(
          std::string("Error parsing block description: ") + name
          + " must be defined.");
    }

  if(!obj[name].IsInt64())
    throw std::runtime_error(std::string("Error parsing block description: ")
                             + name + " must be an integer.");
  target = obj[name].GetInt64();
  return true;
}

void read_expression(rapidjson::Value &obj, const char *name,
                     RationalFunction &target, bool optional = true)
{
  if(!obj.HasMember(name))
    {
      if(optional)
        return;
      else
        throw std::runtime_error(
          std::string("Error parsing block description: ") + name
          + " must be defined.");
    }
  if(!obj[name].IsString())
    throw std::runtime_error(std::string("Error parsing block description: ")
                             + name
                             + " must be a string containing an expression.");

  std::istringstream expression(obj[name].GetString());
  target = parse_rational_function(expression);
}

void read_expression_array(rapidjson::Value &obj, const char *name,
                           std::vector<RationalFunction> &target,
                           bool optional = true)
{
  if(!obj.HasMember(name))
    {
      if(optional)
        return;
      else
        throw std::runtime_error(
          std::string("Error parsing block description: ") + name
          + " must be defined.");
    }

  if(!obj[name].IsArray())
    throw std::runtime_error(std::string("Error parsing block description: ")
                             + name + " must be a list of expressions.");

  target.reserve(obj[name].Size());

  for(auto &m : obj[name].GetArray())
    {
      if(!m.IsString())
        throw std::runtime_error(
          std::string("Error parsing block description: elements of ") + name
          + " must be strings containing expressions.");
      std::istringstream expression(m.GetString());
      target.push_back(parse_rational_function(expression));
    }
}

void read_type(rapidjson::Value &obj, const char *name, int64_t &target,
               bool optional = true)
{
  if(!obj.HasMember(name))
    {
      if(optional)
        return;
      else
        throw std::runtime_error(
          std::string("Error parsing block description: ") + name
          + " must be defined.");
    }

  if(!obj[name].IsString())
    throw std::runtime_error(std::string("Error parsing block description: ")
                             + name + " be either \"primal\" or \"dual\".");
  std::string type_str = obj[name].GetString();
  if(type_str == "primal")
    target = 0;
  else if(type_str == "dual")
    target = 1;
  else
    throw std::runtime_error(std::string("Error parsing block description: ")
                             + name + " be either \"primal\" or \"dual\".");
}

BlockDescription fast_parse_block_description(std::istream &input)
{
  rapidjson::Document block_descr;
  rapidjson::IStreamWrapper isw(input);

  block_descr.ParseStream<rapidjson::kParseCommentsFlag>(isw);

  BlockDescription result;
  result.description.SetObject();

  if(block_descr.HasMember("description"))
    {
      result.description.AddMember("description", block_descr["description"],
                                   result.description.GetAllocator());
    }

  read_string(block_descr, "dimension_var", result.dimensionParameter);
  read_string(block_descr, "spin_var", result.spinParameter);
  read_string(block_descr, "x_var", result.xParameter);
  read_string(block_descr, "t_var", result.tParameter);

  read_expression_array(block_descr, "poles", result.poles);
  read_expression_array(block_descr, "spurious_poles", result.spuriousPoles);

  if(block_descr.HasMember("poles_L"))
    {
      if(!block_descr["poles_L"].IsArray())
        throw std::runtime_error("Error parsing block description: "
                                 "poles_L must be a list.");

      for(auto &m : block_descr["poles_L"].GetArray())
        {
          if(!m.HasMember("L") || !m.HasMember("poles") || !m["L"].IsInt64()
             || !m["poles"].IsArray())
            throw std::runtime_error(
              "Error parsing block description: "
              "elements of poles_L must contain keys \"L\" (with "
              "integer value) and \"poles\" (list of expressions)");
          auto &poles = result.polesL[m["L"].GetInt64()];
          read_expression_array(m, "poles", poles, false);
        }
    }

  if(block_descr.HasMember("spurious_poles_L"))
    {
      if(!block_descr["spurious_poles_L"].IsArray())
        throw std::runtime_error("Error parsing block description: "
                                 "spurious_poles_L must be a list.");

      for(auto &m : block_descr["spurious_poles_L"].GetArray())
        {
          if(!m.HasMember("L") || !m.HasMember("poles") || !m["L"].IsInt64()
             || !m["poles"].IsArray())
            throw std::runtime_error(
              "Error parsing block description: "
              "elements of spurious_poles_L must contain keys \"L\" (with "
              "integer value) and \"poles\" (list of expressions)");
          auto &poles = result.spuriousPolesL[m["L"].GetInt64()];
          read_expression_array(m, "poles", poles, false);
        }
    }

  read_type(block_descr, "type", result.exchangeType);
  if(block_descr.HasMember("seed_type"))
    {
      read_type(block_descr, "seed_type", result.seedType);
    }
  else
    {
      result.seedType = result.exchangeType;
    }

  read_int(block_descr, "p", result.exchangeP);
  read_int(block_descr, "lambda_shift", result.lambdaShift);

  if(!block_descr.HasMember("exported_blocks")
     || !block_descr["exported_blocks"].IsArray()
     || block_descr["exported_blocks"].Size() == 0)
    {
      throw std::runtime_error("Error parsing block description: Block "
                               "description must contain a non-empty list of "
                               "exported blocks in \"exported_blocks\"");
    }

  for(auto &block_name : block_descr["exported_blocks"].GetArray())
    {
      if(!block_name.IsString())
        throw std::runtime_error("Error parsing block description: Elements "
                                 "of \"exported_blocks\" must be strings.");

      if(!block_descr.HasMember(block_name.GetString())
         || !block_descr[block_name.GetString()].IsObject())
        throw std::runtime_error(
          std::string("Error parsing block description: block ")
          + block_name.GetString()
          + " is declared in \"exported_blocks\" but not properly defined.");

      auto &block = block_descr[block_name.GetString()];

      read_expression(block, "zPower", result.zPower[block_name.GetString()]);
      read_expression(block, "one_minus_zPower",
                      result.one_minus_zPower[block_name.GetString()]);
      int64_t block_h_infinity_order;
      if(read_int(block, "h_infinity_order", block_h_infinity_order, true))
        {
          result.h_infinity_order[block_name.GetString()]
            = block_h_infinity_order;
        }

      if(!block.HasMember("value") || !block["value"].IsArray())
        {
          throw std::runtime_error(
            std::string("Error parsing block description: block ")
            + block_name.GetString() + " had no proper \"value\" defined.");
        }
      result.blockRules[block_name.GetString()].reserve(block["value"].Size());
      for(auto &term : block["value"].GetArray())
        {
          SeedBlockDerivative der;
          // read_type(term, "type", der.type, false);
          der.type = result.seedType;
          read_int(term, "perm", der.perm, false);

          if(!term.HasMember("e"))
            throw std::runtime_error(
              std::string("Error parsing block description: block ")
              + "parameter \"e\" must be defined for "
                "every term in the value.");

          if(term["e"].IsString())
            read_expression(term, "e", der.e);
          else
            {
              int64_t e;
              read_int(term, "e", e, false);
              der.e = RationalFunction(Multivariate(e));
            }

          der.p = result.exchangeP;

          read_expression(term, "ap", der.a);
          read_expression(term, "bp", der.b);

          bool xt_deriv_valid = term.HasMember("xt_derivs")
                                && term["xt_derivs"].IsArray()
                                && term["xt_derivs"][0].IsInt64()
                                && term["xt_derivs"][1].IsInt64();

          if(!xt_deriv_valid)
            throw std::runtime_error(
              std::string("Error parsing block description: block ")
              + "parameter \"xt_derivs\" must be defined and be a list of "
                "two integers");

          der.m = term["xt_derivs"][0].GetInt64();
          der.n = term["xt_derivs"][1].GetInt64();

          RationalFunction coefficient;
          read_expression(term, "coefficient", coefficient);

          result.blockRules[block_name.GetString()].emplace_back(coefficient,
                                                                 der);
        }
    }
  return result;
}
