#pragma once

#include <vector>
#include <list>
#include <ostream>
#include <map>
#include <string>

#include "../Bigfloat.hxx"
#include "../DerivativeTable/BlockTable.hxx"
#include "../Polynomial/Polynomial.hxx"

struct MeromorphicFunction
{
  std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>> poles;
  Polynomial<Bigfloat> polynomial;
};

struct PoleCache
{
  std::vector<std::pair<Bigfloat, int64_t>> unique_poles;
  std::vector<std::vector<Bigfloat>> denom_derivs;
  explicit PoleCache(const std::vector<Bigfloat> &poles);
};

struct PolynomialCache
{
  Polynomial<Bigfloat> poly_part;
  std::vector<std::vector<Polynomial<Bigfloat>>> denom_parts;
  explicit PolynomialCache(
    const std::vector<std::pair<Bigfloat, int64_t>> &unique_poles);
};

std::vector<std::pair<Bigfloat, int64_t>>
make_unique_poles(const std::vector<Bigfloat> &sorted_poles);

MeromorphicFunction to_meromorphic_fn(const Polynomial<Bigfloat> &poly,
                                      const PoleCache &pole_cache);

Polynomial<Bigfloat>
to_polynomial(const MeromorphicFunction &fn, const PolynomialCache &cache);

// MeromorphicFunction shift_poles_m(
//  const MeromorphicFunction &original,
//  const std::vector<std::pair<Bigfloat, int64_t>> &new_poles_unprotected,
//  const Bigfloat &unitarity);

// This function protects the poles at unitarity bound;
// Appearance of unitarity bound poles in new_poles has no effect.
BlockTable shift_poles(const BlockTable &original,
                       const std::vector<Bigfloat> &new_poles);