#include "pole_shifting.hxx"
#include <iostream>
#include <vector>
#include <map>
#include <Eigen/LU>

using Eigen_Matrix = Eigen::Matrix<Bigfloat, Eigen::Dynamic, Eigen::Dynamic>;
using Eigen_Vector = Eigen::Matrix<Bigfloat, Eigen::Dynamic, 1>;

const Bigfloat eps(0.000001); // TODO: should perhaps use numeric limits

std::vector<std::pair<Bigfloat, int64_t>>
make_unique_poles(const std::vector<Bigfloat> &poles)
{
  std::vector<Bigfloat> sorted_poles(poles);
  std::sort(sorted_poles.begin(), sorted_poles.end());
  Bigfloat current_pole;
  int64_t n_poles = 0;
  std::vector<std::pair<Bigfloat, int64_t>> unique_poles(0);
  for(auto i = sorted_poles.begin(); i != sorted_poles.end(); i++)
    {
      if(i == sorted_poles.begin())
        {
          current_pole = *i;
          n_poles = 1;
        }
      else
        {
          if(abs(*i - current_pole) > eps)
            {
              current_pole = *i;
              ++n_poles;
            }
        }
    }
  if(n_poles == 0)
    return unique_poles;

  unique_poles.reserve(n_poles);
  int current_order;

  for(auto i = sorted_poles.begin(); i != sorted_poles.end(); i++)
    {
      if(i == sorted_poles.begin())
        {
          current_pole = *i;
          current_order = 1;
        }
      else
        {
          if(abs(*i - current_pole) < eps)
            {
              ++current_order;
            }
          else
            {
              unique_poles.emplace_back(current_pole, current_order);
              current_pole = *i;
              current_order = 1;
            }
        }
    }
  if(!sorted_poles.empty())
    unique_poles.emplace_back(current_pole, current_order);

  return unique_poles;
}

PoleCache::PoleCache(const std::vector<Bigfloat> &poles)
    : unique_poles(make_unique_poles(poles))
{
  int64_t n_poles = unique_poles.size();

  denom_derivs.resize(n_poles);
  for(int64_t i = 0; i < n_poles; ++i)
    {
      int64_t pole_order = unique_poles.at(i).second;
      denom_derivs.at(i).resize(pole_order);
      for(int64_t order = 0; order < pole_order; ++order)
        {
          std::vector<int64_t> iterators(order);
          for(auto &it : iterators)
            it = 0;
          // iteration can be improved by keeping iterators ordered
          // however, this introduces trickier combinatorics; so do this only
          // iff performance issues.
          auto &deriv = denom_derivs.at(i).at(order);
          deriv = 0;
          if(n_poles == 1)
            {
              if(order == 0)
                {
                  deriv = 1;
                }
            }
          else
            {
              while(true)
                {
                  // compute the derivative
                  Bigfloat term(1);
                  for(int64_t j = 0; j < n_poles; ++j)
                    {
                      if(j == i)
                        continue;
                      int64_t n_derivs = 0;
                      int64_t j_order = unique_poles.at(j).second;
                      for(auto it : iterators)
                        {
                          if(j < i && j == it)
                            ++n_derivs;
                          if(j > i && j == it + 1)
                            ++n_derivs;
                        }
                      for(int64_t k = 0; k < n_derivs; k++)
                        {
                          term *= -j_order - k;
                        }
                      term *= pow(unique_poles.at(i).first
                                    - unique_poles.at(j).first,
                                  -n_derivs - j_order);
                    }
                  deriv += term;

                  // increment the iterators or exit the cycle
                  int64_t it = 0;
                  while(it < order && iterators.at(it) + 1 == n_poles - 1)
                    {
                      iterators.at(it) = 0;
                      ++it;
                    }
                  if(it == order)
                    {
                      break;
                    }
                  else
                    {
                      ++iterators.at(it);
                    }
                }
            }
        }
    }
}

Bigfloat evaluate_deriv(const Polynomial<Bigfloat> &poly, const int64_t n,
                        const Bigfloat &x)
{
  Bigfloat result(0);
  for(int64_t k = n; k < static_cast<int64_t>(poly.size()); ++k)
    {
      Bigfloat term(poly[k] * pow(x, k - n));
      for(int64_t l = 0; l < n; ++l)
        {
          term *= k - l;
        }
      result += term;
    }
  return result;
}

template <typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &vec)
{
  out << "{";
  for(int64_t i = 0; i < static_cast<int64_t>(vec.size()); ++i)
    {
      out << vec.at(i);
      if(i + 1 < static_cast<int64_t>(vec.size()))
        out << ", ";
    }
  out << "}";
  return out;
}

MeromorphicFunction to_meromorphic_fn(const Polynomial<Bigfloat> &poly,
                                      const PoleCache &pole_cache)
{
  MeromorphicFunction result;
  int64_t n_poles = pole_cache.unique_poles.size();
  result.poles.resize(n_poles);
  for(int64_t i = 0; i < n_poles; ++i)
    {
      result.poles.at(i).first = pole_cache.unique_poles.at(i).first;
      int64_t pole_order = pole_cache.unique_poles.at(i).second;
      result.poles.at(i).second.resize(pole_order);
      std::vector<Bigfloat> poly_derivs(pole_order);
      for(int64_t k = 0; k < pole_order; ++k)
        {
          poly_derivs.at(k)
            = evaluate_deriv(poly, k, result.poles.at(i).first);
        }
      for(int64_t order = 0; order < pole_order; ++order)
        {
          auto &coefficient = result.poles.at(i).second.at(order);
          coefficient = 0;
          int64_t n = pole_order - order - 1;
          for(int64_t k = 0; k <= n; ++k)
            {
              coefficient += binomial_coefficient(n, k) * poly_derivs.at(k)
                             * pole_cache.denom_derivs.at(i).at(n - k);
            }
          coefficient /= factorial(n);
        }
    }

  Polynomial<Bigfloat> denom({1});
  for(int64_t i = 0; i < n_poles; ++i)
    {
      denom
        *= pow(Polynomial<Bigfloat>({-pole_cache.unique_poles.at(i).first, 1}),
               pole_cache.unique_poles.at(i).second);
    }
  result.polynomial
    = boost::math::tools::quotient_remainder(poly, denom).first;
  return result;
}

PolynomialCache::PolynomialCache(
  const std::vector<std::pair<Bigfloat, int64_t>> &unique_poles)
{
  int64_t n_poles = unique_poles.size();

  poly_part = Polynomial<Bigfloat>({1});
  for(int64_t i = 0; i < n_poles; ++i)
    {
      poly_part *= pow(Polynomial<Bigfloat>({-unique_poles.at(i).first, 1}),
                       unique_poles.at(i).second);
    }

  denom_parts.resize(n_poles);
  for(int64_t i = 0; i < n_poles; ++i)
    {
      int64_t pole_order = unique_poles.at(i).second;
      denom_parts.at(i).resize(pole_order);
      auto &leading = denom_parts.at(i).at(pole_order - 1);
      leading = Polynomial<Bigfloat>({1});
      for(int64_t j = 0; j < n_poles; ++j)
        {
          if(j == i)
            continue;
          leading *= pow(Polynomial<Bigfloat>({-unique_poles.at(j).first, 1}),
                         unique_poles.at(j).second);
        }
      for(int64_t k = 1; k < pole_order; ++k)
        {
          denom_parts.at(i).at(pole_order - 1 - k)
            = denom_parts.at(i).at(pole_order - k)
              * Polynomial<Bigfloat>({-unique_poles.at(i).first, 1});
        }
    }
}

Polynomial<Bigfloat>
to_polynomial(const MeromorphicFunction &fn, const PolynomialCache &cache)
{
  int64_t n_poles = fn.poles.size();

  Polynomial<Bigfloat> result(fn.polynomial);
  result *= cache.poly_part;

  for(int64_t i = 0; i < n_poles; ++i)
    {
      int64_t pole_order = fn.poles.at(i).second.size();
      for(int64_t order = 0; order < pole_order; ++order)
        {
          result += fn.poles.at(i).second.at(order)
                    * cache.denom_parts.at(i).at(order);
        }
    }

  return result;
}

Eigen_Matrix make_shifting_matrix(
  const std::vector<std::pair<Bigfloat, int64_t>> &new_poles_unprotected,
  const Bigfloat &unitarity)
{
  int64_t size = 0;
  for(const auto &pole : new_poles_unprotected)
    {
      size += pole.second;
    }

  Eigen_Matrix A(size, size);
  for(int64_t row = 0; row < size; ++row)
    {
      int64_t k(row - (size / 2));
      int64_t column(0);
      for(size_t i(0); i != new_poles_unprotected.size(); ++i)
        {
          int64_t pole_order = new_poles_unprotected.at(i).second;
          int64_t prefactor(1);
          for(int64_t m(1); m <= pole_order; ++m)
            {
              A(row, column)
                = prefactor
                  * pow(new_poles_unprotected.at(i).first - unitarity,
                        k - m + 1)
                  / factorial(m - 1);
              ++column;
              prefactor *= (k - m + 1);
            }
        }
    }
  return A;
}

// We match the following functional, labeled by integer k:
// k >= 0: Take the coefficient of 1/x^(k+1) at x=infinity
// k < 0 : set n = -(k+1) and take -1/n! times n-th x-derivative at x=0
// The value of this functional on
// 1/(x-x0)^m is k*(k-1)*...*(k-m+2)/m! * x0^(k-m+1)
MeromorphicFunction shift_poles_m(
  const MeromorphicFunction &original,
  const std::vector<std::pair<Bigfloat, int64_t>> &new_poles_unprotected,
  const Bigfloat &unitarity, const Eigen_Matrix &A)
{
  int64_t size = 0;
  for(const auto &pole : new_poles_unprotected)
    {
      size += pole.second;
    }

  Eigen_Vector b(size);
  for(int64_t row = 0; row < size; ++row)
    {
      int64_t k(row - (size / 2));
      auto &element = b(row);
      element = 0;
      for(const auto &pole : original.poles)
        {
          if(abs(pole.first - unitarity) < eps)
            continue;
          int64_t pole_order = pole.second.size();
          int64_t prefactor(1);
          for(int64_t m(1); m <= pole_order; ++m)
            {
              element += pole.second.at(m - 1) * prefactor
                         * pow(pole.first - unitarity, k - m + 1)
                         / factorial(m - 1);
              prefactor *= (k - m + 1);
            }
        }
    }

  Eigen_Vector x = A.lu().solve(b);

  MeromorphicFunction result;
  result.polynomial = original.polynomial;
  std::list<std::pair<Bigfloat, std::vector<Bigfloat>>> protected_poles;
  for(const auto &pole : original.poles)
    {
      if(abs(pole.first - unitarity) < eps)
        {
          protected_poles.push_back(pole);
        }
    }
  result.poles.resize(protected_poles.size() + new_poles_unprotected.size());
  int64_t column = 0;
  for(size_t i(0); i < new_poles_unprotected.size(); ++i)
    {
      result.poles.at(i).first = new_poles_unprotected.at(i).first;
      result.poles.at(i).second.reserve(new_poles_unprotected.at(i).second);
      for(int64_t j(0); j < new_poles_unprotected.at(i).second; ++j)
        {
          result.poles.at(i).second.emplace_back(x(column));
          ++column;
        }
    }
  int64_t index = new_poles_unprotected.size();
  for(auto &&pole : protected_poles)
    {
      result.poles.at(index) = pole;
      ++index;
    }

  return result;
}

BlockTable
shift_poles(const BlockTable &original, const std::vector<Bigfloat> &new_poles_in_delta)
{
  Bigfloat unitarity_bound
    = original.L + Bigfloat(original.p) / 2 + (original.L == 0 ? 1 : 2);
  unitarity_bound = unitarity_bound - original.delta_minus_x;

  std::vector<Bigfloat> original_poles_in_x, new_poles;
  original_poles_in_x.reserve(original.poles.size());
  new_poles.reserve(new_poles_in_delta.size());
  for(const auto & pole : original.poles)
  {
    original_poles_in_x.emplace_back(pole-original.delta_minus_x);
  }
  for(const auto & pole : new_poles_in_delta)
  {
    new_poles.emplace_back(pole-original.delta_minus_x);
  }


  PoleCache orig_pole_cache(original_poles_in_x);

  // The pole lists formed below should be formed in the same way as in
  // shift_poles_m

  // This excludes all unitarity poles from new_poles, but includes all from
  // origina.poles
  std::vector<std::pair<Bigfloat, int64_t>> new_poles_unique(
    make_unique_poles(new_poles));
  std::list<std::pair<Bigfloat, int64_t>> new_poles_final_list;

  std::vector<std::pair<Bigfloat, int64_t>> new_poles_unprotected,
    new_poles_final;
  for(const auto &pole : new_poles_unique)
    {
      if(abs(pole.first - unitarity_bound) > eps)
        {
          new_poles_final_list.push_back(pole);
        }
    }
  new_poles_unprotected.reserve(new_poles_final_list.size());
  for(auto &pole : new_poles_final_list)
    {
      new_poles_unprotected.emplace_back(pole);
    }
  for(const auto &pole : orig_pole_cache.unique_poles)
    {
      if(abs(pole.first - unitarity_bound) <= eps)
        {
          new_poles_final_list.push_back(pole);
        }
    }
  new_poles_final.reserve(new_poles_final_list.size());
  for(auto &&pole : new_poles_final_list)
    {
      new_poles_final.emplace_back(pole);
    }

  Eigen_Matrix A
    = make_shifting_matrix(new_poles_unprotected, unitarity_bound);

  PolynomialCache poly_cache(new_poles_final);

  BlockTable result;
  auto &new_derivatives = result.derivatives;
  for(const auto &block : original.derivatives)
    {
      auto &new_table = new_derivatives[block.first];
      new_table.resize(block.second.size());
      for(size_t m = 0; m < block.second.size(); ++m)
        {
          new_table.at(m).resize(block.second.at(m).size());
          for(size_t n = 0; n < block.second.at(m).size(); ++n)
            {
              new_table.at(m).at(n) = to_polynomial(
                shift_poles_m(
                  to_meromorphic_fn(block.second.at(m).at(n), orig_pole_cache),
                  new_poles_unprotected, unitarity_bound, A),
                poly_cache);
            }
        }
    }

  int64_t poles_total = 0;
  for(const auto &pole : new_poles_final)
    {
      poles_total += pole.second;
    }
  result.poles.reserve(poles_total);
  for(const auto &pole : new_poles_final)
    {
      for(int64_t i(0); i < pole.second; ++i)
        result.poles.emplace_back(pole.first + original.delta_minus_x);
    }
  std::sort(result.poles.begin(), result.poles.end());

  result.delta_minus_x = original.delta_minus_x;
  result.p = original.p;
  result.L = original.L;
  result.description.CopyFrom(original.description,
                              result.description.GetAllocator());
  result.exchangeType = original.exchangeType;
  result.lambda = original.lambda;
  result.lookup = original.lookup;

  result.order = original.order;
  result.pole_order = original.pole_order;

  return result;
}