#include <map>
#include <list>
#include <thread>
#include <sstream>
#include "seed_recurse.hxx"
#include "../Polynomial/Polynomial.hxx"
#include "../table_io/table_io.hxx"
#include "../parsers/parsers.hxx"
#include "../BlockDescription/BlockDescription.hxx"
#include "seed_block_descriptions.hxx"
#include "../Timers.hxx"

void add_derivative(int64_t m, int64_t n, Polynomial<Bigfloat> &result,
                    const SeedTable &block, int64_t e,
                    const DerivativeWithFastPrefactor &derivative)
{
  for(size_t k_m = 0;
      k_m <= static_cast<size_t>(m) && k_m <= derivative.m_prefactor_max;
      ++k_m)
    for(size_t k_n = 0;
        k_n <= static_cast<size_t>(n) && k_n <= derivative.n_prefactor_max;
        ++k_n)
      {
        const auto &prefactor_der
          = derivative.prefactor_derivatives.at(k_m).at(k_n);

        if(prefactor_der.size() == 0)
          continue;
        result += prefactor_der
                  * block.derivative(e, m - k_m + derivative.m,
                                     n - k_n + derivative.n)
                  * binomial_coefficient(m, k_m)  // Can optimize away these
                  * binomial_coefficient(n, k_n); // binomials, but unnecessary
      }
}

SeedTable seed_recurse(const SeedTable &old,
                       const BlockDescription &recursion_rule, int64_t lambda,
                       int64_t p_sign, int64_t L, const Bigfloat &Delta12,
                       const Bigfloat &Delta34, const std::string &prefix = "")
{
  int64_t p = abs(p_sign);
  std::string timer_prefix = prefix + ".seed_recurse.p" + std::to_string(p);
  Timer &total = global_timers->add_and_start(timer_prefix);

  SeedTable result;

  // We assume that the below has been performed:
  // recursion_rule.exchangeP = p;
  // or
  // recursion_rule.exchangeP = -p;

  // if(recursion_rule.exchangeP != p && recursion_rule.exchangeP != -p)
  //   throw std::runtime_error(
  //     "seed_recurse: p and |recursion_rule.exchangeP| do not match");

  std::map<std::string, Bigfloat> lookup;
  lookup["ap"] = -Delta12 / 2;
  lookup["bp"] = Delta34 / 2;
  lookup["p"] = p;
  lookup[recursion_rule.spinParameter] = L;

  result.lookup.resize(p + 1);
  result.p = p;

  for(int64_t e = 0; e <= p; ++e)
    {
      std::string e_timer_prefix = timer_prefix + ".e" + std::to_string(e);
      Timer &e_total = global_timers->add_and_start(e_timer_prefix);

      lookup["e"] = e;

      Timer &specialization
        = global_timers->add_and_start(e_timer_prefix + ".specializing_rule");
      PrefactorDerivativeTable specialized_rule
        = recursion_rule.make_prefactor_derivative_table("xtDeriv", lookup, 10,
                                                         true, p_sign);
      specialization.stop();

      std::stringstream ss;
      ss << "seedBlockE" << e;

      auto &result_at_e = result.derivatives[ss.str()];
      result.lookup[e] = &result_at_e;

      result_at_e.resize(lambda + 1);
      for(int64_t m = 0; m <= lambda; ++m)
        {
          result_at_e.at(m).resize((lambda - m) / 2 + 1);
        }

      for(int64_t m = 0; m <= lambda; ++m)
        for(int64_t n = 0; 2 * n + m <= lambda; ++n)
          {
            auto &derivative_to_fill = result_at_e.at(m).at(n);
            derivative_to_fill = Polynomial<Bigfloat>(0);
            for(const auto &block_contribution : specialized_rule.rule)
              {
                int64_t block_e = block_contribution.first.e;
                if(block_e < 0 || block_e > p - 1)
                  continue;
                for(const auto &block_derivative : block_contribution.second)
                  {
                    add_derivative(m, n, derivative_to_fill, old, block_e,
                                   block_derivative);
                  }
              }
            for(const auto &pole : specialized_rule.spurious_poles)
              {
                Polynomial<Bigfloat> pole_poly({-pole, 1});
                derivative_to_fill = boost::math::tools::quotient_remainder(
                                       derivative_to_fill, pole_poly)
                                       .first;
              }
          }
      e_total.stop();
    }

  total.stop();
  return result;
}

void write_seeds_thread(const boost::filesystem::path &input_dir,
                        const boost::filesystem::path &output_dir,
                        int64_t block_type, int64_t perm, int64_t p,
                        const BlockDescription &description,
                        const std::vector<int64_t> &spins,
                        const std::string &Delta_12_string,
                        const std::string &Delta_34_string, int64_t lambda,
                        int64_t keptPoleOrder, int64_t order,
                        size_t thread_rank, size_t num_threads)
{
  Bigfloat Delta12(Delta_12_string), Delta34(Delta_34_string);

  std::string thread_prefix
    = "write_seeds.thread" + std::to_string(thread_rank);
  Timer &total_thread = global_timers->add_and_start(thread_prefix + ".Total");

  for(size_t spin_pos = thread_rank; spin_pos < spins.size();
      spin_pos += num_threads)
    {
      size_t L = spins.at(spin_pos);

      std::string thread_spin_prefix
        = thread_prefix + ".L" + std::to_string(L);

      Timer &spin_reading = global_timers->add_and_start(
        thread_spin_prefix + ".reading_scalar_block");

      boost::filesystem::path cb_file = table_io::find_scalar_block(
        input_dir, keptPoleOrder, order, L, Delta_12_string, Delta_34_string,
        lambda + 4 * p);

      SeedTable seed_table(parsers::parse_seed_table_file(cb_file.string()));
      seed_table.normalize(L);

      spin_reading.stop();

      Timer &total_recursion = global_timers->add_and_start(
        thread_spin_prefix + ".seed_recurse");
      for(int64_t current_p = 0; current_p < p; ++current_p)
        {
          Timer &p_recursion = global_timers->add_and_start(
            thread_spin_prefix + ".seed_recurse.p"
            + std::to_string(current_p + 1));

          size_t use_p;
          if(block_type == 0)
            use_p = -(current_p + 1);
          else
            use_p = current_p + 1;

          seed_table = seed_recurse(seed_table, description,
                                    lambda + 4 * (p - current_p - 1), use_p, L,
                                    Delta12, Delta34, thread_spin_prefix);
          p_recursion.stop();
        }
      total_recursion.stop();

      if(block_type == 0)
        {
          // Fix the unitarity bound. Currently all polynomials are in terms of
          // x defined as delta = x + 2 + L - p/2, i.e. x = delta - 2 - L +
          // p/2. But we need everything to be in terms of x = delta - 2 - L -
          // p/2. This means that we should replace x in our polynomials by x +
          // p.
          Timer &total_shifting = global_timers->add_and_start(
            thread_spin_prefix + ".shifting_primal_block");

          Bigfloat shift(p);
          seed_table.shift_variable(p);

          total_shifting.stop();
        }

      Timer &total_writing
        = global_timers->add_and_start(thread_spin_prefix + ".writing_file");

      std::stringstream ss;

      // Format looks as
      // seedBlockTable4D-type-dual-perm-1234-P1-L0-a--0.43955000-b-0.61955000-lambda-9-keptPoleOrder-25-order-90
      ss << "seedBlockTable4D-type-";
      if(block_type == 0)
        ss << "primal";
      else
        ss << "dual";
      ss << "-perm-" << perm << "-P" << p << "-L" << L << "-delta12-"
         << Delta_12_string << "-delta34-" << Delta_34_string << "-lambda-"
         << lambda << "-keptPoleOrder-" << keptPoleOrder << "-order-" << order
         << ".m";

      if(global_timers->debug)
        std::cout << "Writing file " << (output_dir / ss.str()) << std::endl;

      boost::filesystem::ofstream outfile(output_dir / ss.str());
      if(!outfile)
        throw std::runtime_error("Cannot open file "
                                 + (output_dir / ss.str()).string()
                                 + " for writing.");

      outfile << std::fixed;
      outfile.precision(boost::multiprecision::mpf_float::default_precision());

      outfile << seed_table << std::endl;

      outfile.close();

      total_writing.stop();
    }
  total_thread.stop();
}

void write_seeds(const boost::filesystem::path &input_dir,
                 const boost::filesystem::path &output_dir, int64_t block_type,
                 int64_t perm, int64_t p, const std::vector<int64_t> &spins,
                 const std::string &Delta_12_string,
                 const std::string &Delta_34_string, int64_t lambda,
                 int64_t keptPoleOrder, int64_t order, size_t num_threads)
{
  BlockDescription *description;

  Timer& loading_timer = global_timers->add_and_start("write_seeds.loading_description");

  if(block_type == 0 && perm == 1234)
    {
      seed_descriptions::init_primal1234();
      description = &seed_descriptions::primal1234;
    }
  else if(block_type == 0 && perm == 1243)
    {
      seed_descriptions::init_primal1243();
      description = &seed_descriptions::primal1243;
    }
  else if(block_type == 1 && perm == 1234)
    {
      seed_descriptions::init_dual1234();
      description = &seed_descriptions::dual1234;
    }
  else if(block_type == 1 && perm == 1243)
    {
      seed_descriptions::init_dual1243();
      description = &seed_descriptions::dual1243;
    }
  else
    throw std::runtime_error("seed permutation type not recongnized");

  loading_timer.stop();

  create_directories(output_dir);

  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      threads.emplace_back(
        write_seeds_thread, std::cref(input_dir), std::cref(output_dir),
        block_type, perm, p, std::cref(*description), std::cref(spins),
        std::cref(Delta_12_string), std::cref(Delta_34_string), lambda,
        keptPoleOrder, order, thread_rank, num_threads);
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
}
