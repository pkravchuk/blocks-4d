#include "../BlockDescription/BlockDescription.hxx"

namespace seed_descriptions
{
  extern BlockDescription primal1234;
  extern BlockDescription primal1243;

  extern BlockDescription dual1234;
  extern BlockDescription dual1243;
  
  void init_primal1234();
  void init_primal1243();
  void init_dual1234();
  void init_dual1243();
}