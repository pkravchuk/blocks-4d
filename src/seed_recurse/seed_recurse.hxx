#pragma once

#include <string>
#include <map>
#include <boost/filesystem.hpp>

#include "../Bigfloat.hxx"
#include "../DerivativeTable/SeedTable.hxx"
#include "../BlockDescription/BlockDescription.hxx"

SeedTable
seed_recurse(const SeedTable &old, BlockDescription &recursion_rule,
             int64_t lambda, int64_t p, int64_t L, const Bigfloat &Delta12,
             const Bigfloat &Delta34, const std::string &prefix = "");

void write_seeds(const boost::filesystem::path &input_dir,
                 const boost::filesystem::path &output_dir, int64_t block_type,
                 int64_t perm, int64_t p, const std::vector<int64_t> &spins,
                 const std::string &Delta_12_string,
                 const std::string &Delta_34_string, int64_t lambda,
                 int64_t keptPoleOrder, int64_t order, size_t num_threads);