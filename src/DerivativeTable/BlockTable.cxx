#include <sstream>
#include <iostream>

#include "../Timers.hxx"
#include "BlockTable.hxx"
#include "../rapidjson/prettywriter.h"
#include "../rapidjson/ostreamwrapper.h"

// debug
#include <stdio.h>

BlockTable::BlockTable(FastDerivativeTable &&table, int64_t L0, int64_t p0)
    : p(p0), L(L0)
{
  derivatives = std::move(table.derivatives);
}

rapidjson::Value std_string_to_value(const std::string &str,
                                     rapidjson::MemoryPoolAllocator<> &alloc)
{
  rapidjson::Value result;
  result.SetString(str.c_str(), str.length(), alloc);
  return result;
}

rapidjson::Value
makevalue(const Bigfloat &val, rapidjson::MemoryPoolAllocator<> &alloc)
{
  int64_t rounded = round_bigfloat(2 * val);
  std::ostringstream ssval;
  Bigfloat epsilon(pow(
    Bigfloat(10), -2
                    * static_cast<int64_t>(
                      boost::multiprecision::mpf_float::default_precision())
                    / 3));
  if(val.is_zero()
     || (abs(val) > 0.3 && abs((rounded - 2 * val) / 2 / val) < epsilon))
    {
      ssval << double(rounded) / 2;
    }
  else
    {
      ssval << std::fixed
            << std::setprecision(
                 boost::multiprecision::mpf_float::default_precision())
            << val;
    }

  return std_string_to_value(ssval.str(), alloc);
}

rapidjson::Value makevalue_no_rounding(const Bigfloat &val,
                                       rapidjson::MemoryPoolAllocator<> &alloc)
{
  std::ostringstream ssval;
  ssval << std::fixed
        << std::setprecision(
             boost::multiprecision::mpf_float::default_precision())
        << val;

  return std_string_to_value(ssval.str(), alloc);
}

rapidjson::Value makevalue(const Polynomial<Bigfloat> &pol,
                           rapidjson::MemoryPoolAllocator<> &alloc)
{
  rapidjson::Value result;
  result.SetArray();
  for(uint64_t deg = 0; deg <= pol.degree(); deg++)
    {
      result.PushBack(makevalue_no_rounding(pol[deg], alloc), alloc);
    }

  return result;
}

template <typename T>
rapidjson::Value
makevalue(const std::vector<T> &vec, rapidjson::MemoryPoolAllocator<> &alloc)
{
  rapidjson::Value result;
  result.SetArray();
  for(const auto &val : vec)
    {
      result.PushBack(makevalue(val, alloc), alloc);
    }

  return result;
}

std::ostream &operator<<(std::ostream &out, const BlockTable &table)
{
  rapidjson::Document result;
  result.SetObject();
  auto &alloc = result.GetAllocator();

  if(table.description.HasMember("description")
     && table.description["description"].GetType() != rapidjson::kNullType)
    {
      rapidjson::Value descr;
      descr.CopyFrom(table.description["description"], alloc);
      result.AddMember("description", descr, alloc);
    }

  result.AddMember("p", rapidjson::Value(table.p), alloc);
  if(table.exchangeType == 0)
    result.AddMember("type", rapidjson::Value("primal"), alloc);
  else
    result.AddMember("type", rapidjson::Value("dual"), alloc);

  result.AddMember("spin", rapidjson::Value(table.L), alloc);

  result.AddMember("lambda", rapidjson::Value(table.lambda), alloc);
  result.AddMember("order", rapidjson::Value(table.order), alloc);
  result.AddMember("kept_pole_order", rapidjson::Value(table.pole_order),
                   alloc);
  result.AddMember("final_kept_pole_order",
                   rapidjson::Value(table.final_pole_order), alloc);

  result.AddMember("poles", makevalue(table.poles, alloc), alloc);

  rapidjson::Value lookup;
  lookup.SetObject();
  for(const auto &entry : table.lookup)
    {
      lookup.AddMember(std_string_to_value(entry.first, alloc),
                       makevalue(entry.second, alloc), alloc);
    }

  result.AddMember("parameters", lookup, alloc);

  result.AddMember("delta_minus_x", makevalue(table.delta_minus_x, alloc),
                   alloc);

  rapidjson::Value blocks;
  blocks.SetObject();
  for(const auto &block : table.derivatives)
    {
      blocks.AddMember(std_string_to_value(block.first, alloc),
                       makevalue(block.second, alloc), alloc);
    }
  result.AddMember("blocks", blocks, alloc);

  rapidjson::OStreamWrapper osw(out);
  rapidjson::PrettyWriter<rapidjson::OStreamWrapper> wrt(osw);
  result.Accept(wrt);

  return out;
}