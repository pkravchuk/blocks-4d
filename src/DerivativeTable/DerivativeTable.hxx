#pragma once

#include <map>
#include <utility>
#include <ostream>
#include "../Polynomial/Multivariate.hxx"
#include "../Polynomial/Polynomial.hxx"

struct DerivativeTable
{
  std::map<std::string, std::map<std::pair<int64_t, int64_t>, Multivariate>>
    derivatives;
  Multivariate &derivative(const std::string &, int64_t, int64_t);
};

struct FastDerivativeTable
{
  std::map<std::string, std::vector<std::vector<Polynomial<Bigfloat>>>>
    derivatives;
  Polynomial<Bigfloat> &derivative(const std::string &, size_t, size_t);
  FastDerivativeTable(){};
  FastDerivativeTable(const DerivativeTable &);
};

std::ostream &operator<<(std::ostream &, const DerivativeTable &);
std::ostream &operator<<(std::ostream &, const FastDerivativeTable &);
