#pragma once

#include <vector>
#include "../Bigfloat.hxx"
#include "../DerivativeTable/DerivativeTable.hxx"

struct SeedTable : FastDerivativeTable
{
  int64_t p;
  std::vector<std::vector<std::vector<Polynomial<Bigfloat>>>*> lookup;
  SeedTable(FastDerivativeTable &&);
  SeedTable() {};
  void normalize(int64_t L);
  void normalize_seeds(int64_t type, int64_t p, int64_t L);
  void shift_variable(const Bigfloat & sh);
  Polynomial<Bigfloat> & derivative(size_t e, size_t m, size_t n);
  const Polynomial<Bigfloat> & derivative(size_t e, size_t m, size_t n) const;
};