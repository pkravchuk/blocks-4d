#include "DerivativeTable.hxx"

std::ostream & operator<<(std::ostream &, const Polynomial<Bigfloat>&);

Multivariate &
DerivativeTable::derivative(const std::string &name, int64_t m, int64_t n)
{
  return derivatives[name][std::make_pair(m, n)];
}

std::ostream &operator<<(std::ostream &out, const DerivativeTable &table)
{
  for(auto &nametable : table.derivatives)
    {
      out << "name : " << nametable.first << std::endl;
      for(auto &rule : nametable.second)
        {
          out << "[" << rule.first.first << ", " << rule.first.second
              << "] -> ";
          out << rule.second << std::endl;
        }
    }
  return out;
}

Polynomial<Bigfloat> &
FastDerivativeTable::derivative(const std::string &name, size_t m, size_t n)
{
  return derivatives[name].at(m).at(n);
}

FastDerivativeTable::FastDerivativeTable(const DerivativeTable &table)
{
  for(const auto &type : table.derivatives)
    {
      auto &derivatives_of_type = derivatives[type.first];
      int64_t lambda = 0;
      for(const auto &derivative : type.second)
        {
          if(lambda < derivative.first.first + 2 * derivative.first.second)
            lambda = derivative.first.first + 2 * derivative.first.second;
        }
      derivatives_of_type.resize(lambda + 1);
      for(int64_t m = 0; m <= lambda; ++m)
        derivatives_of_type.at(m).resize((lambda - m) / 2 + 1);

      for(const auto &derivative : type.second)
        {
          derivatives_of_type.at(derivative.first.first)
            .at(derivative.first.second)
            = derivative.second.to_poly();
        }
    }
}

std::ostream &operator<<(std::ostream &out, const FastDerivativeTable &table)
{
  out << "{" << std::endl;
  for(auto it = table.derivatives.begin(); it != table.derivatives.end(); ++it)
    {
      auto next = it;
      ++next;
      const auto &nametable = *it;
      int64_t lambda = nametable.second.size() - 1;
      for(int64_t m = 0; m <= lambda; m++)
        for(int64_t n = 0; m + 2 * n <= lambda; n++)
          {
            out << nametable.first << "[" << m << ", " << n << "] -> "
                << nametable.second.at(m).at(n);
            if(!(m == lambda && next == table.derivatives.end()))
              out << "," << std::endl;
          }
    }
  out << "}" << std::endl;
  return out;
}

std::ostream &operator<<(std::ostream &out, const Polynomial<Bigfloat> &poly)
{
  for(size_t power = 0; power < poly.size(); ++power)
    {
      out << poly[power];
      if(power != 0)
        {
          out << "*x^" << power;
        }
      if(power + 1 != poly.size())
        {
          out << "\n   + ";
        }
    }
  return out;
}