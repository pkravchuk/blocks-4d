#pragma once

#include <vector>
#include <list>
#include <ostream>
#include <map>
#include <string>

#include "../Bigfloat.hxx"
#include "../DerivativeTable/DerivativeTable.hxx"
#include "../rapidjson/document.h"

struct BlockTable : FastDerivativeTable
{
  int64_t p, L, exchangeType;
  Bigfloat delta_minus_x;
  int64_t order, pole_order, final_pole_order;
  int64_t lambda;
  std::vector<Bigfloat> poles;
  std::map<std::string, Bigfloat> lookup;
  rapidjson::Document description;
  BlockTable(FastDerivativeTable &&, int64_t L, int64_t p);
  BlockTable(){};
};

std::ostream &operator<<(std::ostream &, const BlockTable &);