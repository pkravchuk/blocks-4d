#include "SeedTable.hxx"

SeedTable::SeedTable(FastDerivativeTable &&table)
{
  derivatives = std::move(table.derivatives);
  if(derivatives.size() == 0)
    throw std::runtime_error("Creating SeedTable from empty DerivativeTable.");

  if(derivatives.begin()->first == "abDeriv")
    {
      if(derivatives.size() > 1)
        throw std::runtime_error(
          "Creating scalar SeedTable from DerivativeTable "
          "with more than one block.");

      auto &derivative_list = derivatives.begin()->second;
      if(derivative_list.size() == 0)
        throw std::runtime_error("Empty derivative table");

      size_t lambda = derivative_list.size() - 1;
      for(size_t m = 0; m <= lambda; ++m)
        for(size_t n = 0; 2 * n + m <= lambda; ++n)
          {
            int64_t exp = m + 2 * n;
            Bigfloat coeff(pow(Bigfloat(2), exp));
            derivative_list.at(m).at(n) *= coeff;
          }

      lookup.resize(1);
      lookup[0] = &(derivatives.begin()->second);
      // We should probably change the key here, but it is not really easy to
      // do with std::map. So we should just not try to address by the key in a
      // scalar table

      return;
    }
  // Here we are definitely not in a scalar table, so addressing by seedBlockE
  // is ok
  p = int64_t(derivatives.size()) - 1;
  lookup.resize(p + 1);
  for(int64_t e = 0; e <= p; ++e)
    {
      std::stringstream ss;
      ss << "seedBlockE" << e;
      lookup[e] = &derivatives[ss.str()];
    }
}

Polynomial<Bigfloat> &SeedTable::derivative(size_t e, size_t m, size_t n)
{
  return lookup.at(e)->at(m).at(n);
}

const Polynomial<Bigfloat> &
SeedTable::derivative(size_t e, size_t m, size_t n) const
{
  return lookup.at(e)->at(m).at(n);
}

void SeedTable::normalize(int64_t L)
{
  Bigfloat coefficient((L + 1) * (1 - 2 * (L % 2)));
  for(auto &type : derivatives)
    {
      for(auto &row : type.second)
        {
          for(auto &derivative : row)
            derivative *= coefficient;
        }
    }
}

void SeedTable::shift_variable(const Bigfloat &sh)
{
  for(auto &type : derivatives)
    {
      for(auto &row : type.second)
        {
          for(auto &derivative : row)
            derivative = shift_polynomial(derivative, sh);
        }
    }
}

void SeedTable::normalize_seeds(int64_t type, int64_t p, int64_t L)
{
  if(p == 0)
    return;
  Bigfloat coefficient;
  Bigfloat typefactor;
  if(type == 0)
    {
      coefficient = pow(4 * (3 - 2 * sqrt(Bigfloat(2))), Bigfloat(p) / 2);
      typefactor = Bigfloat(256);
    }
  else
    {
      coefficient = pow(4 * (3 - 2 * sqrt(Bigfloat(2))), -Bigfloat(p) / 2);
      typefactor = Bigfloat(128);
    }

  for(int64_t p_intermediate = 1; p_intermediate <= p; ++p_intermediate)
    {
      coefficient /= typefactor * (p_intermediate + L);
    }

  for(auto &type : derivatives)
    {
      for(auto &row : type.second)
        {
          for(auto &derivative : row)
            derivative *= coefficient;
        }
    }
}