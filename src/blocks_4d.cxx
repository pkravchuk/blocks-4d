#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "Bigfloat.hxx"
#include "Timers.hxx"
#include "parsers/parsers.hxx"
#include "BlockDescription/BlockDescription.hxx"
#include "block_recurse/block_recurse.hxx"

namespace po = boost::program_options;

Timers *global_timers;

std::vector<int64_t> parse_spin_ranges(const std::string &spin_ranges_string);

int main(int argc, char *argv[])
{
  try
    {
      boost::filesystem::path description_path, lookup_file;
      std::string lookup_string, spins_string, input_dir_format,
        filename_format;
      int64_t float_format, num_threads, precision_binary, order,
        kept_pole_order, kept_pole_order_final, lambda;

      std::string debug_path;
      bool debug = false;

      po::options_description desc("Allowed options");
      desc.add_options()("help", "Show this message.")(
        "description,d",
        po::value<boost::filesystem::path>(&description_path)->required(),
        "Path to the block description file")(
        "parameters,p",
        po::value<std::string>(&lookup_string)->default_value("{}"),
        "List of numerical values for parameters used in formulas in the "
        "description file, in JSON format.")(
        "parameters-file", po::value<boost::filesystem::path>(&lookup_file),
        "Path to a JSON file containing numerical values for parameters used "
        "in formulas in the "
        "description file. If specifieds, overrides the --parameters option.")(
        "float-format", po::value<int64_t>(&float_format)->default_value(5),
        "Specifies the number of digits after the decimal point to use in "
        "formatting the names of seed block files, when searching for them.")(
        "threads", po::value<int64_t>(&num_threads)->default_value(1),
        "Number of threads to use.")(
        "precision", po::value<int64_t>(&precision_binary)->required(),
        "The precision, in the number of bits, to use in the computation.")(
        "order", po::value<int64_t>(&order)->required(),
        "The recursion order of the seed blocks to use.")(
        "poles", po::value<int64_t>(&kept_pole_order)->required(),
        "The order of kept poles of the seed blocks to use (aka "
        "keptPoleOrder).")(
        "finalize_poles",
        po::value<int64_t>(&kept_pole_order_final)->default_value(-1),
        "If this option is specified, the poles in the final block are "
        "additionally \"shifted\" (truncated) to the given order. It might be "
        "useful to keep this parameter low, while keeping \'poles\' parameter "
        "relatively large.")(
        "spin-ranges", po::value<std::string>(&spins_string)->required(),
        "Comma-separated list of ranges of spins to compute.")(
        "lambda", po::value<int64_t>(&lambda)->required(),
        "Derivative order to compute. "
        "The seed blocks used are those which match other parameters and "
        "have "
        "lambda=2*n_max-1 at least lambda+lambdaShift, where lambdaShift is "
        "specified in the block description file.")(
        ",i", po::value<std::string>(&input_dir_format)->required(),
        "Input directory that contains seed block files. One may use the "
        "alias {} to denote the spin of the seed block.")(
        ",o", po::value<std::string>(&filename_format)->required(),
        "Format for output file names. One may use the "
        "alias {} to denote the spin of the seed block.")(
        "debug", po::value<std::string>(&debug_path)->implicit_value(""),
        "Enable some debug output with timing stats written to the given "
        "file. If the filename is an empty string, no data is written.");

      po::variables_map variables_map;

      po::store(po::parse_command_line(argc, argv, desc), variables_map);
      if(variables_map.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }
      if(variables_map.count("debug"))
        {
          debug = true;
        }
      po::notify(variables_map);

      if(kept_pole_order_final == -1)
        kept_pole_order_final = kept_pole_order;
      const size_t precision_base_10(
        boost::multiprecision::detail::digits2_2_10(precision_binary));

      boost::multiprecision::mpf_float::default_precision(precision_base_10);

      global_timers = new Timers(debug);

      std::map<std::string, Bigfloat> lookup;

      if(variables_map.count("parameters-file"))
        {
          lookup = parsers::parse_lookup_file(lookup_file.string());
        }
      else
        {
          lookup = parsers::parse_lookup(lookup_string);
        }

      std::vector<int64_t> spins(parse_spin_ranges(spins_string));

      write_blocks(input_dir_format, filename_format, description_path, spins,
                   lookup, lambda, kept_pole_order, kept_pole_order_final,
                   order, float_format, num_threads);

      if(debug && (!debug_path.empty()))
        global_timers->write_profile(debug_path);
    }
  catch(std::exception &e)
    {
      std::cout << "Error: " << e.what() << std::endl;
      exit(1);
    }
  catch(...)
    {
      std::cout << "Unknown error." << std::endl;
      exit(1);
    }

  return 0;
}