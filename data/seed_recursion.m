(* ::Package:: *)

(*SetDirectory[NotebookDirectory[]<>"recursion_relations"];*)


(*SetDirectory[NotebookDirectory[]];*)


myDirectory=If[$InputFileName!="",DirectoryName@$InputFileName,NotebookDirectory[]];


Get[FileNameJoin[{myDirectory,"util.m"}]];


(* ::Section:: *)
(*Auxiliary Functions*)


(*simplification of expression containing H[__][z,zb]*)
sympl[expr_]:=Collect[expr,H[__][__],Simplify];


(*simplification of expression containing Htt[__][x,t]*)
polt[expr_]:=Collect[expr,t,Simplify];
polxt[expr_]:=Collect[expr,x,polt];
sympltt[expr_]:=Collect[expr,Htt[__][__],polxt];


(*It is convenient to disply derivatives of functions Htt as*)

\!\(\*SuperscriptBox[\(Htt[A__, {mm_, nn_}]\), 
TagBox[
RowBox[{"(", 
RowBox[{"m_", ",", "n_"}], ")"}],
Derivative],
MultilineFunction->None]\)[x_,t_]:=Htt[A,{mm+m,nn+n}][x,t];

\!\(\*SuperscriptBox[\(H[A__, {mm_, nn_}]\), 
TagBox[
RowBox[{"(", 
RowBox[{"m_", ",", "n_"}], ")"}],
Derivative],
MultilineFunction->None]\)[z_,zb_]:=H[A,{mm+m,nn+n}][z,zb];


(*removes objects like Htt[{p,-1},__] and Htt[{p,e+1},__]*)
removeVanishingHtt[expr_]:=expr/.Htt[{p_,e_},A__]:>If[e>=0&&e<=p,Htt[{p,e},A],0]


(* ::Section::Closed:: *)
(*Prefactors*)


singleSeedPrefactor[\[CapitalDelta]_,p_]:=1/(64(l+p) (2+p-2 \[CapitalDelta]) (4+2 l+p-2 \[CapitalDelta]))//Factor;
prefactorSeed[\[CapitalDelta]_,p_]:=Product[singleSeedPrefactor[\[CapitalDelta]+(n-1)/2,p-n+1],{n,1,p}]//Factor;

singleDualSeedPrefactor[\[CapitalDelta]_,p_]:=1/(64(l+p) (-4+p+2 \[CapitalDelta]) (-2+l+p+\[CapitalDelta]+\[CapitalDelta]1-\[CapitalDelta]2)(-2+l+p+\[CapitalDelta]+\[CapitalDelta]3-\[CapitalDelta]4))/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1-p/2,\[CapitalDelta]3->2bp+\[CapitalDelta]4+p/2}//Factor;
prefactorDualSeed[\[CapitalDelta]_,p_]:=Product[singleDualSeedPrefactor[\[CapitalDelta]-(n-1)/2,p-n+1],{n,1,p}]//Factor;


(* ::Section:: *)
(*Convertor: (z,zb) to (x,t)*)


(* ::Subsection:: *)
(*Change Of Variables*)


(* Transition to new variables
{z\[Equal]1/2+x+y,zb\[Equal]1/2+x-y}
{x=(z+zb-1)/2,y\[Equal](z-zb)/2}
*)

(*
Since function H is symmetric under z\[UndirectedEdge]zb, we can write:
H[{0,0},A__,{0,0}][z,zb]=Ht[{0,0},A__,{0,0}][x,y]=Htt[{0,0},A__,{0,0}][x,t],
where t=y^2
*)


(*Derivatives in new variables*)
Dz[expr_]:=1/2 D[expr,x]+Sqrt[t] D[expr,t];
Dzb[expr_]:=1/2 D[expr,x]-Sqrt[t] D[expr,t];


(*makes a change of variables: (z,zb) to (x,t)*)
makeChangeVariables[expr_]:=expr/.H[A__,{m_,n_}][z,zb]:>Nest[Dzb[#]&,Nest[Dz[#]&,
Htt[A,{0,0}][x,t]
,m],n]/.{z->1/2+x+Sqrt[t],zb->1/2+x-Sqrt[t]};


(*Derivatives in new variables*)
Dxx[expr_]:=D[expr,z]+D[expr,zb]
Dtt[expr_]:=(1/(z-zb))(D[expr,z]-D[expr,zb])
(*makes an inverse change of variables: (x,t) to (z,zb)*)
makeInverseChangeVariables[expr_]:=expr/.Htt[A__,{m_,n_}]:>Nest[Dtt[#]&,
Nest[Dxx[#]&,
H[A,{0,0}][z,zb]
,m],n]/.{z->1/2,zb->1/2+\[Epsilon]}


(* ::Subsection::Closed:: *)
(*Recursion Relations (non-permuted -- deprecated)*)


(*
It is convenient to use variables {ap=-((\[CapitalDelta]1-\[CapitalDelta]2-p/2)/2),bp=(\[CapitalDelta]3-\[CapitalDelta]4-p/2)/2}.
One can notice that in the recursion relations [p,\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4]\[Rule][p-1,\[CapitalDelta]1,\[CapitalDelta]2+1/2,\[CapitalDelta]3,\[CapitalDelta]4+1/2] the parameters ap and bp remain the same
*)

(*we will not keep an explicit dependence of Htt on ap,bp and l since they do not change in the recursion procedure*)


(*writeSeedRecursion:=Module[{},

seed=safeGet[
	FileNameJoin[{dataDir,"seed_recursion_formula_zzb.m"}]
];

seedStripped=1/singleSeedPrefactor[\[CapitalDelta],p] seed//Factor;
seedStrippedNewVariables=seedStripped//makeChangeVariables//Factor//sympltt;

seedStrippedNewVariablesNewParameters=seedStrippedNewVariables/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1-p/2,\[CapitalDelta]3->2bp+\[CapitalDelta]4+p/2,\[CapitalDelta]->d}/. Htt[{p_,e_},{d_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}][x,t]:> Htt[{p,e},d,{m,n}][x,t]//sympltt;

seedStrippedNewVariablesNewParametersRule=Htt[{p_,e_},d_,{0,0}][x,t]->seedStrippedNewVariablesNewParameters;

safeExport[FileNameJoin[{dataDir,"seed_recursion_formula_xt.m"}],seedStrippedNewVariablesNewParametersRule];

(*seedStrippedNewVariablesNewParametersRule*)
]*)


(*writeDualSeedRecursion:=Module[{},

dualSeed=safeGet[
	FileNameJoin[{dataDir,"dual_seed_recursion_formula_zzb.m"}]
];
dualSeedStripped=1/singleDualSeedPrefactor[\[CapitalDelta],p] dualSeed//Factor;

dualSeedStrippedNewVariables=dualSeedStripped//makeChangeVariables//Factor//sympltt;

dualSeedStrippedNewVariablesNewParameters=dualSeedStrippedNewVariables/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1-p/2,\[CapitalDelta]3->2bp+\[CapitalDelta]4+p/2,\[CapitalDelta]->d}/. Htt[{p_,e_},{d_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}][x,t]:> Htt[{p,e},d,{m,n}][x,t]//sympltt;

dualSeedStrippedNewVariablesNewParametersRule=Htt[{p_,e_},d_,{0,0}][x,t]->dualSeedStrippedNewVariablesNewParameters;
safeExport[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt.m"}],dualSeedStrippedNewVariablesNewParametersRule];

(*dualSeedStrippedNewVariablesNewParametersRule*)

]*)


(* ::Subsection::Closed:: *)
(*Recursion Relations Derivatives (only aux functions)*)


(*
we use the standard convention for \[CapitalLambda]:
\[CapitalLambda] is the total number of derivatives with respect to (x,y)-variables;
odd derivatives in y-variables vanish;
since we deal instead of y- with t-variable; we count every tt-derivative twice
*)


(*constructs all the derivatives*)
evaluateDerivatives[\[CapitalLambda]_]:=Module[{m,n},
derValues={{0,0}};(*demonstrates what derivatives have been computed*)

n=0;
(*PrintTemporary[Dynamic[{m,n}]];*)

Do[
exprHtt[{p,e},d,{m,0}][x,t]=D[exprHtt[{p,e},d,{m-1,0}][x,t],x]//sympltt;
derValues=Append[derValues,{m,0}]
,{m,1,\[CapitalLambda]}];

Do[Do[
           If[2n+m<=\[CapitalLambda],
                exprHtt[{p,e},d,{m,n}][x,t]=D[exprHtt[{p,e},d,{m,n-1}][x,t],t]//sympltt;
                derValues=Append[derValues,{m,n}]
             ]
,{n,1,\[CapitalLambda]/2}],{m,0,\[CapitalLambda]}];

derValues(*demonstrates what derivatives have been computed*)

];


(*constructs a replacement rule for derivatives*)
createListDerivatives[\[CapitalLambda]_]:=Module[{},

derValues=evaluateDerivatives[\[CapitalLambda]];

Table[
Htt[{p_,e_},d_,derValues[[i]]][x,t]->exprHtt[{p,e},d,derValues[[i]]][x,t]/.{x->0,t->0}/.Htt[A__][0,0]:>Htt[A]
,{i,1,derValues//Length}]

];


(*writeSeedDerivativesRecursion[\[CapitalLambda]_]:=Module[{},

writeLog["writeSeedDerivativesRecursion: Creating precomputed derivative table.\n"];

seedRule=safeImport[FileNameJoin[{dataDir,"seed_recursion_formula_xt.m"}]];
exprHtt[{p,e},d,{0,0}][x,t]=Htt[{p,e},d,{0,0}][x,t]/.seedRule;
seedOut=createListDerivatives[\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedPrecomputedRecursionFileName["primal",\[CapitalLambda]]<>"\n"];
safeExport[
seedPrecomputedRecursionFileName["primal",\[CapitalLambda]],
(*FileNameJoin[{dataDir,"seed_recursion_derivatives.m"}],*)
seedOut];

(*seedOut*)

];*)


(*writeDualSeedDerivativesRecursion[\[CapitalLambda]_]:=Module[{},

writeLog["writeDualSeedDerivativesRecursion: Creating precomputed derivative table.\n"];

dualSeedRule=safeImport[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt.m"}]];
exprHtt[{p,e},d,{0,0}][x,t]=Htt[{p,e},d,{0,0}][x,t]/.dualSeedRule;
dualSeedOut=createListDerivatives[\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedPrecomputedRecursionFileName["dual",\[CapitalLambda]]<>"\n"];
safeExport[
seedPrecomputedRecursionFileName["dual",\[CapitalLambda]],
(*FileNameJoin[{dataDir,"dual_seed_recursion_derivatives.m"}],*)
dualSeedOut];

(*dualSeedOut*)

];*)


(* ::Section::Closed:: *)
(*Forming Replacement Rules: explicit p and e (deprecated -- non-permuted)*)


(*computeSeedDerivativesRecursionP[pVal_,\[CapitalLambda]_]:=Module[{reducedTable},
(*seedOut={};*)

writeLog["computeSeedDerivativesRecursionP: restricting precomputed derivative table to p="<>ToString[pVal]<>" lambda="<>ToString[\[CapitalLambda]]<>"\n"];

seedRelation=safeImport[seedPrecomputedRecursionFileName["primal",maximalPrecomputedLambda["primal"]]];
(*FileNameJoin[{dataDir,"seed_recursion_derivatives.m"}]*)

(*
Do[
{m,n}=seedRelation[[i]][[1]][[3]];
If[m+2n<=\[CapitalLambda],
temp=Table[Htt[{pVal,e},d_,{m,n}]->seedRelation[[i]][[2]]/.p->pVal,{e,0,pVal}]//removeVanishingHtt;
seedOut=Append[seedOut,temp]
],{i,1,seedRelation//Length}];

seedOut=seedOut//Flatten;
*)

reducedTable=Cases[seedRelation,c:(Htt[__,{n_,m_}]->_):>c/;n+2m<=\[CapitalLambda]];
reducedTable=reducedTable/.Dispatch[(Htt[_,arg_,{n_,m_}]->b_):>(Htt[{p,e},arg,{n,m}]->b)];
reducedTable=reducedTable/.Dispatch[{p->pVal}];
reducedTable=Table[reducedTable/.Dispatch[{e->eval}],{eval,0,pVal}]//Flatten;
reducedTable=reducedTable/.Dispatch[{Htt[{p_,e_},__]/;e<0||e>p->0}];

reducedTable

];*)


(*computeDualSeedDerivativesRecursionP[pVal_,\[CapitalLambda]_]:=Module[{dualSeedRelation,reducedTable},

writeLog["computeDualSeedDerivativesRecursionP: restricting precomputed derivative table to p="<>ToString[pVal]<>" lambda="<>ToString[\[CapitalLambda]]<>"\n"];
dualSeedRelation=safeImport[seedPrecomputedRecursionFileName["dual",maximalPrecomputedLambda["dual"]]];
(*safeImport[FileNameJoin[{dataDir,"dual_seed_recursion_derivatives.m"}]];*)

reducedTable=Cases[dualSeedRelation,c:(Htt[__,{n_,m_}]->_):>c/;n+2m<=\[CapitalLambda]];
reducedTable=reducedTable/.Dispatch[(Htt[_,arg_,{n_,m_}]->b_):>(Htt[{p,e},arg,{n,m}]->b)];
reducedTable=reducedTable/.Dispatch[{p->pVal}];
reducedTable=Table[reducedTable/.Dispatch[{e->eval}],{eval,0,pVal}]//Flatten;
reducedTable=reducedTable/.Dispatch[{Htt[{p_,e_},__]/;e<0||e>p->0}];

reducedTable

];*)


(*writeSeedDerivativesRecursionP[pVal_,\[CapitalLambda]_]:=Module[{},

seedOut=computeSeedDerivativesRecursionP[pVal,\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursionP: Writing file "<>seedRecursionTableFileName[pVal,\[CapitalLambda]]<>"\n"];
safeExport[seedRecursionTableFileName[pVal,\[CapitalLambda]],seedOut];

];*)


(*writeDualSeedDerivativesRecursionP[pVal_,\[CapitalLambda]_]:=Module[{},

dualSeedOut=computeDualSeedDerivativesRecursionP[pVal,\[CapitalLambda]];
writeLog["writeDualSeedDerivativesRecursionP: Writing file "<>dualSeedRecursionTableFileName[pVal,\[CapitalLambda]]<>"\n"];
safeExport[dualSeedRecursionTableFileName[pVal,\[CapitalLambda]],dualSeedOut];

];*)


(* ::Chapter:: *)
(*Permuted seeds*)


(* ::Section:: *)
(*Convertor: (z,zb) to (x,t)*)


(* ::Subsection:: *)
(*Recursion Relations*)


(*
It is convenient to use variables {ap=-((\[CapitalDelta]1-\[CapitalDelta]2-p/2)/2),bp=(\[CapitalDelta]3-\[CapitalDelta]4-p/2)/2}.
One can notice that in the recursion relations [p,\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4]\[Rule][p-1,\[CapitalDelta]1,\[CapitalDelta]2+1/2,\[CapitalDelta]3,\[CapitalDelta]4+1/2] the parameters ap and bp remain the same
*)

(*we will not keep an explicit dependence of Htt on ap,bp and l since they do not change in the recursion procedure*)


zperm[p_,x_]:=zperm[p,x]=Module[{cf,L},
	cf={0,x,1,L}[[p]];
	Limit[((cf[[1]]-cf[[2]])(cf[[3]]-cf[[4]]))/((cf[[1]]-cf[[3]])(cf[[2]]-cf[[4]])),L->\[Infinity]]
];
invperm[p_]:=Module[{pi={0,0,0,0}},
	pi[[p[[1]]]]=1;
	pi[[p[[2]]]]=2;
	pi[[p[[3]]]]=3;
	pi[[p[[4]]]]=4;
	pi
];


prefactorRatio[ds_,perm_,z_,zb_]:=Module[
	{
	x={0,z,1,L},
	xb={0,zb,1,L},
	xi,
	xbi,
	di,kn,kd
	},
	di=ds[[invperm[perm]]];
	xi=x[[invperm[perm]]];
	xbi=xb[[invperm[perm]]];
	kd=(1/((x[[1]]-x[[2]])(xb[[1]]-xb[[2]])))^((ds[[1]]+ds[[2]])/2);
	kn=L^(2 ds[[4]]) ((xi[[2]]-xi[[4]])/(xi[[1]]-xi[[4]]) (xbi[[2]]-xbi[[4]])/(xbi[[1]]-xbi[[4]]))^((di[[1]]-di[[2]])/2) ((xi[[1]]-xi[[4]])/(xi[[1]]-xi[[3]]) (xbi[[1]]-xbi[[4]])/(xbi[[1]]-xbi[[3]]))^((di[[3]]-di[[4]])/2) (1/((xi[[1]]-xi[[2]])(xbi[[1]]-xbi[[2]])))^((di[[1]]+di[[2]])/2)
	(1/((xi[[3]]-xi[[4]])(xbi[[3]]-xbi[[4]])))^((di[[3]]+di[[4]])/2);
	Limit[kn,L->\[Infinity]]/kd//Simplify
]


permuteRecursion[perm_,seed_]:=Module[{tmp,zn,zbn,rule,
	j={0,p/2,0,p/2},
	jt={0,(p-1)/2,0,(p-1)/2},
	ds={\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},dt={\[CapitalDelta]1,\[CapitalDelta]2+1/2,\[CapitalDelta]3,\[CapitalDelta]4+1/2},di,jti,ji,dti,pR
	},
	di=ds[[invperm[perm]]];
	dti=dt[[invperm[perm]]];
	jti=jt[[invperm[perm]]];
	ji=j[[invperm[perm]]];
	writeLog["permuteRecursion: starting "<>ToString[perm]<>" permutation...\n"];
	tmp=seed/.
	{
		H[args__,{n_,m_}][z,zb]:>Derivative[n,m][H0[args]][z,zb] (* temporarily switching to Mathematica derivatives *)
	};
	tmp=Collect[tmp,H0[__][__]|Derivative[__][__][__],Factor];
	tmp=tmp/.
	{
		(H0[pe_,int_,ext_]:>Function[{z0,zb0},
			pR H[pe,int,ext[[invperm[perm]]],{0,0}][zn,zbn]])/.{zn->zperm[invperm[perm],z0$],zbn->zperm[invperm[perm],zb0$],pR->prefactorRatio[dt+jt,perm,z0$,zb0$]}
	};
	tmp=Collect[tmp,H[__][__],Factor];
	tmp=Collect[tmp*prefactorRatio[ds+j,perm,z,zb]^-1,H[__][__],Factor];
	tmp=tmp/.
	{
		z->zperm[perm,z],
		zb->zperm[perm,zb],
		\[CapitalDelta]1->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[1]]]],
		\[CapitalDelta]2->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[2]]]],
		\[CapitalDelta]3->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[3]]]],
		\[CapitalDelta]4->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[4]]]]
	};
	writeLog["permuteRecursion: simplifying\n"];
	tmp=Collect[tmp,H[___][___]|Derivative[__][_][__],Simplify]/.{
		H[argb__][argc__]:>H[argb]@@Simplify[{argc}],
		HoldPattern[Derivative[ns__][H[argb__]][argc__]]:>Derivative[ns][H[argb]]@@Simplify[{argc}]
	};
	writeLog["permuteRecursion: done.\n"];
	tmp
];


(*writeSeedRecursion:=Module[{},

seed=safeGet[
	FileNameJoin[{dataDir,"seed_recursion_formula_zzb.m"}]
];

seedStripped=1/singleSeedPrefactor[\[CapitalDelta],p] seed//Factor;
seedStrippedNewVariables=seedStripped//makeChangeVariables//Factor//sympltt;

seedStrippedNewVariablesNewParameters=seedStrippedNewVariables/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1-p/2,\[CapitalDelta]3->2bp+\[CapitalDelta]4+p/2,\[CapitalDelta]->d}/. Htt[{p_,e_},{d_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}][x,t]:> Htt[{p,e},d,{m,n}][x,t]//sympltt;

seedStrippedNewVariablesNewParametersRule=Htt[{p_,e_},d_,{0,0}][x,t]->seedStrippedNewVariablesNewParameters;

safeExport[FileNameJoin[{dataDir,"seed_recursion_formula_xt.m"}],seedStrippedNewVariablesNewParametersRule];

(*seedStrippedNewVariablesNewParametersRule*)
]*)


writeSeedRecursion[perm_]:=Module[{js={0,p/2,0,p/2},jnew},

seed=safeGet[
	FileNameJoin[{dataDir,"seed_recursion_formula_zzb.m"}]
];

seed = permuteRecursion[perm,seed];
seedStripped=Collect[1/singleSeedPrefactor[\[CapitalDelta],p] seed,H[__][__],Factor];
seedStrippedNewVariables=Collect[seedStripped//makeChangeVariables,Htt[__][__],Factor@Expand[#,x|t]&];
(*Collect[seedStripped//makeChangeVariables,Htt[__][__],Factor]//sympltt;*)

jnew=js[[invperm[perm]]];
seedStrippedNewVariablesNewParameters=seedStrippedNewVariables/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1+jnew[[1]]-jnew[[2]],\[CapitalDelta]3->2bp+\[CapitalDelta]4+jnew[[4]]-jnew[[3]],\[CapitalDelta]->d}/. Htt[{p_,e_},{d_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}][x,t]:> Htt[{p,e},d,{m,n}][x,t]//sympltt;

seedStrippedNewVariablesNewParametersRule=Htt[{p_,e_},d_,{0,0}][x,t]->seedStrippedNewVariablesNewParameters;

writeLog["writeSeedRecursion : writing to "<>"seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m\n"];
safeExport[FileNameJoin[{dataDir,"seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m"}],seedStrippedNewVariablesNewParametersRule];

(*seedStrippedNewVariablesNewParametersRule*)
]


(*writeDualSeedRecursion:=Module[{},

dualSeed=safeGet[
	FileNameJoin[{dataDir,"dual_seed_recursion_formula_zzb.m"}]
];
dualSeedStripped=1/singleDualSeedPrefactor[\[CapitalDelta],p] dualSeed//Factor;

dualSeedStrippedNewVariables=dualSeedStripped//makeChangeVariables//Factor//sympltt;

dualSeedStrippedNewVariablesNewParameters=dualSeedStrippedNewVariables/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1-p/2,\[CapitalDelta]3->2bp+\[CapitalDelta]4+p/2,\[CapitalDelta]->d}/. Htt[{p_,e_},{d_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}][x,t]:> Htt[{p,e},d,{m,n}][x,t]//sympltt;

dualSeedStrippedNewVariablesNewParametersRule=Htt[{p_,e_},d_,{0,0}][x,t]->dualSeedStrippedNewVariablesNewParameters;
safeExport[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt.m"}],dualSeedStrippedNewVariablesNewParametersRule];

(*dualSeedStrippedNewVariablesNewParametersRule*)

]*)


singleDualSeedPrefactor[perm_,\[CapitalDelta]_,p_]:=(
singleDualSeedPrefactor[\[CapitalDelta],p]/.{ap->-(\[CapitalDelta]1-\[CapitalDelta]2-p/2)/2,bp->(\[CapitalDelta]3-\[CapitalDelta]4-p/2)/2}/.{\[CapitalDelta]1->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[1]]]],
		\[CapitalDelta]2->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[2]]]],
		\[CapitalDelta]3->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[3]]]],
		\[CapitalDelta]4->{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}[[perm[[4]]]]}
);


writeDualSeedRecursion[perm_]:=Module[{js={0,p/2,0,p/2},jnew},

dualSeed=safeGet[
	FileNameJoin[{dataDir,"dual_seed_recursion_formula_zzb.m"}]
];

dualSeed = permuteRecursion[perm,dualSeed];

dualSeedStripped=Collect[1/singleDualSeedPrefactor[perm,\[CapitalDelta],p] dualSeed,H[__][__],Factor];

dualSeedStrippedNewVariables=Collect[dualSeedStripped//makeChangeVariables,Htt[__][__],Factor@Expand[#,x|t]&];
(*Collect[dualSeedStripped//makeChangeVariables,H[__][__],Factor]//sympltt;*)

jnew=js[[invperm[perm]]];
dualSeedStrippedNewVariablesNewParameters=dualSeedStrippedNewVariables/.{\[CapitalDelta]2->2ap+\[CapitalDelta]1+jnew[[1]]-jnew[[2]],\[CapitalDelta]3->2bp+\[CapitalDelta]4+jnew[[4]]-jnew[[3]],\[CapitalDelta]->d}/. Htt[{p_,e_},{d_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}][x,t]:> Htt[{p,e},d,{m,n}][x,t]//sympltt;

dualSeedStrippedNewVariablesNewParametersRule=Htt[{p_,e_},d_,{0,0}][x,t]->dualSeedStrippedNewVariablesNewParameters;

writeLog["writeSeedRecursion : writing to "<>"dual_seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m\n"];
safeExport[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m"}],dualSeedStrippedNewVariablesNewParametersRule];

(*dualSeedStrippedNewVariablesNewParametersRule*)

]


singleDualSeedPrefactor[\[CapitalDelta],p]


singleSeedPrefactor[\[CapitalDelta],p]


(* ::Subsection:: *)
(*Recursion Relations Derivatives*)


(*
we use the standard convention for \[CapitalLambda]:
\[CapitalLambda] is the total number of derivatives with respect to (x,y)-variables;
odd derivatives in y-variables vanish;
since we deal instead of y- with t-variable; we count every tt-derivative twice
*)


(*constructs all the derivatives*)
(*evaluateDerivatives[\[CapitalLambda]_]:=Module[{m,n},
derValues={{0,0}};(*demonstrates what derivatives have been computed*)

n=0;
(*PrintTemporary[Dynamic[{m,n}]];*)

Do[
exprHtt[{p,e},d,{m,0}][x,t]=D[exprHtt[{p,e},d,{m-1,0}][x,t],x]//sympltt;
derValues=Append[derValues,{m,0}]
,{m,1,\[CapitalLambda]}];

Do[Do[
           If[2n+m<=\[CapitalLambda],
                exprHtt[{p,e},d,{m,n}][x,t]=D[exprHtt[{p,e},d,{m,n-1}][x,t],t]//sympltt;
                derValues=Append[derValues,{m,n}]
             ]
,{n,1,\[CapitalLambda]/2}],{m,0,\[CapitalLambda]}];

derValues(*demonstrates what derivatives have been computed*)

];*)


(*constructs a replacement rule for derivatives -- replaced the implementation below -- this one is too slow*)
(*createListDerivatives[\[CapitalLambda]_]:=Module[{},

derValues=evaluateDerivatives[\[CapitalLambda]];

Table[
Htt[{p_,e_},d_,derValues[[i]]][x,t]->exprHtt[{p,e},d,derValues[[i]]][x,t]/.{x->0,t->0}/.Htt[A__][0,0]:>Htt[A]
,{i,1,derValues//Length}]

];*)


createListDerivatives[\[CapitalLambda]_]:=Module[{tmp},
	tmp=Collect[exprHtt[{p,e},d,{0,0}][x,t],Htt[__][__],Factor];
	tmp=tmp/.{x->\[Epsilon]*x,t->\[Epsilon]^2 t};
	tmp=Normal@Series[tmp,{\[Epsilon],0,\[CapitalLambda]}]/.{\[Epsilon]->1};
	tmp=CoefficientList[tmp,{x,t}];
	Flatten@Table[
		Rule@@{ (* Fooling Module scoping *)
			Htt[{p_,e_},d_,{n,m}],
			Collect[n!m!(tmp[[n+1,m+1]]/.Htt[A__][_,_]:>Htt[A]),Htt[__],Factor]
			},
		{n,0,\[CapitalLambda]},
		{m,0,(\[CapitalLambda]-n)/2}
	]
]


(*writeSeedDerivativesRecursion[\[CapitalLambda]_]:=Module[{},

writeLog["writeSeedDerivativesRecursion: Creating precomputed derivative table.\n"];

seedRule=safeImport[FileNameJoin[{dataDir,"seed_recursion_formula_xt.m"}]];
exprHtt[{p,e},d,{0,0}][x,t]=Htt[{p,e},d,{0,0}][x,t]/.seedRule;
seedOut=createListDerivatives[\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedPrecomputedRecursionFileName["primal",\[CapitalLambda]]<>"\n"];
safeExport[
seedPrecomputedRecursionFileName["primal",\[CapitalLambda]],
(*FileNameJoin[{dataDir,"seed_recursion_derivatives.m"}],*)
seedOut];

(*seedOut*)

];*)


writeSeedDerivativesRecursion[\[CapitalLambda]_,perm_]:=Module[{},

writeLog["writeSeedDerivativesRecursion: Creating precomputed derivative table.\n"];

If[!FileExistsQ[FileNameJoin[{dataDir,"seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m"}]],
	writeLog["writeSeedDerivativesRecursion: missing seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m, creating...\n"]
	writeSeedRecursion[perm];
];
seedRule=safeImport[FileNameJoin[{dataDir,"seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m"}]];
exprHtt[{p,e},d,{0,0}][x,t]=Htt[{p,e},d,{0,0}][x,t]/.seedRule;
seedOut=createListDerivatives[\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedPrecomputedRecursionFileName["primal_"<>StringJoin[ToString/@perm],\[CapitalLambda]]<>"\n"];
safeExport[
seedPrecomputedRecursionFileName["primal_"<>StringJoin[ToString/@perm],\[CapitalLambda]],
(*FileNameJoin[{dataDir,"seed_recursion_derivatives.m"}],*)
seedOut];

(*seedOut*)

];


(*writeDualSeedDerivativesRecursion[\[CapitalLambda]_]:=Module[{},

writeLog["writeDualSeedDerivativesRecursion: Creating precomputed derivative table.\n"];

dualSeedRule=safeImport[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt.m"}]];
exprHtt[{p,e},d,{0,0}][x,t]=Htt[{p,e},d,{0,0}][x,t]/.dualSeedRule;
dualSeedOut=createListDerivatives[\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedPrecomputedRecursionFileName["dual",\[CapitalLambda]]<>"\n"];
safeExport[
seedPrecomputedRecursionFileName["dual",\[CapitalLambda]],
(*FileNameJoin[{dataDir,"dual_seed_recursion_derivatives.m"}],*)
dualSeedOut];

(*dualSeedOut*)

];*)


writeDualSeedDerivativesRecursion[\[CapitalLambda]_,perm_]:=Module[{},

writeLog["writeDualSeedDerivativesRecursion: Creating precomputed derivative table.\n"];

If[!FileExistsQ[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m"}]],
	writeLog["writeDualSeedDerivativesRecursion: missing dual_seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m, creating...\n"]
	writeDualSeedRecursion[perm];
];
dualSeedRule=safeImport[FileNameJoin[{dataDir,"dual_seed_recursion_formula_xt_"<>StringJoin[ToString/@perm]<>".m"}]];
exprHtt[{p,e},d,{0,0}][x,t]=Htt[{p,e},d,{0,0}][x,t]/.dualSeedRule;
dualSeedOut=createListDerivatives[\[CapitalLambda]];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedPrecomputedRecursionFileName["dual_"<>StringJoin[ToString/@perm],\[CapitalLambda]]<>"\n"];
safeExport[
seedPrecomputedRecursionFileName["dual_"<>StringJoin[ToString/@perm],\[CapitalLambda]],
(*FileNameJoin[{dataDir,"dual_seed_recursion_derivatives.m"}],*)
dualSeedOut];

(*dualSeedOut*)

];


(* ::Section:: *)
(*Forming Replacement Rules: explicit p and e*)


computeSeedDerivativesRecursion[pVal_,\[CapitalLambda]_,perm_]:=Module[{reducedTable},
(*seedOut={};*)

writeLog["computeSeedDerivativesRecursion: restricting precomputed derivative table to p="<>ToString[pVal]
	<>" lambda="<>ToString[\[CapitalLambda]]
	<>" perm="<>ToString[perm]<>"\n"];

If[maximalPrecomputedLambda["primal_"<>StringJoin[ToString/@perm]]<\[CapitalLambda],
	writeSeedDerivativesRecursion[\[CapitalLambda],perm];
];
seedRelation=safeImport[seedPrecomputedRecursionFileName["primal_"<>StringJoin[ToString/@perm],maximalPrecomputedLambda["primal_"<>StringJoin[ToString/@perm]]]];
(*FileNameJoin[{dataDir,"seed_recursion_derivatives.m"}]*)

(*
Do[
{m,n}=seedRelation[[i]][[1]][[3]];
If[m+2n<=\[CapitalLambda],
temp=Table[Htt[{pVal,e},d_,{m,n}]->seedRelation[[i]][[2]]/.p->pVal,{e,0,pVal}]//removeVanishingHtt;
seedOut=Append[seedOut,temp]
],{i,1,seedRelation//Length}];

seedOut=seedOut//Flatten;
*)

reducedTable=Cases[seedRelation,c:(Htt[__,{n_,m_}]->_):>c/;n+2m<=\[CapitalLambda]];
reducedTable=reducedTable/.Dispatch[(Htt[_,arg_,{n_,m_}]->b_):>(Htt[{p,e},arg,{n,m}]->b)];
reducedTable=reducedTable/.Dispatch[{p->pVal}];
reducedTable=Table[reducedTable/.Dispatch[{e->eval}],{eval,0,pVal}]//Flatten;
reducedTable=reducedTable/.Dispatch[{Htt[{p_,e_},__]/;e<0||e>p->0}];

reducedTable

];


computeDualSeedDerivativesRecursion[pVal_,\[CapitalLambda]_,perm_]:=Module[{dualSeedRelation,reducedTable},

writeLog["computeDualSeedDerivativesRecursion: restricting precomputed derivative table to p="<>ToString[pVal]
	<>" lambda="<>ToString[\[CapitalLambda]]
	<>" perm="<>ToString[perm]<>"\n"];
If[maximalPrecomputedLambda["dual_"<>StringJoin[ToString/@perm]]<\[CapitalLambda],
	writeDualSeedDerivativesRecursion[\[CapitalLambda],perm];
];
dualSeedRelation=safeImport[seedPrecomputedRecursionFileName["dual_"<>StringJoin[ToString/@perm],maximalPrecomputedLambda["dual_"<>StringJoin[ToString/@perm]]]];
(*safeImport[FileNameJoin[{dataDir,"dual_seed_recursion_derivatives.m"}]];*)

reducedTable=Cases[dualSeedRelation,c:(Htt[__,{n_,m_}]->_):>c/;n+2m<=\[CapitalLambda]];
reducedTable=reducedTable/.Dispatch[(Htt[_,arg_,{n_,m_}]->b_):>(Htt[{p,e},arg,{n,m}]->b)];
reducedTable=reducedTable/.Dispatch[{p->pVal}];
reducedTable=Table[reducedTable/.Dispatch[{e->eval}],{eval,0,pVal}]//Flatten;
reducedTable=reducedTable/.Dispatch[{Htt[{p_,e_},__]/;e<0||e>p->0}];

reducedTable

];


writeSeedDerivativesRecursion[pVal_,\[CapitalLambda]_,perm_]:=Module[{},

seedOut=computeSeedDerivativesRecursion[pVal,\[CapitalLambda],perm];
writeLog["writeSeedDerivativesRecursion: Writing file "<>seedRecursionTableFileName[pVal,\[CapitalLambda],perm]<>"\n"];
safeExport[seedRecursionTableFileName[pVal,\[CapitalLambda],perm],seedOut];

];


writeDualSeedDerivativesRecursion[pVal_,\[CapitalLambda]_,perm_]:=Module[{},

dualSeedOut=computeDualSeedDerivativesRecursion[pVal,\[CapitalLambda],perm];
writeLog["writeDualSeedDerivativesRecursion: Writing file "<>dualSeedRecursionTableFileName[pVal,\[CapitalLambda],perm]<>"\n"];
safeExport[dualSeedRecursionTableFileName[pVal,\[CapitalLambda],perm],dualSeedOut];

];
