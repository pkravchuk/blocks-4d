(I*(-1 + e - p)*z*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
          H[{-1 + p, -2 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 0}][z, zb])/
         ((l + p)*(-4 + p + 2*\[CapitalDelta])*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]1 - \[CapitalDelta]2)) + 
        (2*I*(-1 + e - p)*z*zb^2*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (z^3*(-1 + zb)*(p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2) - 
           z^2*(2*l*(-1 + zb) + p*(-3 + zb + 2*zb^2) - 
             2*(-4 + \[CapitalDelta] + 2*zb^2*(-3 + \[CapitalDelta]1 - 
                 \[CapitalDelta]2) - zb*(-8 + \[CapitalDelta] + 
                 \[CapitalDelta]1 - \[CapitalDelta]2 + \[CapitalDelta]3 - 
                 \[CapitalDelta]4))) + z*zb*(4 + 4*l*(-1 + zb) + 4*zb^2 + 
             p*(-6 + 5*zb + zb^2) - 4*\[CapitalDelta] + 
             4*zb*\[CapitalDelta] - 2*zb*\[CapitalDelta]1 - 
             2*zb^2*\[CapitalDelta]1 + 2*zb*\[CapitalDelta]2 + 
             2*zb^2*\[CapitalDelta]2 + 4*zb*\[CapitalDelta]3 - 
             4*zb*\[CapitalDelta]4) + zb^2*(-2*l*(-1 + zb) - 3*p*(-1 + zb) - 
             2*(2 - \[CapitalDelta] + zb*\[CapitalDelta] - zb*
                \[CapitalDelta]1 + zb*\[CapitalDelta]2 + zb*
                \[CapitalDelta]3 - zb*\[CapitalDelta]4)))*
          H[{-1 + p, -2 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 1}][z, zb])/((l + p)*(z - zb)^3*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (8*I*(-1 + e - p)*(-1 + z)*z*(-1 + zb)*zb^4*(-2 + 2*l + p + 
           2*\[CapitalDelta])*H[{-1 + p, -2 + e}, {-(1/2) + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 2}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (2*I*(-1 + e - p)*z^2*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (zb^2*(2*l - p*(-3 + zb) + 2*(-4 + \[CapitalDelta] + zb*
                \[CapitalDelta]1 - zb*\[CapitalDelta]2)) + 
           z^2*(l*(2 + 4*zb) + p*(3 + 5*zb - 2*zb^2) + 
             2*(-2 + \[CapitalDelta] + 2*zb^2*(-3 + \[CapitalDelta]1 - 
                 \[CapitalDelta]2) + zb*(2*\[CapitalDelta] - 
                 \[CapitalDelta]1 + \[CapitalDelta]2 + 2*\[CapitalDelta]3 - 
                 2*\[CapitalDelta]4))) + z*zb*(-2*l*(2 + zb) + 
             p*(-6 - zb + zb^2) - 2*(2*(-1 + \[CapitalDelta]) + zb^2*
                (\[CapitalDelta]1 - \[CapitalDelta]2) + zb*(-8 + 
                 \[CapitalDelta] + \[CapitalDelta]1 - \[CapitalDelta]2 + 
                 \[CapitalDelta]3 - \[CapitalDelta]4))) + 
           z^3*(-2*l + p*(-3 + zb) - 2*(\[CapitalDelta] - \[CapitalDelta]1 + 
               zb*(-2 + \[CapitalDelta]1 - \[CapitalDelta]2) + 
               \[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4)))*
          H[{-1 + p, -2 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 0}][z, zb])/((l + p)*(z - zb)^3*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (8*I*(-1 + e - p)*(-1 + z)*z^2*(-1 + zb)*zb^2*(z + zb)*
          (-2 + 2*l + p + 2*\[CapitalDelta])*H[{-1 + p, -2 + e}, 
            {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 1}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (8*I*(-1 + e - p)*(-1 + z)*z^4*(-1 + zb)*zb*(-2 + 2*l + p + 
           2*\[CapitalDelta])*H[{-1 + p, -2 + e}, {-(1/2) + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 0}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (I*(-2 + 2*l + p + 2*\[CapitalDelta])*(8*l^3 + 
           p^3*(-9 + 9*z + 9*zb - 3*z*zb) + 4*l^2*(4*e + 5*p + 
             6*(-2 + \[CapitalDelta])) + 2*p^2*(2*e*(-3 + z)*(-3 + zb) + 
             3*(-2 + (1 + zb)*\[CapitalDelta] + zb*(-3*\[CapitalDelta]1 + 
                 3*\[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
             z*(zb*(-6 + 3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 + 
                 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) + 3*
                (\[CapitalDelta] - 3*\[CapitalDelta]1 + 3*\[CapitalDelta]2 + 
                 \[CapitalDelta]3 - \[CapitalDelta]4))) + 
           8*(2*(-3 + e)*\[CapitalDelta]^2 + \[CapitalDelta]^3 + 
             z*zb*(\[CapitalDelta]1 - \[CapitalDelta]2)*
              (-2 + \[CapitalDelta]3 - \[CapitalDelta]4)*
              (2*e - \[CapitalDelta]3 + \[CapitalDelta]4) + 
             \[CapitalDelta]*(8 + 2*e*(-4 + zb*\[CapitalDelta]1 + 
                 z*(\[CapitalDelta]1 - \[CapitalDelta]2) - 
                 zb*\[CapitalDelta]2) - zb*\[CapitalDelta]1*
                \[CapitalDelta]3 + zb*\[CapitalDelta]2*\[CapitalDelta]3 - z*
                (\[CapitalDelta]1 - \[CapitalDelta]2)*(\[CapitalDelta]3 - 
                 \[CapitalDelta]4) + zb*\[CapitalDelta]1*\[CapitalDelta]4 - 
               zb*\[CapitalDelta]2*\[CapitalDelta]4)) - 
           2*l*(-3*p^2*(1 + z + zb) + 4*e*(8 + p*(-6 + z + zb) - 4*
                \[CapitalDelta] - 2*z*\[CapitalDelta]1 - 2*zb*
                \[CapitalDelta]1 + 2*z*\[CapitalDelta]2 + 2*zb*
                \[CapitalDelta]2) - 2*p*(-20 + 10*\[CapitalDelta] - 3*zb*
                \[CapitalDelta]1 + 3*zb*\[CapitalDelta]2 + zb*
                \[CapitalDelta]3 + z*(-3*\[CapitalDelta]1 + 
                 3*\[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4) - 
               zb*\[CapitalDelta]4) + 4*(-8 + 12*\[CapitalDelta] - 3*
                \[CapitalDelta]^2 + zb*\[CapitalDelta]1*\[CapitalDelta]3 - zb*
                \[CapitalDelta]2*\[CapitalDelta]3 + z*(\[CapitalDelta]1 - 
                 \[CapitalDelta]2)*(\[CapitalDelta]3 - \[CapitalDelta]4) - zb*
                \[CapitalDelta]1*\[CapitalDelta]4 + zb*\[CapitalDelta]2*
                \[CapitalDelta]4)) - 4*p*(-24 - 5*\[CapitalDelta]^2 - 
             6*z*zb*\[CapitalDelta]1 + 6*z*zb*\[CapitalDelta]2 + 
             2*z*zb*\[CapitalDelta]3 + 3*z*\[CapitalDelta]1*
              \[CapitalDelta]3 + 3*zb*\[CapitalDelta]1*\[CapitalDelta]3 + 
             2*z*zb*\[CapitalDelta]1*\[CapitalDelta]3 - 3*z*\[CapitalDelta]2*
              \[CapitalDelta]3 - 3*zb*\[CapitalDelta]2*\[CapitalDelta]3 - 
             2*z*zb*\[CapitalDelta]2*\[CapitalDelta]3 - 
             z*zb*\[CapitalDelta]3^2 + 2*e*(12 + (-6 + zb)*\[CapitalDelta] - 
               3*zb*\[CapitalDelta]1 + 3*zb*\[CapitalDelta]2 + z*
                (\[CapitalDelta] - 3*\[CapitalDelta]1 + 3*\[CapitalDelta]2 + 
                 zb*(-2 + \[CapitalDelta]1 - \[CapitalDelta]2 + 
                   \[CapitalDelta]3 - \[CapitalDelta]4))) - 
             2*z*zb*\[CapitalDelta]4 - 3*z*\[CapitalDelta]1*
              \[CapitalDelta]4 - 3*zb*\[CapitalDelta]1*\[CapitalDelta]4 - 
             2*z*zb*\[CapitalDelta]1*\[CapitalDelta]4 + 3*z*\[CapitalDelta]2*
              \[CapitalDelta]4 + 3*zb*\[CapitalDelta]2*\[CapitalDelta]4 + 
             2*z*zb*\[CapitalDelta]2*\[CapitalDelta]4 + 
             2*z*zb*\[CapitalDelta]3*\[CapitalDelta]4 - 
             z*zb*\[CapitalDelta]4^2 + \[CapitalDelta]*(20 + z*
                (3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4) + zb*(3*\[CapitalDelta]1 - 
                 3*\[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4))))*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 0}][z, zb])/
         (16*(l + p)*(-4 + p + 2*\[CapitalDelta])*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]1 - \[CapitalDelta]2)*
          (-2 + l + p + \[CapitalDelta] + \[CapitalDelta]3 - 
           \[CapitalDelta]4)) + (I*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (-4*z*(z - zb)^2*(2 + l + p + \[CapitalDelta] - \[CapitalDelta]1 - 
             \[CapitalDelta]2)*(-2 + l + p + \[CapitalDelta] + 
             \[CapitalDelta]1 - \[CapitalDelta]2) + 4*z*(z - zb)^2*zb*
            (2 + l + p + \[CapitalDelta] - \[CapitalDelta]1 - 
             \[CapitalDelta]2)*(-2 + l + p + \[CapitalDelta] + 
             \[CapitalDelta]1 - \[CapitalDelta]2) - 
           z^3*(48 - 4*p - 7*p^2 - 24*\[CapitalDelta] - 4*p*\[CapitalDelta] + 
             4*e*(-8 + 2*l + 3*p + 2*\[CapitalDelta]) - 16*\[CapitalDelta]1 + 
             4*\[CapitalDelta]1^2 - 4*l*(6 + p - 2*\[CapitalDelta]2) + 
             8*p*\[CapitalDelta]2 + 8*\[CapitalDelta]*\[CapitalDelta]2 - 
             4*\[CapitalDelta]2^2) - z*zb^2*(48 - 4*p + 17*p^2 - 
             24*\[CapitalDelta] + 12*p*\[CapitalDelta] - 
             4*e*(8 + 2*l + 3*p + 2*\[CapitalDelta]) - 16*\[CapitalDelta]1 + 
             4*\[CapitalDelta]1^2 + 8*p*\[CapitalDelta]2 + 8*\[CapitalDelta]*
              \[CapitalDelta]2 - 4*\[CapitalDelta]2^2 + 
             4*l*(-6 + 3*p + 2*\[CapitalDelta]2)) - 
           2*z^2*zb*(p^2 + p*(36 - 6*e - 8*\[CapitalDelta]2) - 
             4*l*(-6 + e + 2*\[CapitalDelta]2) - 4*(8 - 4*\[CapitalDelta]1 + 
               \[CapitalDelta]1^2 - \[CapitalDelta]2^2 + \[CapitalDelta]*
                (-6 + e + 2*\[CapitalDelta]2))) - 
           2*zb^3*(2*l + 3*p + 2*\[CapitalDelta])*(-2*e*(-1 + zb) + 
             2*p*(-1 + zb) + zb*(2 - \[CapitalDelta]1 + \[CapitalDelta]2 + 
               \[CapitalDelta]3 - \[CapitalDelta]4)) + 
           z^4*zb*(p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
            (-4*e + 3*p + 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) + 
           z^4*(p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
            (4*e - 3*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
           2*z*zb^4*(2 - 2*e + 2*p - \[CapitalDelta]1 + \[CapitalDelta]2 + 
             \[CapitalDelta]3 - \[CapitalDelta]4)*(4 + p - 
             2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
           z^3*zb*(64 + 12*p - 5*p^2 - 24*\[CapitalDelta] - 
             4*p*\[CapitalDelta] - 8*\[CapitalDelta]1 + 
             4*p*\[CapitalDelta]1 + 4*\[CapitalDelta]1^2 - 
             4*l*(6 + p - 2*\[CapitalDelta]2) - 8*\[CapitalDelta]2 + 
             4*p*\[CapitalDelta]2 + 8*\[CapitalDelta]*\[CapitalDelta]2 - 
             4*\[CapitalDelta]2^2 - 8*\[CapitalDelta]3 - 
             4*p*\[CapitalDelta]3 - 8*\[CapitalDelta]1*\[CapitalDelta]3 + 
             8*\[CapitalDelta]2*\[CapitalDelta]3 + 8*e*(-6 + l + p + 
               \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             8*\[CapitalDelta]4 + 4*p*\[CapitalDelta]4 + 8*\[CapitalDelta]1*
              \[CapitalDelta]4 - 8*\[CapitalDelta]2*\[CapitalDelta]4) - 
           z^2*zb^2*(32 - 156*p - 7*p^2 - 40*\[CapitalDelta] - 
             16*\[CapitalDelta]1 + 20*p*\[CapitalDelta]1 - 4*\[CapitalDelta]*
              \[CapitalDelta]1 + 8*\[CapitalDelta]1^2 - 16*\[CapitalDelta]2 - 
             4*p*\[CapitalDelta]2 + 20*\[CapitalDelta]*\[CapitalDelta]2 - 
             8*\[CapitalDelta]2^2 - 16*\[CapitalDelta]3 - 
             8*p*\[CapitalDelta]3 + 4*\[CapitalDelta]*\[CapitalDelta]3 - 
             4*\[CapitalDelta]1*\[CapitalDelta]3 + 4*\[CapitalDelta]2*
              \[CapitalDelta]3 + 8*e*(12 + l + 2*p + \[CapitalDelta] - 3*
                \[CapitalDelta]1 + 3*\[CapitalDelta]2 + 2*\[CapitalDelta]3 - 
               2*\[CapitalDelta]4) + 16*\[CapitalDelta]4 + 
             8*p*\[CapitalDelta]4 - 4*\[CapitalDelta]*\[CapitalDelta]4 + 
             4*\[CapitalDelta]1*\[CapitalDelta]4 - 4*\[CapitalDelta]2*
              \[CapitalDelta]4 - 4*l*(10 + \[CapitalDelta]1 - 5*
                \[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4)) + 
           z*zb^3*(32 - 28*p + 13*p^2 - 8*\[CapitalDelta] + 
             12*p*\[CapitalDelta] - 8*\[CapitalDelta]1 + 
             4*p*\[CapitalDelta]1 - 8*\[CapitalDelta]*\[CapitalDelta]1 + 
             4*\[CapitalDelta]1^2 - 8*\[CapitalDelta]2 + 
             4*p*\[CapitalDelta]2 + 16*\[CapitalDelta]*\[CapitalDelta]2 - 
             4*\[CapitalDelta]2^2 - 8*\[CapitalDelta]3 + 
             4*p*\[CapitalDelta]3 + 8*\[CapitalDelta]*\[CapitalDelta]3 + 
             8*\[CapitalDelta]4 - 4*p*\[CapitalDelta]4 - 8*\[CapitalDelta]*
              \[CapitalDelta]4 - 8*e*(-2 + l + p + \[CapitalDelta] + 2*
                \[CapitalDelta]1 - 2*\[CapitalDelta]2 - \[CapitalDelta]3 + 
               \[CapitalDelta]4) + 4*l*(3*p - 2*(1 + \[CapitalDelta]1 - 
                 2*\[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4))) - 2*z^3*zb^2*
            (p^2 - 2*e*(p + 2*(6 - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2 + 
                 \[CapitalDelta]3 - \[CapitalDelta]4)) + 
             p*(22 - 5*\[CapitalDelta]1 + 5*\[CapitalDelta]2 + 5*
                \[CapitalDelta]3 - 5*\[CapitalDelta]4) + 
             2*(4 + \[CapitalDelta]3^2 + \[CapitalDelta]2*(-2 + 
                 3*\[CapitalDelta]3 - 3*\[CapitalDelta]4) - 2*
                \[CapitalDelta]3*\[CapitalDelta]4 + \[CapitalDelta]4^2 + 
               \[CapitalDelta]1*(2 - 3*\[CapitalDelta]3 + 
                 3*\[CapitalDelta]4))) - z^2*zb^3*(5*p^2 - 
             4*e*(8 + p + 2*\[CapitalDelta]1 - 2*\[CapitalDelta]2 - 4*
                \[CapitalDelta]3 + 4*\[CapitalDelta]4) + 
             2*p*(20 + \[CapitalDelta]1 - \[CapitalDelta]2 - 7*
                \[CapitalDelta]3 + 7*\[CapitalDelta]4) - 
             4*(\[CapitalDelta]2*(-4 + 3*\[CapitalDelta]3 - 
                 3*\[CapitalDelta]4) + \[CapitalDelta]1*(4 - 
                 3*\[CapitalDelta]3 + 3*\[CapitalDelta]4) + 2*
                (-4 + \[CapitalDelta]3^2 - 2*\[CapitalDelta]3*
                  \[CapitalDelta]4 + \[CapitalDelta]4^2))))*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 1}][z, zb])/(4*(l + p)*(z - zb)^3*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (I*(-1 + zb)*zb^3*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (zb*(2*l + 3*p + 2*\[CapitalDelta]) + 
           z^2*(8*e - 7*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) - 
           z*(-4 + 8*e + 2*l - 5*p + 4*zb + p*zb + 2*\[CapitalDelta] - 
             2*zb*\[CapitalDelta]3 + 2*zb*\[CapitalDelta]4))*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 2}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (I*z*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (zb^3*(-4 + 4*e + 2*l - p + 2*\[CapitalDelta])*(-8 + 2*l + 3*p + 
             2*\[CapitalDelta]) + z^2*zb*(32 + 4*l^2 - 4*p + 21*p^2 - 
             24*\[CapitalDelta] + 20*p*\[CapitalDelta] + 
             4*\[CapitalDelta]^2 - 4*e*(8 + 2*l + 3*p + 2*\[CapitalDelta]) + 
             4*l*(-6 + 5*p + 2*\[CapitalDelta])) - 2*z*zb^2*
            (4*l^2 + 3*p^2 + p*(-36 + 6*e + 8*\[CapitalDelta]) + 
             4*(4 + (-6 + e)*\[CapitalDelta] + \[CapitalDelta]^2) + 
             4*l*(e + 2*(-3 + p + \[CapitalDelta]))) + 
           2*z^3*(2*l + 3*p + 2*\[CapitalDelta])*(-2*e*(-1 + z) + 
             2*p*(-1 + z) + z*(2 - \[CapitalDelta]1 + \[CapitalDelta]2 + 
               \[CapitalDelta]3 - \[CapitalDelta]4)) + 
           zb^4*(p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
            (-4*e + 3*p + 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) - 
           z*zb^4*(p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
            (-4*e + 3*p + 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) + 
           2*z^4*zb*(-2 + 2*e - 2*p + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4)*(4 + p - 
             2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) - 
           z*zb^3*(48 + 4*l^2 + 12*p - p^2 - 24*\[CapitalDelta] + 
             4*p*\[CapitalDelta] + 4*\[CapitalDelta]^2 + 
             4*l*(-6 + p + 2*\[CapitalDelta]) + 8*\[CapitalDelta]1 + 
             4*p*\[CapitalDelta]1 - 8*\[CapitalDelta]2 - 
             4*p*\[CapitalDelta]2 - 8*\[CapitalDelta]3 - 
             4*p*\[CapitalDelta]3 - 8*\[CapitalDelta]1*\[CapitalDelta]3 + 
             8*\[CapitalDelta]2*\[CapitalDelta]3 + 8*e*(-6 + l + p + 
               \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             8*\[CapitalDelta]4 + 4*p*\[CapitalDelta]4 + 8*\[CapitalDelta]1*
              \[CapitalDelta]4 - 8*\[CapitalDelta]2*\[CapitalDelta]4) + 
           z^2*zb^2*(8*l^2 - 156*p + p^2 - 40*\[CapitalDelta] + 
             16*p*\[CapitalDelta] + 8*\[CapitalDelta]^2 + 
             16*\[CapitalDelta]1 + 20*p*\[CapitalDelta]1 - 4*\[CapitalDelta]*
              \[CapitalDelta]1 - 16*\[CapitalDelta]2 - 
             20*p*\[CapitalDelta]2 + 4*\[CapitalDelta]*\[CapitalDelta]2 - 
             16*\[CapitalDelta]3 - 8*p*\[CapitalDelta]3 + 4*\[CapitalDelta]*
              \[CapitalDelta]3 - 4*\[CapitalDelta]1*\[CapitalDelta]3 + 
             4*\[CapitalDelta]2*\[CapitalDelta]3 + 8*e*(12 + l + 2*p + 
               \[CapitalDelta] - 3*\[CapitalDelta]1 + 3*\[CapitalDelta]2 + 2*
                \[CapitalDelta]3 - 2*\[CapitalDelta]4) + 
             4*l*(-10 + 4*p + 4*\[CapitalDelta] - \[CapitalDelta]1 + 
               \[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             16*\[CapitalDelta]4 + 8*p*\[CapitalDelta]4 - 4*\[CapitalDelta]*
              \[CapitalDelta]4 + 4*\[CapitalDelta]1*\[CapitalDelta]4 - 
             4*\[CapitalDelta]2*\[CapitalDelta]4) - 
           z^3*zb*(16 + 4*l^2 - 28*p + 17*p^2 - 8*\[CapitalDelta] + 
             20*p*\[CapitalDelta] + 4*\[CapitalDelta]^2 + 
             8*\[CapitalDelta]1 + 4*p*\[CapitalDelta]1 - 8*\[CapitalDelta]*
              \[CapitalDelta]1 - 8*\[CapitalDelta]2 - 4*p*\[CapitalDelta]2 + 
             8*\[CapitalDelta]*\[CapitalDelta]2 - 8*\[CapitalDelta]3 + 
             4*p*\[CapitalDelta]3 + 8*\[CapitalDelta]*\[CapitalDelta]3 + 
             4*l*(5*p + 2*(-1 + \[CapitalDelta] - \[CapitalDelta]1 + 
                 \[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
             8*\[CapitalDelta]4 - 4*p*\[CapitalDelta]4 - 8*\[CapitalDelta]*
              \[CapitalDelta]4 - 8*e*(-2 + l + p + \[CapitalDelta] + 2*
                \[CapitalDelta]1 - 2*\[CapitalDelta]2 - \[CapitalDelta]3 + 
               \[CapitalDelta]4)) + 2*z^2*zb^3*(p^2 - 
             2*e*(p + 2*(6 - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2 + 
                 \[CapitalDelta]3 - \[CapitalDelta]4)) + 
             p*(22 - 5*\[CapitalDelta]1 + 5*\[CapitalDelta]2 + 5*
                \[CapitalDelta]3 - 5*\[CapitalDelta]4) + 
             2*(4 + \[CapitalDelta]3^2 + \[CapitalDelta]2*(-2 + 
                 3*\[CapitalDelta]3 - 3*\[CapitalDelta]4) - 2*
                \[CapitalDelta]3*\[CapitalDelta]4 + \[CapitalDelta]4^2 + 
               \[CapitalDelta]1*(2 - 3*\[CapitalDelta]3 + 
                 3*\[CapitalDelta]4))) + z^3*zb^2*(5*p^2 - 
             4*e*(8 + p + 2*\[CapitalDelta]1 - 2*\[CapitalDelta]2 - 4*
                \[CapitalDelta]3 + 4*\[CapitalDelta]4) + 
             2*p*(20 + \[CapitalDelta]1 - \[CapitalDelta]2 - 7*
                \[CapitalDelta]3 + 7*\[CapitalDelta]4) + 
             4*(\[CapitalDelta]1*(-4 + 3*\[CapitalDelta]3 - 
                 3*\[CapitalDelta]4) + \[CapitalDelta]2*(4 - 
                 3*\[CapitalDelta]3 + 3*\[CapitalDelta]4) - 2*
                (-4 + \[CapitalDelta]3^2 - 2*\[CapitalDelta]3*
                  \[CapitalDelta]4 + \[CapitalDelta]4^2))))*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 0}][z, zb])/(4*(l + p)*(z - zb)^3*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (2*I*z*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*(2*(e - p)*z^2 + 
           4*(-1 + e - p)*z*zb + 2*(e - p)*zb^2 + 2*z^2*zb^2*
            (2*e - 2*p - \[CapitalDelta]1 + \[CapitalDelta]2 + 
             \[CapitalDelta]3 - \[CapitalDelta]4) + 
           z^3*(2 - 2*e + 2*p - \[CapitalDelta]1 + \[CapitalDelta]2 + 
             \[CapitalDelta]3 - \[CapitalDelta]4) + 
           zb^3*(2 - 2*e + 2*p - \[CapitalDelta]1 + \[CapitalDelta]2 + 
             \[CapitalDelta]3 - \[CapitalDelta]4) + 
           z^3*zb*(-2 + 2*e - 2*p + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4) + 
           z*zb^3*(-2 + 2*e - 2*p + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4) + 
           z^2*zb*(2 - 6*e + 6*p + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4) + 
           z*zb^2*(2 - 6*e + 6*p + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4))*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 1}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (4*I*(-1 + z)*z*(-1 + zb)*zb^3*(-2 + 2*l + p + 2*\[CapitalDelta])*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 2}][z, zb])/((l + p)*(z - zb)*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (I*(-1 + z)*z^3*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (2*l*(z - zb) + p*((-z)*(-3 + zb) + (5 - 7*zb)*zb) + 
           2*(z*\[CapitalDelta] + zb^2*(4*e - \[CapitalDelta]3 + 
               \[CapitalDelta]4) - zb*(-2 + 4*e + \[CapitalDelta] + z*
                (2 - \[CapitalDelta]3 + \[CapitalDelta]4))))*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 0}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (4*I*(-1 + z)*z^3*(-1 + zb)*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          H[{-1 + p, -1 + e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 1}][z, zb])/((l + p)*(z - zb)*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (I*(-2 + 2*l + p + 2*\[CapitalDelta])*(2*l + 3*p + 2*\[CapitalDelta])*
          (-4 + l + p + \[CapitalDelta] + \[CapitalDelta]1 - 
           \[CapitalDelta]2)*(-4 - 4*e + 3*p + 2*\[CapitalDelta]3 - 
           2*\[CapitalDelta]4)*H[{-1 + p, e}, {-(1/2) + \[CapitalDelta], l}, 
            {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 0}][z, zb])/
         (16*(l + p)*(-4 + p + 2*\[CapitalDelta])*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]1 - \[CapitalDelta]2)*
          (-2 + l + p + \[CapitalDelta] + \[CapitalDelta]3 - 
           \[CapitalDelta]4)) + (I*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (16*zb^2 - 8*l*zb^2 + 8*e*l*zb^2 - 12*p*zb^2 + 12*e*p*zb^2 - 
           4*l*p*zb^2 - 7*p^2*zb^2 - 16*zb^3 + 16*l*zb^3 - 8*e*l*zb^3 + 
           24*p*zb^3 - 12*e*p*zb^3 + 4*l*p*zb^3 + 7*p^2*zb^3 - 
           8*zb^2*\[CapitalDelta] + 8*e*zb^2*\[CapitalDelta] - 
           4*p*zb^2*\[CapitalDelta] + 16*zb^3*\[CapitalDelta] - 
           8*e*zb^3*\[CapitalDelta] + 4*p*zb^3*\[CapitalDelta] - 
           16*zb^2*\[CapitalDelta]1 + 16*zb^3*\[CapitalDelta]1 - 
           4*l*zb^3*\[CapitalDelta]1 - 6*p*zb^3*\[CapitalDelta]1 - 
           4*zb^3*\[CapitalDelta]*\[CapitalDelta]1 + 
           4*zb^2*\[CapitalDelta]1^2 - 4*zb^3*\[CapitalDelta]1^2 + 
           8*l*zb^2*\[CapitalDelta]2 + 8*p*zb^2*\[CapitalDelta]2 - 
           4*l*zb^3*\[CapitalDelta]2 - 2*p*zb^3*\[CapitalDelta]2 + 
           8*zb^2*\[CapitalDelta]*\[CapitalDelta]2 - 4*zb^3*\[CapitalDelta]*
            \[CapitalDelta]2 - 4*zb^2*\[CapitalDelta]2^2 + 
           4*zb^3*\[CapitalDelta]2^2 + z^2*(16 + 4*p - 7*p^2 - 
             8*\[CapitalDelta] - 4*p*\[CapitalDelta] + 
             4*e*(-4 + 2*l + 3*p + 2*\[CapitalDelta]) - 16*\[CapitalDelta]1 + 
             4*\[CapitalDelta]1^2 - 4*l*(2 + p - 2*\[CapitalDelta]2) + 
             8*p*\[CapitalDelta]2 + 8*\[CapitalDelta]*\[CapitalDelta]2 - 
             4*\[CapitalDelta]2^2) - 4*z^2*(-1 + zb)*(-4 + l^2 + p^2 + 
             \[CapitalDelta]^2 + 4*\[CapitalDelta]1 - \[CapitalDelta]1^2 + 
             2*p*(\[CapitalDelta] - \[CapitalDelta]2) + 
             2*l*(p + \[CapitalDelta] - \[CapitalDelta]2) - 2*\[CapitalDelta]*
              \[CapitalDelta]2 + \[CapitalDelta]2^2) + 8*z*(-1 + zb)*zb*
            (-4 + l^2 + p^2 + \[CapitalDelta]^2 + 4*\[CapitalDelta]1 - 
             \[CapitalDelta]1^2 + 2*p*(\[CapitalDelta] - \[CapitalDelta]2) + 
             2*l*(p + \[CapitalDelta] - \[CapitalDelta]2) - 2*\[CapitalDelta]*
              \[CapitalDelta]2 + \[CapitalDelta]2^2) + 
           4*zb^2*(-4 + l^2 + p^2 + \[CapitalDelta]^2 + 4*\[CapitalDelta]1 - 
             \[CapitalDelta]1^2 + 2*p*(\[CapitalDelta] - \[CapitalDelta]2) + 
             2*l*(p + \[CapitalDelta] - \[CapitalDelta]2) - 2*\[CapitalDelta]*
              \[CapitalDelta]2 + \[CapitalDelta]2^2) - 
           4*zb^3*(-4 + l^2 + p^2 + \[CapitalDelta]^2 + 4*\[CapitalDelta]1 - 
             \[CapitalDelta]1^2 + 2*p*(\[CapitalDelta] - \[CapitalDelta]2) + 
             2*l*(p + \[CapitalDelta] - \[CapitalDelta]2) - 2*\[CapitalDelta]*
              \[CapitalDelta]2 + \[CapitalDelta]2^2) + 
           2*z*zb*(-16 + 20*p + 7*p^2 + 8*\[CapitalDelta] + 
             4*p*\[CapitalDelta] - 4*e*(2 + 2*l + 3*p + 2*\[CapitalDelta]) + 
             16*\[CapitalDelta]1 - 4*\[CapitalDelta]1^2 + 
             4*l*(2 + p - 2*\[CapitalDelta]2) - 8*p*\[CapitalDelta]2 - 
             8*\[CapitalDelta]*\[CapitalDelta]2 + 4*\[CapitalDelta]2^2) + 
           4*l*zb^3*\[CapitalDelta]3 + 6*p*zb^3*\[CapitalDelta]3 + 
           4*zb^3*\[CapitalDelta]*\[CapitalDelta]3 + 
           z^3*(4 + p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
            (-4 - 4*e + 3*p + 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) - 
           4*l*zb^3*\[CapitalDelta]4 - 6*p*zb^3*\[CapitalDelta]4 - 
           4*zb^3*\[CapitalDelta]*\[CapitalDelta]4 + 
           z^3*zb*(4 + 4*e - 3*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
           z*zb^3*(4*e - 3*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (4 + p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
           z*zb^2*(16 - 40*p - 11*p^2 - 32*\[CapitalDelta] - 
             8*p*\[CapitalDelta] - 24*\[CapitalDelta]1 + 
             6*p*\[CapitalDelta]1 + 8*\[CapitalDelta]*\[CapitalDelta]1 + 
             8*\[CapitalDelta]1^2 + 4*e*(-4 + 4*l + 5*p + 4*\[CapitalDelta] + 
               2*\[CapitalDelta]1 - 2*\[CapitalDelta]2) - 
             8*\[CapitalDelta]2 + 10*p*\[CapitalDelta]2 + 8*\[CapitalDelta]*
              \[CapitalDelta]2 - 8*\[CapitalDelta]2^2 + 8*\[CapitalDelta]3 - 
             10*p*\[CapitalDelta]3 - 8*\[CapitalDelta]*\[CapitalDelta]3 - 
             4*\[CapitalDelta]1*\[CapitalDelta]3 + 4*\[CapitalDelta]2*
              \[CapitalDelta]3 - 8*l*(4 + p - \[CapitalDelta]1 - 
               \[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4) - 
             8*\[CapitalDelta]4 + 10*p*\[CapitalDelta]4 + 8*\[CapitalDelta]*
              \[CapitalDelta]4 + 4*\[CapitalDelta]1*\[CapitalDelta]4 - 
             4*\[CapitalDelta]2*\[CapitalDelta]4) + 
           z^2*zb*(16 - 56*p + p^2 + 16*\[CapitalDelta] + 
             4*p*\[CapitalDelta] + 6*p*\[CapitalDelta]1 - 4*\[CapitalDelta]*
              \[CapitalDelta]1 - 4*\[CapitalDelta]1^2 - 
             4*e*(-24 + 2*l + p + 2*\[CapitalDelta] + 4*\[CapitalDelta]1 - 4*
                \[CapitalDelta]2) + 16*\[CapitalDelta]2 - 
             14*p*\[CapitalDelta]2 - 4*\[CapitalDelta]*\[CapitalDelta]2 + 
             4*\[CapitalDelta]2^2 - 16*\[CapitalDelta]3 + 
             2*p*\[CapitalDelta]3 + 4*\[CapitalDelta]*\[CapitalDelta]3 + 
             8*\[CapitalDelta]1*\[CapitalDelta]3 - 8*\[CapitalDelta]2*
              \[CapitalDelta]3 + 4*l*(4 + p - \[CapitalDelta]1 - 
               \[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             16*\[CapitalDelta]4 - 2*p*\[CapitalDelta]4 - 4*\[CapitalDelta]*
              \[CapitalDelta]4 - 8*\[CapitalDelta]1*\[CapitalDelta]4 + 
             8*\[CapitalDelta]2*\[CapitalDelta]4) - 2*z^2*zb^2*
            (-3*p^2 + 4*p*(-5 + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             4*e*(6 + p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
             4*(\[CapitalDelta]3^2 - 2*\[CapitalDelta]3*(1 + 
                 \[CapitalDelta]4) + \[CapitalDelta]4*(2 + 
                 \[CapitalDelta]4))))*H[{-1 + p, e}, 
            {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 1}][z, zb])/(8*(l + p)*(z - zb)^3*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (I*(-1 + zb)*zb^2*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (-2*l*z + p*z + 4*e*(-1 + z)*z + 4*z^2 - 3*p*z^2 + 2*l*zb + 
           3*p*zb - 4*z*zb - p*z*zb - 2*z*\[CapitalDelta] + 
           2*zb*\[CapitalDelta] - 2*z^2*\[CapitalDelta]3 + 
           2*z*zb*\[CapitalDelta]3 + 2*z^2*\[CapitalDelta]4 - 
           2*z*zb*\[CapitalDelta]4)*H[{-1 + p, e}, {-(1/2) + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 2}][z, zb])/(2*(l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (I*z*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (zb^2*(4*e + 2*l - p + 2*\[CapitalDelta])*(-4 + 2*l + 3*p + 
             2*\[CapitalDelta]) - 2*z*zb*(4*l^2 - 20*p - 3*p^2 - 
             8*\[CapitalDelta] + 4*p*\[CapitalDelta] + 4*\[CapitalDelta]^2 + 
             4*l*(-2 + p + 2*\[CapitalDelta]) + 4*e*(2 + 2*l + 3*p + 2*
                \[CapitalDelta])) + z^2*(2*l + 3*p + 2*\[CapitalDelta])*
            (-4 + 4*e + 2*l - p + 2*\[CapitalDelta] + 
             z*(8 - 4*e - 2*l + p - 2*\[CapitalDelta] - 2*\[CapitalDelta]1 + 
               2*\[CapitalDelta]2 + 2*\[CapitalDelta]3 - 2*
                \[CapitalDelta]4)) + zb^3*(4 + p - 2*\[CapitalDelta]1 + 
             2*\[CapitalDelta]2)*(-4 - 4*e + 3*p + 2*\[CapitalDelta]3 - 
             2*\[CapitalDelta]4) + z*zb^3*(4 + 4*e - 3*p - 
             2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
           z^3*zb*(4*e - 3*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (4 + p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) - 
           z*zb^2*(-32 + 4*l^2 + 56*p + 3*p^2 - 16*\[CapitalDelta] + 
             4*p*\[CapitalDelta] + 4*\[CapitalDelta]^2 + 
             16*\[CapitalDelta]1 - 6*p*\[CapitalDelta]1 + 4*\[CapitalDelta]*
              \[CapitalDelta]1 + 4*e*(-24 + 2*l + p + 2*\[CapitalDelta] + 4*
                \[CapitalDelta]1 - 4*\[CapitalDelta]2) - 
             16*\[CapitalDelta]2 + 6*p*\[CapitalDelta]2 - 4*\[CapitalDelta]*
              \[CapitalDelta]2 + 16*\[CapitalDelta]3 - 2*p*\[CapitalDelta]3 - 
             4*\[CapitalDelta]*\[CapitalDelta]3 - 8*\[CapitalDelta]1*
              \[CapitalDelta]3 + 8*\[CapitalDelta]2*\[CapitalDelta]3 - 
             16*\[CapitalDelta]4 + 2*p*\[CapitalDelta]4 + 4*\[CapitalDelta]*
              \[CapitalDelta]4 + 8*\[CapitalDelta]1*\[CapitalDelta]4 - 
             8*\[CapitalDelta]2*\[CapitalDelta]4 + 4*l*(-4 + p + 2*
                \[CapitalDelta] + \[CapitalDelta]1 - \[CapitalDelta]2 - 
               \[CapitalDelta]3 + \[CapitalDelta]4)) + 
           z^2*zb*(-16 + 8*l^2 - 40*p - 3*p^2 - 32*\[CapitalDelta] + 
             8*p*\[CapitalDelta] + 8*\[CapitalDelta]^2 + 8*\[CapitalDelta]1 + 
             6*p*\[CapitalDelta]1 + 8*\[CapitalDelta]*\[CapitalDelta]1 + 
             4*e*(-4 + 4*l + 5*p + 4*\[CapitalDelta] + 2*\[CapitalDelta]1 - 2*
                \[CapitalDelta]2) - 8*\[CapitalDelta]2 - 
             6*p*\[CapitalDelta]2 - 8*\[CapitalDelta]*\[CapitalDelta]2 + 
             8*\[CapitalDelta]3 - 10*p*\[CapitalDelta]3 - 8*\[CapitalDelta]*
              \[CapitalDelta]3 - 4*\[CapitalDelta]1*\[CapitalDelta]3 + 
             4*\[CapitalDelta]2*\[CapitalDelta]3 - 8*\[CapitalDelta]4 + 
             10*p*\[CapitalDelta]4 + 8*\[CapitalDelta]*\[CapitalDelta]4 + 
             4*\[CapitalDelta]1*\[CapitalDelta]4 - 4*\[CapitalDelta]2*
              \[CapitalDelta]4 + 8*l*(-4 + p + 2*\[CapitalDelta] + 
               \[CapitalDelta]1 - \[CapitalDelta]2 - \[CapitalDelta]3 + 
               \[CapitalDelta]4)) - 2*z^2*zb^2*(-3*p^2 + 
             4*p*(-5 + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             4*e*(6 + p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
             4*(\[CapitalDelta]3^2 - 2*\[CapitalDelta]3*(1 + 
                 \[CapitalDelta]4) + \[CapitalDelta]4*(2 + 
                 \[CapitalDelta]4))))*H[{-1 + p, e}, 
            {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 0}][z, zb])/(8*(l + p)*(z - zb)^3*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (I*z*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (-2*e*(-1 + z)*(-1 + zb)*(z + zb) + 2*p*(-1 + z)*(-1 + zb)*
            (z + zb) + (z - zb)^2*(-2 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4))*
          H[{-1 + p, e}, {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 1}][z, zb])/((l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
        (2*I*(-1 + z)*z*(-1 + zb)*zb^2*(-2 + 2*l + p + 2*\[CapitalDelta])*
          H[{-1 + p, e}, {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 2}][z, zb])/((l + p)*(z - zb)*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (I*(-1 + z)*z^2*(-2 + 2*l + p + 2*\[CapitalDelta])*
          (2*l*(z - zb) + p*((-z)*(-3 + zb) + zb - 3*zb^2) + 
           2*(2*e*(-1 + zb)*zb + (z - zb)*(\[CapitalDelta] + zb*
                (-2 + \[CapitalDelta]3 - \[CapitalDelta]4))))*
          H[{-1 + p, e}, {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 0}][z, zb])/(2*(l + p)*(z - zb)^2*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)) - 
        (2*I*(-1 + z)*z^2*(-1 + zb)*zb*(-2 + 2*l + p + 2*\[CapitalDelta])*
          H[{-1 + p, e}, {-(1/2) + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 1}][z, zb])/((l + p)*(z - zb)*
          (-4 + p + 2*\[CapitalDelta])*(-2 + l + p + \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-2 + l + p + 
           \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4))
