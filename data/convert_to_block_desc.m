(* ::Package:: *)

myDirectory=If[$InputFileName!="", DirectoryName@$InputFileName, NotebookDirectory[]];


invperm[p_]:=Module[{pi={0,0,0,0}},pi[[p[[1]]]]=1;pi[[p[[2]]]]=2;pi[[p[[3]]]]=3;pi[[p[[4]]]]=4;pi];


aperm[a_,b_,perm_]:=Module[{ks={0,2a,2b,0},ksnew},ksnew=ks[[invperm[perm]]];-(ksnew[[1]]-ksnew[[2]])/2];
bperm[a_,b_,perm_]:=Module[{ks={0,2a,2b,0},ksnew},ksnew=ks[[invperm[perm]]];(ksnew[[3]]-ksnew[[4]])/2];


screenLineBreaks[text_]:=StringReplace[text,"\n"->"\\\n"];


makeValueData[expression_, type_]:=Module[{seeds, coeffs, string},
seeds=Cases[expression,seedBlock[__][__],{0,Infinity}]//DeleteDuplicates;
If[Length@seeds == 0, Return[{}]];
coeffs=Coefficient[expression, #]&/@seeds;
If[Collect[seeds.coeffs - expression, seedBlock[__][__], Simplify] =!= 0, Print["Error: Coefficient function doesn't work on this expression!"]; Return[];];

Table[
{"coefficient"->ToString[coeffs[[i]],InputForm],
"type"->type,
"perm"->seeds[[i,0,1]],
"e"->ToString[seeds[[i,0,4]],InputForm],
"ap"->ToString[seeds[[i,0,2]],InputForm],
"bp"->ToString[seeds[[i,0,3]],InputForm],
"xt_derivs"->List@@(seeds[[i]])
}
,{i,Length@seeds}]
]


writeSeedBlockDescription[perm_, "primal", filename_, compact_:False]:=Module[{file, seedrecname, permString, seed, valueData, permNumber, primalData},
permString =  StringJoin@@(ToString/@perm);
seedrecname = "seed_recursion_formula_xt_"<>permString<>".m";
seed = Get[FileNameJoin[{myDirectory, seedrecname}]];
permNumber = Table[10^(4-n),{n,1,4}].perm;
valueData = makeValueData[Collect[I^(-1)seed[[2]], Htt[__][__],Simplify]/.{Htt[{_,E_},_,{m_,n_}][__]:>seedBlock[permNumber,ap,bp,E][m,n]},"primal"]; 

primalData={
   "description" -> {
   "Block description for primal seed permutation "<>permString<>".",
   "Recursion step also adds a factor i/(256(l+p)) and poles at",
   "   {((2+p)/2),((4+2 l+p)/2)} (for l>0)",
   "   {((2+p)/2)} (for l=0),",
   "The seed block in the rhs is evaluated at (dimension + 1/2)"},
   
   "dimension_var" -> "d",
   "spin_var" -> "l",
   "x_var" -> "x",
   "t_var" -> "t",
   "spurious_poles" -> {},
   "spurious_poles_L" -> { {"L"->0, "poles"->{"2+p/2"}} },
   "poles" -> {"(2+p)/2", "(4+2*l+p)/2" },
   "poles_L" -> { {"L"->0, "poles"->{"(2+p)/2"}} },
   "type" -> "primal",
   "exported_blocks" -> {"xtDeriv"},
   "xtDeriv" -> { "value" -> valueData }
   };

Export[FileNameJoin[{myDirectory,filename}],primalData,"JSON","Compact"->compact]

];


writeSeedBlockDescription[perm_, "dual", filename_, compact_:False]:=Module[{file, seedrecname, permString, seed, valueData, permNumber, dualData},
permString =  StringJoin@@(ToString/@perm);
seedrecname = "dual_seed_recursion_formula_xt_"<>permString<>".m";
seed = Get[FileNameJoin[{myDirectory, seedrecname}]];
permNumber = Table[10^(4-n),{n,1,4}].perm;
valueData = makeValueData[Collect[I^(-1)seed[[2]], Htt[__][__],Simplify]/.{Htt[{_,E_},_,{m_,n_}][__]:>seedBlock[permNumber,ap,bp,E][m,n]},"dual"]; 

dualData=
{
    "description"->{
        "Block description for dual seed permutation "<>permString<>".",
        "Recursion step also adds a factor i/(128(l+p)) and poles at",
        "   {((4-p)/2)} (for l>0)",
        "   {} (for l=0)",
        "The seed block in the rhs is evaluated at (dimension - 1/2);"
    },
  
    "dimension_var" -> "d",
    "spin_var" -> "l",
    "x_var" -> "x",
    "t_var" -> "t",
    "spurious_poles" -> (ToString[#,InputForm]&/@{2+2*aperm[ap,bp,perm]-l-0.5(3*p),2-2*bperm[ap,bp,perm]-l-0.5(3*p)}),
    "spurious_poles_L" -> { {"L"->0, "poles"->(ToString[#,InputForm]&/@{2+2*aperm[ap,bp,perm]-0.5(3*p),2-2*bperm[ap,bp,perm]-0.5(3*p),2-0.5p})} },
    "poles" -> {"(4-p)/2"},
    "poles_L" -> { {"L"->0, "poles"->{}} },
    "type" -> "dual",
    "exported_blocks" -> {"xtDeriv"},
    "xtDeriv" -> { "value" -> valueData }
};

(*Export[filename,dualData,"JSON"];*)
Export[FileNameJoin[{myDirectory,filename}],dualData,"JSON","Compact"->compact]

];


writeSeedBlockDescription[{1,2,3,4},"primal","seed_1234.txt",False];
writeSeedBlockDescription[{1,2,4,3},"primal","seed_1243.txt",False];


writeSeedBlockDescription[{1,2,3,4},"dual","dual_seed_1234.txt",False];
writeSeedBlockDescription[{1,2,4,3},"dual","dual_seed_1243.txt",False];
