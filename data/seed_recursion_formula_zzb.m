(I*(-1 + e - p)*z*zb*(p - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2)*
          (2 + l + p - \[CapitalDelta] + \[CapitalDelta]3 - \[CapitalDelta]4)*
          H[{-1 + p, -2 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 0}][z, zb])/
         ((l + p)*(2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 
           2*\[CapitalDelta])) + (1/((l + p)*(z - zb)^3*
           (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])))*
         (2*I*(-1 + e - p)*z*zb^2*(-2*l*(z - zb)^2*(-1 + zb) + 
           p*(-3 + z)*(z - zb)^2*(-1 + zb) - 
           2*(z^3*(-1 + zb)*(\[CapitalDelta]1 - \[CapitalDelta]2) + 
             zb^2*(-2 + \[CapitalDelta] - zb*(-4 + \[CapitalDelta] + 
                 \[CapitalDelta]1 - \[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4)) + z^2*(\[CapitalDelta] + zb^2*
                (6 - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2) - zb*
                (4 + \[CapitalDelta] - \[CapitalDelta]1 + \[CapitalDelta]2 - 
                 \[CapitalDelta]3 + \[CapitalDelta]4)) + 
             z*zb*(6 - 2*\[CapitalDelta] + zb^2*(-2 + \[CapitalDelta]1 - 
                 \[CapitalDelta]2) + zb*(-8 + 2*\[CapitalDelta] + 
                 \[CapitalDelta]1 - \[CapitalDelta]2 - 2*\[CapitalDelta]3 + 
                 2*\[CapitalDelta]4))))*H[{-1 + p, -2 + e}, 
            {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 1}][z, zb]) - 
        (8*I*(-1 + e - p)*(-1 + z)*z*(-1 + zb)*zb^4*
          H[{-1 + p, -2 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 2}][z, zb])/((l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) + 
        (1/((l + p)*(z - zb)^3*(2 + p - 2*\[CapitalDelta])*
           (4 + 2*l + p - 2*\[CapitalDelta])))*(2*I*(-1 + e - p)*z^2*zb*
          (zb^2*(-2*l + p*(-3 + zb) + 2*(\[CapitalDelta] - zb*
                \[CapitalDelta]1 + zb*\[CapitalDelta]2)) + 
           z^3*(2*l - p*(-3 + zb) - 2*(-4 + \[CapitalDelta] + 
               \[CapitalDelta]1 - \[CapitalDelta]2 + zb*(2 - 
                 \[CapitalDelta]1 + \[CapitalDelta]2) - \[CapitalDelta]3 + 
               \[CapitalDelta]4)) + z*zb*(2*l*(2 + zb) + p*(6 + zb - zb^2) + 
             2*(6 - 2*\[CapitalDelta] + zb^2*(\[CapitalDelta]1 - 
                 \[CapitalDelta]2) - zb*(4 + \[CapitalDelta] - 
                 \[CapitalDelta]1 + \[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4))) + z^2*(-2*l*(1 + 2*zb) + 
             p*(-3 - 5*zb + 2*zb^2) + 2*(-2 + \[CapitalDelta] + zb^2*
                (6 - 2*\[CapitalDelta]1 + 2*\[CapitalDelta]2) + zb*
                (-8 + 2*\[CapitalDelta] + \[CapitalDelta]1 - 
                 \[CapitalDelta]2 - 2*\[CapitalDelta]3 + 
                 2*\[CapitalDelta]4))))*H[{-1 + p, -2 + e}, 
            {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 0}][z, zb]) + 
        (8*I*(-1 + e - p)*(-1 + z)*z^2*(-1 + zb)*zb^2*(z + zb)*
          H[{-1 + p, -2 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 1}][z, zb])/((l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) - 
        (8*I*(-1 + e - p)*(-1 + z)*z^4*(-1 + zb)*zb*
          H[{-1 + p, -2 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 0}][z, zb])/((l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) + 
        (1/(16*(l + p)*(2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 
            2*\[CapitalDelta])))*(I*(8*l^3 + p^3*(-9 + 9*z + 9*zb - 3*z*zb) + 
           4*l^2*(12 + 4*e + 5*p - 6*\[CapitalDelta]) - 
           4*p*(-24 - 5*\[CapitalDelta]^2 + 12*z*\[CapitalDelta]1 + 
             12*zb*\[CapitalDelta]1 - 6*z*zb*\[CapitalDelta]1 - 
             12*z*\[CapitalDelta]2 - 12*zb*\[CapitalDelta]2 + 
             6*z*zb*\[CapitalDelta]2 - 4*z*\[CapitalDelta]3 - 
             4*zb*\[CapitalDelta]3 + 2*z*zb*\[CapitalDelta]3 + 
             3*z*\[CapitalDelta]1*\[CapitalDelta]3 + 3*zb*\[CapitalDelta]1*
              \[CapitalDelta]3 + 2*z*zb*\[CapitalDelta]1*\[CapitalDelta]3 - 
             3*z*\[CapitalDelta]2*\[CapitalDelta]3 - 3*zb*\[CapitalDelta]2*
              \[CapitalDelta]3 - 2*z*zb*\[CapitalDelta]2*\[CapitalDelta]3 - 
             z*zb*\[CapitalDelta]3^2 + 2*e*(6*(-2 + \[CapitalDelta]) - zb*
                (-4 + \[CapitalDelta] + 3*\[CapitalDelta]1 - 
                 3*\[CapitalDelta]2) + z*(4 - \[CapitalDelta] - 
                 3*\[CapitalDelta]1 + 3*\[CapitalDelta]2 + 
                 zb*(-2 + \[CapitalDelta]1 - \[CapitalDelta]2 + 
                   \[CapitalDelta]3 - \[CapitalDelta]4))) + 
             \[CapitalDelta]*(20 + z*(-3*\[CapitalDelta]1 + 
                 3*\[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4) + 
               zb*(-3*\[CapitalDelta]1 + 3*\[CapitalDelta]2 + 
                 \[CapitalDelta]3 - \[CapitalDelta]4)) + 
             4*z*\[CapitalDelta]4 + 4*zb*\[CapitalDelta]4 - 
             2*z*zb*\[CapitalDelta]4 - 3*z*\[CapitalDelta]1*
              \[CapitalDelta]4 - 3*zb*\[CapitalDelta]1*\[CapitalDelta]4 - 
             2*z*zb*\[CapitalDelta]1*\[CapitalDelta]4 + 3*z*\[CapitalDelta]2*
              \[CapitalDelta]4 + 3*zb*\[CapitalDelta]2*\[CapitalDelta]4 + 
             2*z*zb*\[CapitalDelta]2*\[CapitalDelta]4 + 
             2*z*zb*\[CapitalDelta]3*\[CapitalDelta]4 - 
             z*zb*\[CapitalDelta]4^2) - 8*(-2*(3 + e)*\[CapitalDelta]^2 + 
             \[CapitalDelta]^3 - (\[CapitalDelta]1 - \[CapitalDelta]2)*
              (4*zb + z*(4 + zb*(-2 + \[CapitalDelta]3 - \[CapitalDelta]4)))*
              (2*e - \[CapitalDelta]3 + \[CapitalDelta]4) + 
             \[CapitalDelta]*(8 + 2*e*(4 + zb*\[CapitalDelta]1 + 
                 z*(\[CapitalDelta]1 - \[CapitalDelta]2) - 
                 zb*\[CapitalDelta]2) - zb*\[CapitalDelta]1*
                \[CapitalDelta]3 + zb*\[CapitalDelta]2*\[CapitalDelta]3 - z*
                (\[CapitalDelta]1 - \[CapitalDelta]2)*(\[CapitalDelta]3 - 
                 \[CapitalDelta]4) + zb*\[CapitalDelta]1*\[CapitalDelta]4 - 
               zb*\[CapitalDelta]2*\[CapitalDelta]4)) - 
           2*l*(-3*p^2*(1 + z + zb) + 4*e*(p*(-6 + z + zb) + 2*
                (-4 + 2*\[CapitalDelta] - z*\[CapitalDelta]1 - 
                 zb*\[CapitalDelta]1 + z*\[CapitalDelta]2 + 
                 zb*\[CapitalDelta]2)) + 4*(-8 + 12*\[CapitalDelta] - 3*
                \[CapitalDelta]^2 + zb*\[CapitalDelta]1*\[CapitalDelta]3 - zb*
                \[CapitalDelta]2*\[CapitalDelta]3 + z*(\[CapitalDelta]1 - 
                 \[CapitalDelta]2)*(\[CapitalDelta]3 - \[CapitalDelta]4) - zb*
                \[CapitalDelta]1*\[CapitalDelta]4 + zb*\[CapitalDelta]2*
                \[CapitalDelta]4) + 2*p*(-20 + 10*\[CapitalDelta] + 3*zb*
                \[CapitalDelta]1 - 3*zb*\[CapitalDelta]2 - zb*
                \[CapitalDelta]3 + zb*\[CapitalDelta]4 + z*
                (3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4))) + 2*p^2*(2*e*(-3 + z)*(-3 + zb) + 
             z*(zb*(-6 + 3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 + 
                 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) - 3*
                (-4 + \[CapitalDelta] + 3*\[CapitalDelta]1 - 
                 3*\[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4)) - 
             3*(-2 + \[CapitalDelta] + zb*(-4 + \[CapitalDelta] + 
                 3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4))))*H[{-1 + p, -1 + e}, 
            {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 0}][z, zb]) + 
        (1/(4*(l + p)*(z - zb)^3*(2 + p - 2*\[CapitalDelta])*
           (4 + 2*l + p - 2*\[CapitalDelta])))*
         (I*zb*(4*l^2*z*(z - zb)^2*(-1 + zb) + p^2*(z - zb)^2*(-1 + zb)*
            (3*z^2 - 12*zb + z*(-3 + 4*zb)) + 4*l*(z - zb)^2*
            (z*(-1 + zb)*(2 + 2*e + p - 2*\[CapitalDelta]) + 
             zb*(2*e*(-1 + zb) - 2*p*(-1 + zb) + zb*(-2 + \[CapitalDelta]1 - 
                 \[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4))) - 
           2*p*(zb^3*(-6*e*(-1 + zb) + 4*(-4 + \[CapitalDelta]) + zb*
                (22 - 4*\[CapitalDelta] - 3*\[CapitalDelta]1 + 
                 3*\[CapitalDelta]2 + 3*\[CapitalDelta]3 - 
                 3*\[CapitalDelta]4)) + z^3*(6 - 2*e*(-3 + 2*zb + zb^2) - 2*
                \[CapitalDelta] + zb^2*(22 - 5*\[CapitalDelta]1 + 
                 5*\[CapitalDelta]2 + 5*\[CapitalDelta]3 - 
                 5*\[CapitalDelta]4) + 2*zb*(-7 + \[CapitalDelta] - 
                 \[CapitalDelta]1 + \[CapitalDelta]2 + \[CapitalDelta]3 - 
                 \[CapitalDelta]4)) + z^4*(-1 + zb)*(2*e + 3*
                \[CapitalDelta]1 - 3*\[CapitalDelta]2 - \[CapitalDelta]3 + 
               \[CapitalDelta]4) + z*zb^2*(38 + 2*e*(-3 + 2*zb + zb^2) - 10*
                \[CapitalDelta] + zb^2*(-10 + \[CapitalDelta]1 - 
                 \[CapitalDelta]2 + 3*\[CapitalDelta]3 - 
                 3*\[CapitalDelta]4) + 2*zb*(-13 + 5*\[CapitalDelta] - 
                 \[CapitalDelta]1 + \[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4)) + z^2*zb*(4 - 2*e*(3 - 4*zb + zb^2) + 8*
                \[CapitalDelta] - 2*zb*(23 + 4*\[CapitalDelta] - 
                 5*\[CapitalDelta]1 + 5*\[CapitalDelta]2 + 
                 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) + zb^2*
                (20 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                 7*\[CapitalDelta]3 + 7*\[CapitalDelta]4))) + 
           4*(z^4*(-1 + zb)*(\[CapitalDelta]1 - \[CapitalDelta]2)*
              (2*e - \[CapitalDelta]3 + \[CapitalDelta]4) - 
             zb^3*(-4 + \[CapitalDelta])*(2*e*(-1 + zb) + zb*
                (-2 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                 \[CapitalDelta]3 + \[CapitalDelta]4)) + 
             z*zb^2*((-(-2 + \[CapitalDelta]))*\[CapitalDelta] + 2*e*
                (8 - \[CapitalDelta] + zb^2*(-2 + \[CapitalDelta]3 - 
                   \[CapitalDelta]4) + zb*(-2 + \[CapitalDelta] - 
                   2*\[CapitalDelta]1 + 2*\[CapitalDelta]2 + 
                   \[CapitalDelta]3 - \[CapitalDelta]4)) + zb^2*
                (-2 + \[CapitalDelta]3 - \[CapitalDelta]4)*(-2 + 
                 \[CapitalDelta]1 - \[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4) + zb*(\[CapitalDelta]^2 + 
                 2*\[CapitalDelta]*(-3 + \[CapitalDelta]1 - 
                   \[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4) - 
                 6*(-2 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                   \[CapitalDelta]3 + \[CapitalDelta]4))) - 
             z^3*(\[CapitalDelta]*(-2 - 2*e + \[CapitalDelta]) + zb^2*
                (4 - 2*\[CapitalDelta]2 + 3*\[CapitalDelta]2*
                  \[CapitalDelta]3 + \[CapitalDelta]3^2 - 3*\[CapitalDelta]2*
                  \[CapitalDelta]4 - 2*\[CapitalDelta]3*\[CapitalDelta]4 + 
                 \[CapitalDelta]4^2 + 2*e*(-6 + 2*\[CapitalDelta]1 - 
                   2*\[CapitalDelta]2 - \[CapitalDelta]3 + 
                   \[CapitalDelta]4) + \[CapitalDelta]1*(2 - 
                   3*\[CapitalDelta]3 + 3*\[CapitalDelta]4)) + zb*
                (2*\[CapitalDelta] - \[CapitalDelta]^2 + 2*e*(2 + 
                   \[CapitalDelta] - \[CapitalDelta]3 + \[CapitalDelta]4) + 
                 2*(-2 + \[CapitalDelta]2 + \[CapitalDelta]3 - 
                   \[CapitalDelta]2*\[CapitalDelta]3 + \[CapitalDelta]1*
                    (-1 + \[CapitalDelta]3 - \[CapitalDelta]4) - 
                   \[CapitalDelta]4 + \[CapitalDelta]2*\[CapitalDelta]4))) + 
             z^2*zb*(2*(-4 - 2*\[CapitalDelta] + \[CapitalDelta]^2) + zb*
                (8 - 2*\[CapitalDelta]^2 - \[CapitalDelta]2*
                  \[CapitalDelta]3 + \[CapitalDelta]1*(\[CapitalDelta]3 - 
                   \[CapitalDelta]4) + \[CapitalDelta]*(6 - 
                   \[CapitalDelta]1 + \[CapitalDelta]2 + \[CapitalDelta]3 - 
                   \[CapitalDelta]4) + \[CapitalDelta]2*\[CapitalDelta]4) + 2*
                e*(4 - \[CapitalDelta] + zb*(-16 + \[CapitalDelta] + 
                   3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 - 
                   2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
                 zb^2*(4 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                   2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)) + zb^2*
                (\[CapitalDelta]2*(-4 + 3*\[CapitalDelta]3 - 
                   3*\[CapitalDelta]4) + \[CapitalDelta]1*(4 - 
                   3*\[CapitalDelta]3 + 3*\[CapitalDelta]4) + 
                 2*(-4 + \[CapitalDelta]3^2 - 2*\[CapitalDelta]3*
                    \[CapitalDelta]4 + \[CapitalDelta]4^2)))))*
          H[{-1 + p, -1 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 1}][z, zb]) - 
        (I*(-1 + zb)*zb^3*((-zb)*(8 + 2*l + 3*p - 2*\[CapitalDelta]) + 
           z^2*(-8*e + 7*p + 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) + 
           z*(4 + 8*e + 2*l - 5*p + 4*zb + p*zb - 2*\[CapitalDelta] - 
             2*zb*\[CapitalDelta]3 + 2*zb*\[CapitalDelta]4))*
          H[{-1 + p, -1 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 2}][z, zb])/((l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) - 
        (1/(4*(l + p)*(z - zb)^3*(2 + p - 2*\[CapitalDelta])*
           (4 + 2*l + p - 2*\[CapitalDelta])))*
         (I*z*(p^2*(z - zb)^2*(4*z^2*(-3 + zb) - 3*(-1 + zb)*zb + 
             z*(12 - 7*zb + 3*zb^2)) - 2*p*(z^2*zb*(38 + 2*l*(5 + 4*zb) - 10*
                \[CapitalDelta] + zb^2*(22 - 5*\[CapitalDelta]1 + 
                 5*\[CapitalDelta]2 + 5*\[CapitalDelta]3 - 
                 5*\[CapitalDelta]4) - 2*zb*(23 + 4*\[CapitalDelta] - 
                 5*\[CapitalDelta]1 + 5*\[CapitalDelta]2 + 
                 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4)) + 
             z^4*(22 + 4*l - 4*\[CapitalDelta] - 3*\[CapitalDelta]1 + 3*
                \[CapitalDelta]2 + 3*\[CapitalDelta]3 + zb*(-10 + 
                 \[CapitalDelta]1 - \[CapitalDelta]2 + 3*\[CapitalDelta]3 - 
                 3*\[CapitalDelta]4) - 3*\[CapitalDelta]4) + 
             zb^3*(6 + 2*l - 2*\[CapitalDelta] - 3*zb*\[CapitalDelta]1 + 3*zb*
                \[CapitalDelta]2 + zb*\[CapitalDelta]3 - zb*
                \[CapitalDelta]4) + z*zb^2*(4 - 2*l*(4 + zb) + 8*
                \[CapitalDelta] + 2*zb*(-7 + \[CapitalDelta] - 
                 \[CapitalDelta]1 + \[CapitalDelta]2 + \[CapitalDelta]3 - 
                 \[CapitalDelta]4) + zb^2*(3*\[CapitalDelta]1 - 
                 3*\[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4)) + 
             z^3*(-2*l*(2 + 5*zb) + 4*(-4 + \[CapitalDelta]) + 2*zb*
                (-13 + 5*\[CapitalDelta] - \[CapitalDelta]1 + 
                 \[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4) + 
               zb^2*(20 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                 7*\[CapitalDelta]3 + 7*\[CapitalDelta]4))) + 
           4*e*(zb^3*(-2*l + p*(-3 + zb) + 2*(\[CapitalDelta] - 
                 zb*\[CapitalDelta]1 + zb*\[CapitalDelta]2)) + 
             z^4*(8 + 2*l - p*(-3 + zb) - 4*zb - 2*\[CapitalDelta] + 2*zb*
                \[CapitalDelta]3 - 2*zb*\[CapitalDelta]4) + 
             z*zb^2*(2*l*(1 + zb) + p*(3 + 2*zb - zb^2) + 2*
                (4 - \[CapitalDelta] + zb^2*(\[CapitalDelta]1 - 
                   \[CapitalDelta]2) - zb*(2 + \[CapitalDelta] - 
                   \[CapitalDelta]3 + \[CapitalDelta]4))) + 
             z^2*zb*(-2*l*(-1 + zb) + p*(3 - 4*zb + zb^2) - 2*
                (-8 + \[CapitalDelta] + zb^2*(-6 + 2*\[CapitalDelta]1 - 
                   2*\[CapitalDelta]2 - \[CapitalDelta]3 + 
                   \[CapitalDelta]4) - zb*(-16 + \[CapitalDelta] + 
                   3*\[CapitalDelta]1 - 3*\[CapitalDelta]2 - 
                   2*\[CapitalDelta]3 + 2*\[CapitalDelta]4))) + 
             z^3*(-2*l*(1 + zb) + p*(-3 - 2*zb + zb^2) + 2*(-4 + 
                 \[CapitalDelta] + zb*(-2 + \[CapitalDelta] - 
                   2*\[CapitalDelta]1 + 2*\[CapitalDelta]2 + 
                   \[CapitalDelta]3 - \[CapitalDelta]4) + zb^2*
                  (4 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                   2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)))) + 
           4*(zb^3*(-l^2 + 2*l*(-1 + \[CapitalDelta]) + 2*\[CapitalDelta] - 
               \[CapitalDelta]^2 + zb*(\[CapitalDelta]1 - \[CapitalDelta]2)*
                (\[CapitalDelta]3 - \[CapitalDelta]4)) + 
             z^4*(4 + l - \[CapitalDelta] + zb*(-2 + \[CapitalDelta]3 - 
                 \[CapitalDelta]4))*(-2 + \[CapitalDelta]1 - 
               \[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4) - 
             z^2*zb*(l^2*(1 + 2*zb) + (-2 + \[CapitalDelta])*
                \[CapitalDelta] + l*(2 - 2*\[CapitalDelta] + 
                 zb*(6 - 4*\[CapitalDelta] - \[CapitalDelta]1 + 
                   \[CapitalDelta]2 + \[CapitalDelta]3 - \[CapitalDelta]4)) + 
               zb*(-8 + 2*\[CapitalDelta]^2 + \[CapitalDelta]2*
                  \[CapitalDelta]3 - \[CapitalDelta]2*\[CapitalDelta]4 + 
                 \[CapitalDelta]1*(-\[CapitalDelta]3 + \[CapitalDelta]4) + 
                 \[CapitalDelta]*(-6 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                   \[CapitalDelta]3 + \[CapitalDelta]4)) + zb^2*
                (4 + \[CapitalDelta]3^2 + \[CapitalDelta]2*(-2 + 
                   3*\[CapitalDelta]3 - 3*\[CapitalDelta]4) - 
                 2*\[CapitalDelta]3*\[CapitalDelta]4 + \[CapitalDelta]4^2 + 
                 \[CapitalDelta]1*(2 - 3*\[CapitalDelta]3 + 
                   3*\[CapitalDelta]4))) + z^3*zb*(12 + l^2 - 6*
                \[CapitalDelta] + \[CapitalDelta]^2 - 6*\[CapitalDelta]1 + 2*
                \[CapitalDelta]*\[CapitalDelta]1 + 6*\[CapitalDelta]2 - 2*
                \[CapitalDelta]*\[CapitalDelta]2 + 6*\[CapitalDelta]3 - 2*
                \[CapitalDelta]*\[CapitalDelta]3 - 6*\[CapitalDelta]4 + 2*
                \[CapitalDelta]*\[CapitalDelta]4 - 2*l*(-3 + 
                 \[CapitalDelta] + \[CapitalDelta]1 - \[CapitalDelta]2 - 
                 \[CapitalDelta]3 + \[CapitalDelta]4) + zb*(\[CapitalDelta]2*
                  (-4 + 3*\[CapitalDelta]3 - 3*\[CapitalDelta]4) + 
                 \[CapitalDelta]1*(4 - 3*\[CapitalDelta]3 + 
                   3*\[CapitalDelta]4) + 2*(-4 + \[CapitalDelta]3^2 - 
                   2*\[CapitalDelta]3*\[CapitalDelta]4 + \[CapitalDelta]4^
                    2))) + z*zb^2*(l^2*(2 + zb) - 2*l*(2 + zb)*
                (-1 + \[CapitalDelta]) + 2*(-4 - 2*\[CapitalDelta] + 
                 \[CapitalDelta]^2) - zb^2*(\[CapitalDelta]1 - 
                 \[CapitalDelta]2)*(\[CapitalDelta]3 - \[CapitalDelta]4) + zb*
                (-2*\[CapitalDelta] + \[CapitalDelta]^2 + 
                 2*(2 - \[CapitalDelta]3 + \[CapitalDelta]2*(-1 + 
                     \[CapitalDelta]3 - \[CapitalDelta]4) + 
                   \[CapitalDelta]4 + \[CapitalDelta]1*(1 - 
                     \[CapitalDelta]3 + \[CapitalDelta]4))))))*
          H[{-1 + p, -1 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 0}][z, zb]) - 
        (1/((l + p)*(z - zb)^2*(2 + p - 2*\[CapitalDelta])*
           (4 + 2*l + p - 2*\[CapitalDelta])))*(2*I*z*zb*(2*z^3 - 4*z*zb + 
           2*z^2*zb - 2*z^3*zb + 2*z*zb^2 + 2*zb^3 - 2*z*zb^3 + 
           2*e*(-1 + z)*(-1 + zb)*(z + zb)^2 - 2*p*(-1 + z)*(-1 + zb)*
            (z + zb)^2 - z^3*\[CapitalDelta]1 + z^2*zb*\[CapitalDelta]1 + 
           z^3*zb*\[CapitalDelta]1 + z*zb^2*\[CapitalDelta]1 - 
           2*z^2*zb^2*\[CapitalDelta]1 - zb^3*\[CapitalDelta]1 + 
           z*zb^3*\[CapitalDelta]1 + z^3*\[CapitalDelta]2 - 
           z^2*zb*\[CapitalDelta]2 - z^3*zb*\[CapitalDelta]2 - 
           z*zb^2*\[CapitalDelta]2 + 2*z^2*zb^2*\[CapitalDelta]2 + 
           zb^3*\[CapitalDelta]2 - z*zb^3*\[CapitalDelta]2 + 
           z^3*\[CapitalDelta]3 - z^2*zb*\[CapitalDelta]3 - 
           z^3*zb*\[CapitalDelta]3 - z*zb^2*\[CapitalDelta]3 + 
           2*z^2*zb^2*\[CapitalDelta]3 + zb^3*\[CapitalDelta]3 - 
           z*zb^3*\[CapitalDelta]3 - z^3*\[CapitalDelta]4 + 
           z^2*zb*\[CapitalDelta]4 + z^3*zb*\[CapitalDelta]4 + 
           z*zb^2*\[CapitalDelta]4 - 2*z^2*zb^2*\[CapitalDelta]4 - 
           zb^3*\[CapitalDelta]4 + z*zb^3*\[CapitalDelta]4)*
          H[{-1 + p, -1 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 1}][z, zb]) - 
        (4*I*(-1 + z)*z*(-1 + zb)*zb^3*H[{-1 + p, -1 + e}, 
            {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 2}][z, zb])/((l + p)*(z - zb)*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) + 
        (I*(-1 + z)*z^3*(z*(8 + 2*l + 3*p - 4*zb - p*zb - 2*\[CapitalDelta] + 
             2*zb*\[CapitalDelta]3 - 2*zb*\[CapitalDelta]4) + 
           zb*(-4 - 2*l + 5*p + 8*e*(-1 + zb) - 7*p*zb + 2*\[CapitalDelta] - 
             2*zb*\[CapitalDelta]3 + 2*zb*\[CapitalDelta]4))*
          H[{-1 + p, -1 + e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 0}][z, zb])/((l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) + 
        (4*I*(-1 + z)*z^3*(-1 + zb)*zb*H[{-1 + p, -1 + e}, 
            {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 1}][z, zb])/((l + p)*(z - zb)*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) + 
        (I*(8 + 2*l + 3*p - 2*\[CapitalDelta])*(l + p - \[CapitalDelta] + 
           \[CapitalDelta]1 - \[CapitalDelta]2)*(-4 - 4*e + 3*p + 
           2*\[CapitalDelta]3 - 2*\[CapitalDelta]4)*
          H[{-1 + p, e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 0}][z, zb])/
         (16*(l + p)*(2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 
           2*\[CapitalDelta])) + (1/(8*(l + p)*(z - zb)^3*
           (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])))*
         (I*zb*((-zb^2)*(8 + 2*l + 3*p - 2*\[CapitalDelta])*
            (-4 + p + 4*e*(-1 + zb) + 2*l*(-1 + zb) - p*zb + 
             2*\[CapitalDelta] - 2*zb*\[CapitalDelta] + 
             2*zb*\[CapitalDelta]1 - 2*zb*\[CapitalDelta]2 - 
             2*zb*\[CapitalDelta]3 + 2*zb*\[CapitalDelta]4) + 
           z^3*(4 + 4*e - 3*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (p*(-1 + zb) - 2*(2 - \[CapitalDelta]1 + \[CapitalDelta]2 + zb*
                \[CapitalDelta]3 - zb*\[CapitalDelta]4)) + 
           z^2*(32 + 20*p - 3*p^2 - 4*l^2*(-1 + zb) + 32*zb - 72*p*zb - 
             3*p^2*zb + 40*p*zb^2 + 6*p^2*zb^2 - 24*\[CapitalDelta] - 
             4*p*\[CapitalDelta] + 16*zb*\[CapitalDelta] + 
             4*p*zb*\[CapitalDelta] + 4*\[CapitalDelta]^2 - 
             4*zb*\[CapitalDelta]^2 - 32*zb*\[CapitalDelta]1 + 
             6*p*zb*\[CapitalDelta]1 + 4*zb*\[CapitalDelta]*
              \[CapitalDelta]1 + 32*zb*\[CapitalDelta]2 - 
             6*p*zb*\[CapitalDelta]2 - 4*zb*\[CapitalDelta]*
              \[CapitalDelta]2 + 2*p*zb*\[CapitalDelta]3 + 
             16*zb^2*\[CapitalDelta]3 - 8*p*zb^2*\[CapitalDelta]3 - 
             4*zb*\[CapitalDelta]*\[CapitalDelta]3 + 8*zb*\[CapitalDelta]1*
              \[CapitalDelta]3 - 8*zb*\[CapitalDelta]2*\[CapitalDelta]3 - 
             8*zb^2*\[CapitalDelta]3^2 - 4*e*(2*l*(-1 + zb) + p*(-3 + zb + 
                 2*zb^2) - 2*(2 - \[CapitalDelta] + zb*(8 + \[CapitalDelta] - 
                   2*\[CapitalDelta]1 + 2*\[CapitalDelta]2) + 2*zb^2*
                  (-3 + \[CapitalDelta]3 - \[CapitalDelta]4))) - 
             2*p*zb*\[CapitalDelta]4 - 16*zb^2*\[CapitalDelta]4 + 
             8*p*zb^2*\[CapitalDelta]4 + 4*zb*\[CapitalDelta]*
              \[CapitalDelta]4 - 8*zb*\[CapitalDelta]1*\[CapitalDelta]4 + 
             8*zb*\[CapitalDelta]2*\[CapitalDelta]4 + 
             16*zb^2*\[CapitalDelta]3*\[CapitalDelta]4 - 
             8*zb^2*\[CapitalDelta]4^2 - 4*l*(p*(-1 + zb) + 2*
                (-3 + \[CapitalDelta]) + zb*(4 - 2*\[CapitalDelta] + 
                 \[CapitalDelta]1 - \[CapitalDelta]2 - \[CapitalDelta]3 + 
                 \[CapitalDelta]4))) + z*zb*(-64 + 8*p + 6*p^2 + 
             8*l^2*(-1 + zb) - 16*zb - 8*p*zb - 3*p^2*zb - 12*p*zb^2 - 
             3*p^2*zb^2 + 48*\[CapitalDelta] + 8*p*\[CapitalDelta] - 
             32*zb*\[CapitalDelta] - 8*p*zb*\[CapitalDelta] - 
             8*\[CapitalDelta]^2 + 8*zb*\[CapitalDelta]^2 + 
             40*zb*\[CapitalDelta]1 + 6*p*zb*\[CapitalDelta]1 - 
             8*zb*\[CapitalDelta]*\[CapitalDelta]1 - 40*zb*\[CapitalDelta]2 - 
             6*p*zb*\[CapitalDelta]2 + 8*zb*\[CapitalDelta]*
              \[CapitalDelta]2 - 24*zb*\[CapitalDelta]3 - 
             10*p*zb*\[CapitalDelta]3 - 8*zb^2*\[CapitalDelta]3 + 
             4*p*zb^2*\[CapitalDelta]3 + 8*zb*\[CapitalDelta]*
              \[CapitalDelta]3 - 4*zb*\[CapitalDelta]1*\[CapitalDelta]3 + 
             4*zb*\[CapitalDelta]2*\[CapitalDelta]3 + 
             4*zb^2*\[CapitalDelta]3^2 + 24*zb*\[CapitalDelta]4 + 
             10*p*zb*\[CapitalDelta]4 + 8*zb^2*\[CapitalDelta]4 - 
             4*p*zb^2*\[CapitalDelta]4 - 8*zb*\[CapitalDelta]*
              \[CapitalDelta]4 + 4*zb*\[CapitalDelta]1*\[CapitalDelta]4 - 
             4*zb*\[CapitalDelta]2*\[CapitalDelta]4 - 8*zb^2*\[CapitalDelta]3*
              \[CapitalDelta]4 + 4*zb^2*\[CapitalDelta]4^2 + 
             8*l*(p*(-1 + zb) + 2*(-3 + \[CapitalDelta]) + zb*
                (4 - 2*\[CapitalDelta] + \[CapitalDelta]1 - 
                 \[CapitalDelta]2 - \[CapitalDelta]3 + \[CapitalDelta]4)) + 
             4*e*(4*l*(-1 + zb) + p*(-6 + 5*zb + zb^2) + 2*
                (2*(-5 + \[CapitalDelta]) + zb*(6 - 2*\[CapitalDelta] + 
                   \[CapitalDelta]1 - \[CapitalDelta]2) + zb^2*
                  (2 - \[CapitalDelta]3 + \[CapitalDelta]4)))))*
          H[{-1 + p, e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 1}][z, zb]) + 
        (I*(-1 + zb)*zb^2*((-zb)*(8 + 2*l + 3*p - 2*\[CapitalDelta]) + 
           z^2*(-4 - 4*e + 3*p + 2*\[CapitalDelta]3 - 2*\[CapitalDelta]4) + 
           z*(8 + 4*e + 2*l - p + 4*zb + p*zb - 2*\[CapitalDelta] - 
             2*zb*\[CapitalDelta]3 + 2*zb*\[CapitalDelta]4))*
          H[{-1 + p, e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {0, 2}][z, zb])/(2*(l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) - 
        (1/(8*(l + p)*(z - zb)^3*(2 + p - 2*\[CapitalDelta])*
           (4 + 2*l + p - 2*\[CapitalDelta])))*
         (I*z*(zb^2*(8 + 4*e + 2*l - p - 2*\[CapitalDelta])*
            (4 + 2*l + 3*p - 2*\[CapitalDelta]) - 2*z*zb*(32 + 4*l^2 - 4*p - 
             3*p^2 + 4*l*(6 + p - 2*\[CapitalDelta]) + 
             4*e*(10 + 2*l + 3*p - 2*\[CapitalDelta]) - 24*\[CapitalDelta] - 
             4*p*\[CapitalDelta] + 4*\[CapitalDelta]^2) + 
           z^2*(8 + 2*l + 3*p - 2*\[CapitalDelta])*(4 + 4*e + 2*l - p - 
             2*\[CapitalDelta] + z*(-4*e - 2*l + p + 2*\[CapitalDelta] - 2*
                \[CapitalDelta]1 + 2*\[CapitalDelta]2 + 2*\[CapitalDelta]3 - 
               2*\[CapitalDelta]4)) + zb^3*(4 + p - 2*\[CapitalDelta]1 + 
             2*\[CapitalDelta]2)*(-4 - 4*e + 3*p + 2*\[CapitalDelta]3 - 
             2*\[CapitalDelta]4) + z*zb^3*(4 + 4*e - 3*p - 
             2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
           z^3*zb*(4*e - 3*p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4)*
            (4 + p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) - 
           z*zb^2*(-32 + 4*l^2 + 72*p + 3*p^2 - 16*\[CapitalDelta] - 
             4*p*\[CapitalDelta] + 4*\[CapitalDelta]^2 + 
             32*\[CapitalDelta]1 - 6*p*\[CapitalDelta]1 - 4*\[CapitalDelta]*
              \[CapitalDelta]1 - 32*\[CapitalDelta]2 + 6*p*\[CapitalDelta]2 + 
             4*\[CapitalDelta]*\[CapitalDelta]2 + 4*e*(2*l + p - 2*
                (8 + \[CapitalDelta] - 2*\[CapitalDelta]1 + 
                 2*\[CapitalDelta]2)) - 2*p*\[CapitalDelta]3 + 
             4*\[CapitalDelta]*\[CapitalDelta]3 - 8*\[CapitalDelta]1*
              \[CapitalDelta]3 + 8*\[CapitalDelta]2*\[CapitalDelta]3 + 
             2*p*\[CapitalDelta]4 - 4*\[CapitalDelta]*\[CapitalDelta]4 + 
             8*\[CapitalDelta]1*\[CapitalDelta]4 - 8*\[CapitalDelta]2*
              \[CapitalDelta]4 + 4*l*(4 + p - 2*\[CapitalDelta] + 
               \[CapitalDelta]1 - \[CapitalDelta]2 - \[CapitalDelta]3 + 
               \[CapitalDelta]4)) + z^2*zb*(-16 + 8*l^2 - 8*p - 3*p^2 - 
             32*\[CapitalDelta] - 8*p*\[CapitalDelta] + 8*\[CapitalDelta]^2 + 
             40*\[CapitalDelta]1 + 6*p*\[CapitalDelta]1 - 8*\[CapitalDelta]*
              \[CapitalDelta]1 + 4*e*(12 + 4*l + 5*p - 4*\[CapitalDelta] + 2*
                \[CapitalDelta]1 - 2*\[CapitalDelta]2) - 
             40*\[CapitalDelta]2 - 6*p*\[CapitalDelta]2 + 8*\[CapitalDelta]*
              \[CapitalDelta]2 - 24*\[CapitalDelta]3 - 
             10*p*\[CapitalDelta]3 + 8*\[CapitalDelta]*\[CapitalDelta]3 - 
             4*\[CapitalDelta]1*\[CapitalDelta]3 + 4*\[CapitalDelta]2*
              \[CapitalDelta]3 + 24*\[CapitalDelta]4 + 
             10*p*\[CapitalDelta]4 - 8*\[CapitalDelta]*\[CapitalDelta]4 + 
             4*\[CapitalDelta]1*\[CapitalDelta]4 - 4*\[CapitalDelta]2*
              \[CapitalDelta]4 + 8*l*(4 + p - 2*\[CapitalDelta] + 
               \[CapitalDelta]1 - \[CapitalDelta]2 - \[CapitalDelta]3 + 
               \[CapitalDelta]4)) - 2*z^2*zb^2*(-3*p^2 + 
             4*p*(-5 + \[CapitalDelta]3 - \[CapitalDelta]4) + 
             4*e*(6 + p - 2*\[CapitalDelta]3 + 2*\[CapitalDelta]4) + 
             4*(\[CapitalDelta]3^2 - 2*\[CapitalDelta]3*(1 + 
                 \[CapitalDelta]4) + \[CapitalDelta]4*(2 + 
                 \[CapitalDelta]4))))*H[{-1 + p, e}, {1/2 + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 0}][z, zb]) + 
        (I*z*zb*(2*e*(-1 + z)*(-1 + zb)*(z + zb) - 2*p*(-1 + z)*(-1 + zb)*
            (z + zb) - (z - zb)^2*(-2 + \[CapitalDelta]1 - \[CapitalDelta]2 - 
             \[CapitalDelta]3 + \[CapitalDelta]4))*
          H[{-1 + p, e}, {1/2 + \[CapitalDelta], l}, {\[CapitalDelta]1, 
             1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 1}][z, zb])/((l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) + 
        (2*I*(-1 + z)*z*(-1 + zb)*zb^2*H[{-1 + p, e}, {1/2 + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {1, 2}][z, zb])/((l + p)*(z - zb)*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) - 
        (I*(-1 + z)*z^2*(z*(8 + 2*l + 3*p - 4*zb - p*zb - 2*\[CapitalDelta] + 
             2*zb*\[CapitalDelta]3 - 2*zb*\[CapitalDelta]4) + 
           zb*(-8 - 2*l + p + 4*e*(-1 + zb) + 4*zb - 3*p*zb + 
             2*\[CapitalDelta] - 2*zb*\[CapitalDelta]3 + 
             2*zb*\[CapitalDelta]4))*H[{-1 + p, e}, {1/2 + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 0}][z, zb])/(2*(l + p)*(z - zb)^2*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta])) - 
        (2*I*(-1 + z)*z^2*(-1 + zb)*zb*H[{-1 + p, e}, {1/2 + \[CapitalDelta], 
             l}, {\[CapitalDelta]1, 1/2 + \[CapitalDelta]2, \[CapitalDelta]3, 
             1/2 + \[CapitalDelta]4}, {2, 1}][z, zb])/((l + p)*(z - zb)*
          (2 + p - 2*\[CapitalDelta])*(4 + 2*l + p - 2*\[CapitalDelta]))
