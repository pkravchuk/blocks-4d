(* ::Package:: *)

prec=200;
small=SetPrecision[10^(-prec-1),prec];

lowPrec=8;

lambdaRecursionPrecompute=40;
lambdaStep=4;

\[Rho]Crossing=SetPrecision[3-2Sqrt[2],prec];
rCrossing=SetPrecision[3-2Sqrt[2],prec];
zCrossing=SetPrecision[1/2,prec];


dimpatt=d_;
dimval=d;


unitarityBound[p_,lval_]:=If[lval==0,1+p/2,2+lval+p/2];


writeLog[str__]:=WriteString["stdout",str];


formatNumber[0] := StringTake["0.00000000000000000000000",10];
formatNumber[x_]:=Module[{str,ln},
	If[x<0,ln=11,ln=10];
	str=ToString[SetPrecision[x,prec]];
	StringTake[str,ln]
];


safeImport[filename_]:=If[FileExistsQ[filename],Import[filename],writeLog["safeImport: File "<>filename<>" not found!"]; Pause[5];Quit[];];


safeGet[filename_]:=If[FileExistsQ[filename],Get[filename],writeLog["safeGet: File "<>filename<>" not found!"]; Quit[];];


safeExport[filename_,args__]:=Module[{dirname},
	dirname=DirectoryName[filename];
	If[dirname!=""&&!DirectoryQ[dirname],
		CreateDirectory[dirname,CreateIntermediateDirectories->True];
	];
	Export[filename,args];
];


highPrecision[var_]:=If[Precision[var]<prec,SetPrecision[var,prec],var];
lowPrecision[var_]:=SetPrecision[var,lowPrec];


seedRecursionDir=FileNameJoin[{DirectoryName@$InputFileName,"tables","recursion_tables"}];
scalarBlockDir=FileNameJoin[{DirectoryName@$InputFileName,"tables","scalar_tables"}];
seedBlockDir=FileNameJoin[{DirectoryName@$InputFileName,"tables","seed_tables"}];

dataDir=DirectoryName@$InputFileName;

cbDir=FileNameJoin[{DirectoryName@$InputFileName,"tables","cb_tables"}];


seedPrecomputedRecursionFileName[type_,lambda_]:=FileNameJoin[{seedRecursionDir,type<>"-precomputed-lambda-"<>ToString[lambda]<>".m"}];


maximalPrecomputedLambda[type_]:=Module[{tables},
	tables=FileNameSplit[#][[-1]]&/@FileNames[type<>"-precomputed-lambda-*.m",seedRecursionDir];
	tables=ToExpression/@StringReplace[tables,__~~"-precomputed-lambda-"~~l__~~".m":>l];
	Max[tables]
]


seedRecursionTableFileName[pval_,lambda_,perm_]:=FileNameJoin[{seedRecursionDir,"seed-"<>StringJoin[ToString/@perm]<>"-p"<>ToString[pval]<>"-lambda"<>ToString[lambda]<>".m"}];
dualSeedRecursionTableFileName[pval_,lambda_,perm_]:=FileNameJoin[{seedRecursionDir,"dualseed-"<>StringJoin[ToString/@perm]<>"-p"<>ToString[pval]<>"-lambda"<>ToString[lambda]<>".m"}];


seedRecursionTableFileName["primal",pval_,lambda_,perm_]:=seedRecursionTableFileName[pval,lambda,perm];
seedRecursionTableFileName["dual",pval_,lambda_,perm_]:=dualSeedRecursionTableFileName[pval,lambda,perm];


scalarBlockTableFileName[lambda_,keptPoleOrder_,l_,a_,b_]:=FileNameJoin[{
	scalarBlockDir,
	"scalarBlockTable4D-lambda-"<>ToString[lambda]<>
	"-keptPoleOrder-"<>ToString[keptPoleOrder]<>
	"-L"<>ToString[l]<>
	"-a-"<>(*ToString@lowPrecision*)formatNumber[a]<>
	"-b-"<>(*ToString@lowPrecision*)formatNumber[b]<>
	".m"
}];

scalarBlockTableFileName[lambda_,keptPoleOrder_,order_,l_,a_,b_]:=FileNameJoin[{
	scalarBlockDir,
	"scalarBlockTable4D-lambda-"<>ToString[lambda]<>
	"-keptPoleOrder-"<>ToString[keptPoleOrder]<>
	"-order-"<>ToString[order]<>
	"-L"<>ToString[l]<>
	"-a-"<>(*ToString@lowPrecision*)formatNumber[a]<>
	"-b-"<>(*ToString@lowPrecision*)formatNumber[b]<>
	".m"
}];


seedBlockTableFileName[type_,perm_,p_,l_,a_,b_,lambda_,keptPoleOrder_]:=FileNameJoin[{
	seedBlockDir,
	"seedBlockTable4D"<>
	"-type-"<>type<>
	"-perm-"<>StringJoin[ToString/@perm]<>
	"-P"<>ToString[p]<>
	"-L"<>ToString[l]<>
	"-a-"<>(*ToString@lowPrecision*)formatNumber[a]<>
	"-b-"<>(*ToString@lowPrecision*)formatNumber[b]<>
	"-lambda-"<>ToString[lambda]<>
	"-keptPoleOrder-"<>ToString[keptPoleOrder]<>
	".m"
}];

seedBlockTableFileName[type_,perm_,p_,l_,a_,b_,lambda_,keptPoleOrder_,order_]:=FileNameJoin[{
	seedBlockDir,
	"seedBlockTable4D"<>
	"-type-"<>type<>
	"-perm-"<>StringJoin[ToString/@perm]<>
	"-P"<>ToString[p]<>
	"-L"<>ToString[l]<>
	"-a-"<>(*ToString@lowPrecision*)formatNumber[a]<>
	"-b-"<>(*ToString@lowPrecision*)formatNumber[b]<>
	"-lambda-"<>ToString[lambda]<>
	"-keptPoleOrder-"<>ToString[keptPoleOrder]<>
	"-order-"<>ToString[order]<>
	".m"
}];


safeGet[FileNameJoin[{DirectoryName@$InputFileName,"cb_naming.m"}]];


(* fileWithFunction[filename_,fn_,args_]; -- wrapper for resource files *)
(* fileWithMinimalValue[min_,searchfn_,genfn_,args_] *)


checkAndCreateFile[fileWithFunction[filename_,fn_,args_]]:=If[!FileExistsQ[filename],
	fn@@args;
	If[!FileExistsQ[filename],
		writeLog["checkAndCreateFile: Failed to create "<>filename<>" using function "<>ToString[fn]<>" !"];
		Pause[1];
		Quit[];
	];
];


checkAndCreateFile[fileWithMinimalValue[min_,searchfn_,filename_,genfn_,args_]]:=If[searchfn<min,
	genfn@@args;
	If[!FileExistsQ[filename],
		writeLog["checkAndCreateFile: Failed to create "<>filename<>" using function "<>ToString[fn]<>" !"];
		Exit[];
	];
];


zzbPrefactorTable[\[CapitalLambda]_,a_]:=zzbPrefactorTable[\[CapitalLambda],a]=Module[{tmp},
tmp=Normal@Series[(1/4-t+x+x^2)^a fxtDummy[x,t]/.{x->\[Epsilon] x,t->\[Epsilon]^2 t},{\[Epsilon],0,\[CapitalLambda]}]/.{\[Epsilon]->1};
tmp=CoefficientList[tmp,{x,t}];
tmp=tmp/.Dispatch@{Derivative[m_,n_][fxtDummy][0,0]:>fxtDummy[m,n]};
(*Flatten@Table[
tmp[[m+1,n+1]],
{m,0,\[CapitalLambda]},
{n,0,(\[CapitalLambda]-m)/2}
]*)
tmp
];
zzbPrefactorConvertor[prefix_,\[CapitalLambda]_,a_][table_]:=table/.Dispatch[{
((p:prefix)[m_,n_]->_):>(p[m,n]->Expand[m!n!(zzbPrefactorTable[\[CapitalLambda],a][[m+1,n+1]]/.fxtDummy->p/.table)])
}];
