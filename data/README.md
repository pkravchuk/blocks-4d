# Data files

These data files 

- `seed_recursion_formula_zzb.m`
- `dual_seed_recursion_formula_zzb.m`

and the code files

- `util.m`
- `seed_recursion.m`

are copied from `block_framework` (written in Mathematica). The functions in
`seed_recursion.m` create the other data files `(dual_)seed_recursion_formula_xt_****.m`,
which contain simplified descriptions of the seed recursion rules.

The file `convert_to_block_desc.m` contains code to convert these latter files to
block description files. The produced block description files may not strictly adhere
to the block descritption format, since they are treated in a special way in the
`C++` code.

These block description files are incorporated into `seed_recurse/seed_block_descriptoins.cxx`, 
so that they do not need to be provided alongside the executable.

We allow only `1234` and `1243` permutations. A bunch of other permutations can
be obtained by applying additional kinematic permutations that do not require their
own block-generating code. For example, `2134` can be obtained by taking `1243`
and applying the kinematic permutation `(21)(43)` on top.