import os

def options(opt):
    opt.load(['clang_compilation_database','compiler_cxx','gnu_dirs','cxx14','threads','boost','gmp','fmt','eigen'])

def configure(conf):
    conf.load(['compiler_cxx','gnu_dirs','cxx14','threads','boost','gmp','fmt','eigen'])

def build(bld):
    # '-fdiagnostics-show-template-tree',
    default_flags=['-Wall', '-Wextra', '-O3',  '-fdiagnostics-color=always', '-Wno-unused-parameter', '-Wno-return-std-move']
    # default_flags=['-Wall', '-Wextra', '-g',  '-fdiagnostics-color=always', '-Wno-unused-parameter', '-Wno-return-std-move']
    use_packages=['cxx14','threads','boost','gmp', 'fmt','eigen']

    bld.stlib(source=[ 'src/Polynomial/Multivariate.cxx',
                        'src/DerivativeTable/DerivativeTable.cxx',
                        'src/DerivativeTable/SeedTable.cxx',
                        'src/DerivativeTable/BlockTable.cxx',
                        'src/parsers/parsers.cxx',
                        'src/Polynomial/RationalFunction.cxx',
                        'src/BlockDescription/BlockDescription.cxx',
                        'src/seed_recurse/seed_block_descriptions.cxx',
                        'src/table_io/table_io.cxx',
                        'src/parse_spin_ranges.cxx',
                        'src/parsers/fast_block_parser.cxx',
                        'src/parsers/lookup.cxx',
                        'src/parsers/shunting_yard/read_token.cxx',
                        'src/parsers/shunting_yard/shunting_yard.cxx',
                        'src/parsers/fast_block_description_parser.cxx',
                        'src/pole_shifting/pole_shifting.cxx'
                        ],
                target='deps',
                cxxflags=default_flags,
                use=use_packages
                )
    
    # Main executable
    #bld.program(source=['src/main.cxx', 
    #                    'src/Polynomial/Multivariate.cxx',
    #                    'src/DerivativeTable/DerivativeTable.cxx',
    #                    'src/parsers/parsers.cxx',
    #                    'src/BlockDescription/BlockDescription.cxx'
    #                    ],
    #            target='spirit_test',
    #            cxxflags=default_flags,
    #            use=use_packages
    #            )

    # test

    bld.program(source=['src/main.cxx',
#                        'src/block_recurse/block_recurse.cxx'
                            ],
                target='blocks_4d_test',
                cxxflags=default_flags,
                use=use_packages + ['deps']
                )

    # seed_blocks
    bld.program(source=['src/seed_blocks.cxx', 
                        'src/seed_recurse/seed_recurse.cxx'
                         ],
                target='seed_blocks',
                cxxflags=default_flags,
                use=use_packages + ['deps']
                )

    # blocks_4d
    bld.program(source=['src/blocks_4d.cxx',
                        'src/block_recurse/block_recurse.cxx'
                        ],
                target='blocks_4d',
                cxxflags=default_flags,
                use=use_packages + ['deps']
                )            
