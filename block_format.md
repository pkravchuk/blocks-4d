# Format of block descrption files

## JSON
Description files for conformal blocks are in `JSON` format, with `C`-style comments enabled (`// comment` or `/* multi-line comment */`). Let us briefly review this 
format as necessary for our purposes. See [here](https://www.json.org/json-en.html) 
for the complete (and very short!) specification, as well as links to libraries 
for various languages. The basic notion in `JSON` are the key-value
pairs

    key : value

Here `key` is any string (i.e. `"a"`), while `value` can be of several types.
It can be a number,

    "a" : -1.234

a string,

    "a" : "stuff"

a list of values,

    "a" : [ value1, value2, ... ]

or an object

    "a" : { ... }

An object is simply a comma-separated list of key-value pairs between `{` and `}`.
In our case, the contents of the entire `JSON` file always represent an object. 
Here is a simple example of a `JSON` file,

    {
        "a" : 0,
        "b" : "stuff",
        "c" : { "some_key" : "some_string" },
        "an_array" : [1, 2, 3]
    }

A comment about strings : `JSON` supports escape sequences inside strings, and
it is common for `Mathematica` (among others) to escape slash `/` in strings when exporting to
`JSON`, even though it is not needed (for most applications). For example, running

    ExportString[{"a"->"b/c"},"JSON"]

in `Mathematica` produces the output

    {
        "a":"b\/c"
    }

This is perfectly legal and will not affect parsing of the file. In other words,
inside of `JSON` strings, `\/` means exactly the same as `/`. (Of course if you
want to actually represent `\/` in a `JSON` string, you simply need to escape the
backslash, `\\/`.)

## Block description files

### Basic format

A single block description file may contain description for several blocks that share
the quantum numbers of the exchanged operator. The bare minimum for specifying a block is

    {
        "p" : 3,
        "type" : "primal",
        "lambda_shift" : 4,
        "exported_blocks" : [ "block_name_1", ..., "block_name_n" ],
        "block_name_1" : {
            "value" : [
                {
                    "coefficient" : "x*t+d*x/(l+1)",
                    "perm" : 1234,
                    "e": 2,
                    "ap" : "0.123412",
                    "bp" : "-13.41234",
                    "xt_derivs" : [1, 1]
                }, 
                ...
            ]
        }
        ...
    }

There are additional optional parameters that can be specified and which are covered below.
For now, let us go through this example. First we specify that we have a primal 
`p=3` exchange. Then we specify `lambda_shift`, which is basically the maximal order of the 
differential operator that computes our blocks (`t`-derivatives counted twice). For simplicity it is the same for all
blocks in the file. The code will look for seed blocks that have `lambda` that is at least
`lambda+lambda_shift`, where the latter `lambda` is the derivative order that we wish to
compute our blocks at.

Then we declare that the file describes ("exports") some number of blocks with names like
`block_name_1` etc. The block name can be any string. 
Then we start describing our blocks by providing key-value pairs where keys are
the exported blocks and values are some objects. In the simplest case these objects only
contain one key `value` for which the value is a list of objects which represent
disctinct terms in the expression for `block_name_1` in terms of seed blocks.

Each term is a derivative of a seed block multiplied by a coefficient that is a function of
`x`, `t`, `d` (scaling dimension), and `l`, spin. (It can depend on additional parameters as well, see below.) The object representing the term
is pretty self-explanatory. Let us just note that `xt_derivs` contains first the order of the `x`-derivative and the the order of `t`-derivative.

### A comment about types and external parameters

Here are some comments about the types that we use for `JSON` values.

Since we typically need arbitrary-precision arithmetics for our computations,
we avoid using `JSON` number type to represent any non-integer numbers. This is because
in some languages/libraries the `JSON` parser may parse numbers into a standard floating point format
which does not have enough precision, leading to precision loss (or, better yet, a crash).  
So, if a number may in general be fractional, we use a string to represent its value,
i.e.

    "number" = "0.1234123413241235"

instead of

    "number" = 0.1234123413241235

More generally, we introduce the expression type for values. From the point of
view of `JSON`, an expression is just a string. (So that, in particular, `"0.1234123413241235"` is an expression.) For string to be an expression we additionally require it contain an arithmetic expression subject to the following rules.

Expression is built using binary operators `+, -, *, /, ^`, parentheses `(` and `)`, and unary minus `-`.
These operators act on numbers and variables. Numbers may be integer or floating-point (exponential notation for numbers must be explicitly expanded into, e.g., `1.123*10^10`). Variable names must start with
a letter, and subsequent symbols can be anything except spaces (whitespace, newline, etc.) or operator/parenthesis symbols. For example, `Delta'_12` is a valid variable name. Keep in mind that some symbols
need to be escaped, e.g., use `\"` for ". 

An additional restriction is that exponent of the power operator `^` can only be a constant integer, behavior is undefined otherwise. This means that when viewed as a function
of all the variables, expression is a ratinoal function with (generally) real coefficients.

Normally it should be safe to use `Mathematica`'s `ToString[expression, InputForm]`, provided one replaces the exponential notation `*^` by `*10^` in the result and gets rid of Mathematica precision
specifications such as ``1.2`20``. E.g. use the code

    toExpressionString[expr_]:=StringReplace[ToString[expr,InputForm], {"^*"->"*10^", "`"~~NumberString -> ""}];

This code hasn't been thoroughly tested though.

Unary minus has the highest precedence,
so `10^-5` is fine, etc.

Many values in block description files have expression type. The expressions depend on variables.
When a block is computed, the values for these variables are substituted from a lookup table passed 
to `blocks_4d` by the user. Parameters such as external dimensions should be passed in this way.
Additionally, the value of the spin is subsituted. Depending on the context, the expression is expected
to evaluate to a number or to a polynomial in some variables. In the latter case the code does not
perform any simplifications, so the expression should be explictily polynomial in the said variables in
the first place.

### Complete description of the format

Below we provide a complete description of the format. You can add new key-value pairs
if you want to store more information, they will be simply ignored by the code. We use (..)
to denote default values. When no default is specified, the value is required.

    {
        "dimension_var": <string>,      // ("d") The variable for scaling dimension 
        "spin_var": <string>,           // ("l") The variable for spin
        "x_var": <string>,              // ("x") The variable for x coordinate
        "t_var": <string>,              // ("t") The variable for t coordinate

        "spurious_poles": [<expression>, ...],              // ([]) List of spurious poles for 
                                                            // generic spin
        "spurious_poles_L": [                               // ([]) List of spins for which
            { "L" : <int>, "poles" : [<expression>, ...]},  // spurious pole list is special,
            ...                                             // along with the special lists
        ],

        "poles": [<expression>, ...],                       // ([]) List of extra poles for
                                                            // generic spin
        "poles_L": [                                        // ([]) Analogous to spurious_poles_L
            { "L" : <int>, "poles" : [<expression>, ...]},
            ...
        ],
        
        "p": <int>,             // value of p for the exchanged operator
        "type": <string>,       // "primal" or "dual", type of exchanged operator
        "seed_type": <string>,  // "primal" or "dual", type of seeds to use; defaults 
                                // to be the same as "type"
                                // We allow to specify type of the seed block to be possibly
                                // distinct from the type of the final block, since one
                                // may want to utilize kinematic permutations which flip
                                // dual and primal blocks.

        "lambda_shift": <int>,  // how much larger should lambda be for seed blocks
                                // files than for the blocks in this file
                                // (i.e. the order of the differential op, d_t counted twice)

        "description": <anything>,  // (null) Any valid JSON is allowed here, and is passed
                                    // directly into the output files, unless null.

        "exported_blocks" : [<string>, ...], // List of block names defined in this file

        <string> : {                                // Name of the block
            "zPower" : <expression>,                // ("0") The expression below is multiplied by
                                                    // (z*zb)^zPower
            "one_minus_zPower" : <expression>,      // ("0") Same as above for 
                                                    // ((1-z)(1-zb))^one_minus_zPower
            "value" : [
                {
                    "coefficient" : <expression>,   // Coefficient in front of seed block derivative
                    "perm" : <int>,                 // 1234 or 1243: the permutation type of seed block
                    "e" : <int>,                    // e, the 4pt structure index of the seed block
                    "ap" : <expression>,            // ap of the seed block
                    "bp" : <expression>,            // bp of the seed block
                    "xt_derivs" : [ <int>, <int> ]  // x- and t- orders of the derivative
                },
                ...
            ]
        }
    }



