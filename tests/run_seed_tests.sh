#!/bin/bash

scalar_blocks=$1
wolfram=$2
threads=$3

tests/generate_test_seeds.sh $scalar_blocks $threads
$wolfram tests/test_seeds.m