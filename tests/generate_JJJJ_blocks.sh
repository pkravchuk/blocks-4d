#!/bin/bash

order=80
poles=25
final_poles=20

lambda=5

scalar_blocks=$1
threads=$2

#Generate the seed blocks

tests/generate_JJJJ_seeds.sh $scalar_blocks $threads

#Generate P=0 blocks


#spin 0
for struct in {1..7}
do
  echo "Computing p=0 spin 0 structure $struct"
  description=tests/test_data/JJJJ_blocks/p\=0/blockP0Spin0Structure${struct}.json
  output=tests/output/block_tests/JJJJ_blocks/p\=0/blockP0Spin0Structure${struct}.json
  ./build/blocks_4d -d $description --precision 655 --order $order --poles $poles --finalize_poles $final_poles --spin-ranges 0 --lambda $lambda -i tests/output/block_tests/seeds -o $output
done

#even spins
for struct in {1..7}
do
  echo "Computing p=0 spin even structure $struct"
  description=tests/test_data/JJJJ_blocks/p\=0/blockP0SpinEvenStructure${struct}.json
  output=tests/output/block_tests/JJJJ_blocks/p\=0/blockP0Spin{}Structure${struct}.json
  ./build/blocks_4d --threads $threads -d $description --precision 655 --order $order --poles $poles --finalize_poles $final_poles --spin-ranges 2,4,6,8,10 --lambda $lambda -i tests/output/block_tests/seeds -o $output
done


#odd spins
for struct in 1 3 4 6 7
do
  echo "Computing p=0 spin odd structure $struct"
  description=tests/test_data/JJJJ_blocks/p\=0/blockP0SpinOddStructure${struct}.json
  output=tests/output/block_tests/JJJJ_blocks/p\=0/blockP0Spin{}Structure${struct}.json
  ./build/blocks_4d --threads $threads -d $description --precision 655 --order $order --poles $poles --finalize_poles $final_poles --spin-ranges 1,3,5,7,9 --lambda $lambda -i tests/output/block_tests/seeds -o $output
done

#Generate P=2 blocks

#Even spins
for struct in {1..7}
do
  echo "Computing p=2 spin even structure $struct"
  description=tests/test_data/JJJJ_blocks/p\=2/blockP2SpinEvenStructure${struct}.json
  output=tests/output/block_tests/JJJJ_blocks/p\=2/blockP2Spin{}Structure${struct}.json
  ./build/blocks_4d --threads $threads -d $description --precision 655 --order $order --poles $poles --finalize_poles $final_poles --spin-ranges 2,4,6,8,10 --lambda $lambda -i tests/output/block_tests/seeds -o $output
done

#Odd spins
for struct in {1..7}
do
  echo "Computing p=2 spin odd structure $struct"
  description=tests/test_data/JJJJ_blocks/p\=2/blockP2SpinOddStructure${struct}.json
  output=tests/output/block_tests/JJJJ_blocks/p\=2/blockP2Spin{}Structure${struct}.json
  ./build/blocks_4d --threads $threads -d $description --precision 655 --order $order --poles $poles --finalize_poles $final_poles --spin-ranges 1,3,5,7,9 --lambda $lambda -i tests/output/block_tests/seeds -o $output
done

#Generate P=4 blocks

#Even spins
for struct in {1..7}
do
  echo "Computing p=4 spin even structure $struct"
  description=tests/test_data/JJJJ_blocks/p\=4/blockP4SpinEvenStructure${struct}.json
  output=tests/output/block_tests/JJJJ_blocks/p\=4/blockP4Spin{}Structure${struct}.json
  ./build/blocks_4d --threads $threads -d $description --precision 655 --order $order --poles $poles --finalize_poles $final_poles --spin-ranges 0,2,4,6,8,10 --lambda $lambda -i tests/output/block_tests/seeds -o $output
done
