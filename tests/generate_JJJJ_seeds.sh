#!/bin/bash

scalar_blocks=$1
threads=$2

max_derivsP0=8
max_derivsP2=12
max_derivsP4=15

lambdaP2=13
lambdaP4=13


order=80
poles=25


depsP0=("--delta-12 0.00000 --delta-34 0.00000" "--delta-12 -2.00000 --delta-34 -2.00000" "--delta-12 -2.00000 --delta-34 0.00000" "--delta-12 -2.00000 --delta-34 2.00000")

depsP2=("--delta-12 -1.00000 --delta-34 -1.00000")

depsP4=("--delta-12 -2.00000 --delta-34 -2.00000")

#doing scalar blocks P=0

echo P=0
for dep in "${depsP0[@]}"
do
  echo "Computing P=0 seeds $dep"
  $scalar_blocks --dim 4 --precision 655 --num-threads $threads --max-derivs $max_derivsP0 --output-ab --spin-ranges 0-10 --poles $poles --order $order $dep -o tests/output/block_tests/seeds
done

#doing scalar blocks and seeds P=2

echo P=2
for dep in "${depsP2[@]}"
do
  echo "Computing P=2 seeds $dep"
  $scalar_blocks --dim 4 --precision 655 --num-threads $threads --max-derivs $max_derivsP2 --output-ab --spin-ranges 0-10 --poles $poles --order $order $dep -o tests/output/block_tests/scalar_blocks
  ./build/seed_blocks --type primal --p 2 $dep --threads $threads --precision 655 --order $order --poles $poles --spin-ranges 0-10 --lambda $lambdaP2 -i tests/output/block_tests/scalar_blocks -o tests/output/block_tests/seeds
done

#doing scalar blocks and seeds P=4

echo P=4
for dep in "${depsP4[@]}"
do
  echo "Computing P=4 seeds $dep"
  $scalar_blocks --dim 4 --precision 655 --num-threads $threads --max-derivs $max_derivsP4 --output-ab --spin-ranges 0-10 --poles $poles --order $order $dep -o tests/output/block_tests/scalar_blocks
  ./build/seed_blocks --type primal --p 4 $dep --threads $threads --precision 655 --order $order --poles $poles --spin-ranges 0-10 --lambda $lambdaP4 -i tests/output/block_tests/scalar_blocks -o tests/output/block_tests/seeds
done