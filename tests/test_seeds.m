(* ::Package:: *)

fileNameOld[1,type_,perm_,L_]:="tests/test_data/seed_blocks/old_code_tables/seedBlockTable4D-type-"<>type<>"-perm-"<>ToString[perm]<>"-P1-L"<>ToString[L]<>"-a--0.43955000-b-0.61955000-lambda-9-keptPoleOrder-25-order-90.m";


fileNameOld[2,type_,perm_,L_]:="tests/test_data/seed_blocks/old_code_tables/seedBlockTable4D-type-"<>type<>"-perm-"<>ToString[perm]<>"-P2-L"<>ToString[L]<>"-a--0.43955000-b-0.61955000-lambda-5-keptPoleOrder-25-order-90.m";


fileNameNew[1,type_,perm_,L_]:="tests/output/seed_tests/seeds/seedBlockTable4D-type-"<>type<>"-perm-"<>ToString[perm]<>"-P1-L"<>ToString[L]<>"-delta12-0.8791-delta34-1.2391-lambda-9-keptPoleOrder-25-order-90.m";


fileNameNew[2,type_,perm_,L_]:="tests/output/seed_tests/seeds/seedBlockTable4D-type-"<>type<>"-perm-"<>ToString[perm]<>"-P2-L"<>ToString[L]<>"-delta12-0.8791-delta34-1.2391-lambda-5-keptPoleOrder-25-order-90.m";


ClearAll[oldSeed,newSeed];
oldSeed[p_,type_,perm_,L_]:=oldSeed[p,type,perm,L]=Import[fileNameOld[p,type,perm,L]]/.d->x+2+L+p/2;
newSeed[p_,type_,perm_,L_]:=newSeed[p,type,perm,L]=Import[fileNameNew[p,type,perm,L]];


errorValue[p_,type_,perm_,L_]:=N@Max@Abs@Flatten[(oldSeed[p,type,perm,L][[All,2]]/(I^p*newSeed[p,type,perm,L][[All,2]])/.x->Range[1,10])-1];


Do[
WriteString["stdout","Testing p="<>ToString[p]<>" "<>type<>" perm="<>ToString[perm]<>" L="<>ToString[L]<>" : "];
If[errorValue[p,type,perm,L]<10^(-20),WriteLine["stdout","OK"];, 
	WriteLine["stdout","ERROR"];
	(*WriteLine["stderr","ERROR in p="<>ToString[p]<>" "<>type<>" perm="<>ToString[perm]<>" L="<>ToString[L]];*)
	Break[];
];
,{p,{1,2}}
,{type,{"primal","dual"}}
,{perm,{1234,1243}}
,{L,0,5}]
