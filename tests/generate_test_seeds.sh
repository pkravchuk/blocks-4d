#!/bin/bash

delta12=0.8791
delta34=1.2391

scalarblocks=$1
numthreads=$2

#we want lambda = 13 so we set nmax=7
maxderivs=7

#generate scalar blocks

echo "Computing scalar blocks"
$scalarblocks --dim 4 --order 90 --poles 25 --spin-ranges 0-5 --delta-12 $delta12 --delta-34 $delta34 --output-ab --max-derivs $maxderivs --num-threads $numthreads --precision=655 -o tests/output/seed_tests/scalars

#generate P=1 blocks

lambda=9
for type in "primal" "dual"
do
  for perm in "1234" "1243"
  do
    echo "Computing p=1 seed blocks of type $type and perm $perm"
    ./build/seed_blocks --type $type --perm $perm --p 1 --delta-12 $delta12 --delta-34 $delta34 --threads $numthreads --precision 655 --order 90 --poles 25 --spin-ranges 0-5 --lambda $lambda -i tests/output/seed_tests/scalars/ -o tests/output/seed_tests/seeds/
  done
done

lambda=5
for type in "primal" "dual"
do
  for perm in "1234" "1243"
  do
    echo "Computing p=2 seed blocks of type $type and perm $perm"
    ./build/seed_blocks --type $type --perm $perm --p 2 --delta-12 $delta12 --delta-34 $delta34 --threads $numthreads --precision 655 --order 90 --poles 25 --spin-ranges 0-5 --lambda $lambda -i tests/output/seed_tests/scalars/ -o tests/output/seed_tests/seeds/
  done
done