#!/bin/bash

if [ "$#" -ne "3" ] 
then 
  echo 'The script should be run as'
  echo '    ./tests/run_tests.sh <path-to-scalar_blocks> <wolfram-script-command> <number-of-threads-to-use>'
  echo 'For example:'
  echo '    ./tests/run_tests.sh "scalar_blocks" "wolframscript -script" 4'
  echo 'to run with 4 threads with scalar_blocks in the PATH and with Mathematica 12.0.0.0' 
  exit 1
fi

if [ ! -d "tests" ]
then
  echo 'This script should be run from the root directory of the repository'
  exit 1
fi

echo "=== Testing seed_blocks ==="
echo
tests/run_seed_tests.sh "$1" "$2" "$3"
echo
echo "=== Testing blocks_4d ==="
echo
tests/run_block_tests.sh "$1" "$2" "$3"
