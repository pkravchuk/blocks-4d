(* ::Package:: *)

SetDirectory[ParentDirectory[NotebookDirectory[]]];


ClearAll[jsonRead];
jsonRead[data_,path_List]:=Fold[Function[{subdata,element},element/.subdata],data,path];
jsonRead[data_,path_String]:=jsonRead[data,StringSplit[path,"."]];

numberJSONToMath[number_]:=StringReplace[number,"e"->"*^"]//ToExpression;

blockPoles[json_]:=numberJSONToMath/@jsonRead[json,"poles"];
blockElements[json_,name_]:=Map[#.Table[x^n,{n,0,Length[#]-1}]&,Map[numberJSONToMath,jsonRead[json,"blocks."<>name],{3}],{2}];
deltaMinusX[json_]:=numberJSONToMath[jsonRead[json,"delta_minus_x"]];

blockFormula[json_,name_]:=((4(3-2Sqrt[2]))^\[CapitalDelta] blockElements[json,name])/Times@@(\[CapitalDelta]-blockPoles[json])/.{x->\[CapitalDelta]-deltaMinusX[json]};


exactFiles=FileNames[__~~".m",FileNameJoin[{"tests","test_data","JJJJ_blocks","test_values"}]]


exactData=Join@@(Import/@exactFiles);


(* Some filtering out *)
exactData=Cases[DeleteCases[exactData,{blockP0SpinOdd[2|5][__],_,_}],{_[m_,n_],_,_}/;m+2n<=5];


keptPoleOrder=20;


fileName[p_,spin_,structure_]:=FileNameJoin[{"tests","output","block_tests","JJJJ_blocks","p="<>ToString[p],"blockP"<>ToString[p]<>"Spin"<>ToString[spin]<>"Structure"<>ToString[structure]<>".json"}];


blockName[blockP0Spin0[i_]]:="blockP0Spin0Structure"<>ToString[i];
blockName[blockP0SpinEven11[i_]]:="blockP0SpinEven11Structure"<>ToString[i];
blockName[blockP0SpinEven12[i_]]:="blockP0SpinEven12Structure"<>ToString[i];
blockName[blockP0SpinEven22[i_]]:="blockP0SpinEven22Structure"<>ToString[i];
blockName[blockP0SpinOdd[i_]]:="blockP0SpinOddStructure"<>ToString[i];
blockName[blockP2SpinEven[i_]]:="blockP2SpinEvenStructure"<>ToString[i];
blockName[blockP2SpinOdd[i_]]:="blockP2SpinOddStructure"<>ToString[i];
blockName[blockP4SpinEven[i_]]:="blockP4SpinEvenStructure"<>ToString[i];

blockP[blockP0Spin0[_]|blockP0SpinEven11[_]|blockP0SpinEven12[_]|blockP0SpinEven22[_]|blockP0SpinOdd[_]]:=0;
blockP[blockP2SpinEven[_]|blockP2SpinOdd[_]]:=2;
blockP[blockP4SpinEven[_]]:=4;


ClearAll[newBlockFormula];
newBlockFormula[name_, spin_]:=newBlockFormula[name, spin]=Module[{json},
json=Import[fileName[blockP[name],spin,name[[1]]],"JSON"];
If[Head[name]===blockP0SpinEven12,2,1]If[Head[name]===blockP0SpinOdd,-1,1]blockFormula[json, blockName[name]]
];


newBlockValue[{name_[m_,n_],{\[CapitalDelta]->dval_,l->lval_},_}]:=newBlockFormula[name,lval][[m+1,n+1]]/.\[CapitalDelta]->dval;


results={#[[1]],#[[2]],Abs[newBlockValue[#]/#[[-1]]-1]}&/@exactData;


Do[
	error=results[[i,-1]];
	If[(error>Max[10^-22,10^(-Precision[exactData[[i,-1]]]+2)])||!NumberQ[error],WriteLine["stdout","Error in "<>ToString[exactData[[i,1;;2]],InputForm]<>" with rel. error "<>ToString[results[[i,-1]],InputForm]];
	(*WriteLine["stderr","Error in "<>ToString[exactData[[i,1;;2]],InputForm]<>" with rel. error "<>ToString[results[[i,-1]],InputForm]];*)Exit[];];
,{i,Length@exactData}]

WriteLine["stdout", "Block tests OK"];
